<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Dashboard_model extends CI_Model
{
	public function rules(){
		return array(
			array('field'=>'kategori','label'=>'Kategori Berita','rules'=>'required'),
       		array('field'=>'judul','label'=>'Judul Berita','rules'=>'required|min_length[5]|max_length[50]'),
       		array('field'=>'isi','label'=>'Isi Berita','rules'=>'required|min_length[5]|max_length[250]')
		);
	}
	public function getUserDescription($id){
		$this->db->select('name, description');
		$this->db->from('groups');
		$this->db->where('id', $id);
		return $this->db->get();
	}

	public function info_getMessages(){
		$this->db->select('*');
		$this->db->from('hubungi');
		$this->db->where('dibaca', 'N');
		return $this->db->get()->num_rows();
    }

    public function new_message($limit){
    	$this->db->order_by('id_hubungi', 'DESC');
    	return $this->db->get('hubungi', $limit);
    }

    public function info_news(){
    	return $this->db->get('berita')->num_rows();
    }

    public function info_pages(){
    	return $this->db->get('halamanstatis')->num_rows();
    }

    public function info_agenda(){
    	return $this->db->get('agenda')->num_rows();
    }

	public function info_users(){
		return $this->db->get('users')->num_rows();
    }

    public function grafik_kunjungan(){
		$this->db->select('tanggal, COUNT(*) as jumlah');
		$this->db->group_by('tanggal'); 
		$this->db->order_by('tanggal', 'desc'); 
		return $this->db->get('statistik', 10)->result();
	}

	public function hits(){
		$this->db->select('SUM(hits) as jmlh_hits');
		$this->db->group_by('tanggal'); 
		$this->db->order_by('tanggal', 'desc'); 
		return $this->db->get('statistik', 10)->result();
	}

	public function hitsVisitor(){
		$this->db->select('SUM(hits) as jmlh_hits');
		return $this->db->get('statistik');
	}

	public function getKategori(){
		return $this->db->get('kategori');
    }

	public function quickInsertNews(){
		$data = array(
			'username' => $this->session->username,
			'tanggal' => date('Y-m-d'),
			'hari' => hari_ini(date('w')),
			'jam' => date('H:i:s'),
			'aktif' => 'N',
			'dibaca' => '0',
			'judul_seo' => seo_title($this->input->post('judul')),
			'id_kategori' => $this->input->post('kategori'),
            'judul' => $this->input->post('judul'),
            'isi_berita' => $this->input->post('isi')
        );

        // berita is the name of the db table you are inserting in
        return $this->db->insert('berita', $data);
	}

	public function visitor(){
        $ip      = $_SERVER['REMOTE_ADDR'];
        $tanggal = date("Y-m-d");
        $waktu   = time(); 
        $cekk = $this->db->query("SELECT * FROM statistik WHERE ip='$ip' AND tanggal='$tanggal'");
        $rowh = $cekk->row_array();
        if($cekk->num_rows() == 0){
            $datadb = array('ip'=>$ip, 'tanggal'=>$tanggal, 'hits'=>'1', 'online'=>$waktu);
            $this->db->insert('statistik',$datadb);
        }else{
            $hitss = $rowh['hits'] + 1;
            $datadb = array('ip'=>$ip, 'tanggal'=>$tanggal, 'hits'=>$hitss, 'online'=>$waktu);
            $array = array('ip' => $ip, 'tanggal' => $tanggal);
            $this->db->where($array);
            $this->db->update('statistik',$datadb);
        }
    }
}