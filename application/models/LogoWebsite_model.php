<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class LogoWebsite_model extends CI_Model
{
  var $table = 'logo';

  function logo(){
      return $this->db->get($this->table);
  }

  public function update($where, $data)
  {
    $this->db->update($this->table, $data, $where);
    return $this->db->affected_rows();
  }

  public function get_by_id($id)
  {
    $this->db->from($this->table);
    $this->db->where('id_logo',$id);
    $query = $this->db->get();
    return $query->row();
  }
}