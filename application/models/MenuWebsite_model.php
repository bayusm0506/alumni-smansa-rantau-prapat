<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class MenuWebsite_model extends CI_Model
{
	var $table = 'menu';
	var $column_order = array(null,'nama_menu','id_parent','link','aktif','position','urutan'); //set column field database for datatable orderable
	var $column_search = array('id_parent','nama_menu','link','aktif','position','urutan'); //set column field database for datatable searchable just firstname , lastname , address are searchable
	var $order = array('id_menu' => 'ASC'); // default order 

	private function _get_datatables_query()
	{
		//add custom filter here
        if($this->input->post('nama_menu'))
        {
            $this->db->like('nama_menu', $this->input->post('nama_menu'));
        }
        if($this->input->post('id_parent'))
        {
            $this->db->where('id_parent', $this->input->post('id_parent'));
        }
        if($this->input->post('link'))
        {
            $this->db->like('link', $this->input->post('link'));
        }
        if($this->input->post('aktif'))
        {
            $this->db->where('aktif', $this->input->post('aktif'));
        }
        if($this->input->post('position'))
        {
            $this->db->where('position', $this->input->post('position'));
        }
        if($this->input->post('urutan'))
        {
            $this->db->where('urutan', $this->input->post('urutan'));
        }

		$this->db->from($this->table);
		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if(isset($_POST['search']['value'])) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function rules(){
		return array(
			array('field'=>'link_menu','label'=>'Link Menu','rules'=>'required'),
       		array('field'=>'level_menu','label'=>'Level Menu','rules'=>'required'),
       		array('field'=>'nama_menu','label'=>'Nama Menu','rules'=>'required'),
       		array('field'=>'posisi','label'=>'Posisi','rules'=>'required'),
       		array('field'=>'urutan','label'=>'Urutan','rules'=>'required')
		);
	}

    public function menu_cek($id){
    	$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('id_menu', $id);
		return $this->db->get();
    }

    public function delete($id){
		$this->db->where('id_menu', $id);
		return $this->db->delete($this->table);
    }

    public function menu_utama(){
		$this->db->where('id_parent', 0);
		$this->db->order_by('urutan', 'ASC');
		return $this->db->get($this->table);
    }

    public function save($data){
        $this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data){
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function autocomplete($nama_menu){
        $this->db->like('nama_menu', $nama_menu , 'both');
        $this->db->order_by('nama_menu', 'ASC');
        $this->db->limit(10);
        return $this->db->get($this->table)->result();
    }

    public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('id_menu',$id);
		$query = $this->db->get();
		return $query->row();
	}

	//Function is use in views main/template
	public function getByMenuUtama(){
		$this->db->where('aktif', 'Ya');
		$this->db->where('position', 'Top');
		$this->db->where('id_parent', 0);
		$this->db->order_by('urutan', 'ASC');
		return $this->db->get($this->table);

		// return $this->db->query("
		// 	SELECT * FROM $this->table A
		// 		LEFT JOIN halamanstatis B ON A.link =  B.id_halaman
		// 	WHERE A.aktif = 'Ya' 
		// 		AND A.position = 'Top'
		// 		AND A.id_parent = '0'
		// 	ORDER BY A.urutan ASC
		// ");
    }

    //Function is use in views main/template
	public function getDropdownMenu($id){
		$this->db->where('aktif', 'Ya');
		$this->db->where('id_parent', $id);
		$this->db->order_by('urutan', 'ASC');
		return $this->db->get($this->table);
    }
}