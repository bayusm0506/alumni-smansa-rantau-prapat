<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class ListBerita_model extends CI_Model
{
  var $table = 'berita';
  var $column_order = array(null,'judul','tanggal','status'); //set column field database for datatable orderable
  var $column_search = array('judul','tanggal','status'); //set column field database for datatable searchable just firstname , lastname , address are searchable
  var $order = array('tanggal' => 'DESC'); // default order 

  private function _get_datatables_query()
  {
    //add custom filter here
        if($this->input->post('judul'))
        {
            $this->db->like('judul', $this->input->post('judul'));
        }
        if($this->input->post('tanggal'))
        {
            $this->db->where('tanggal', $this->input->post('tanggal'));
        }
        if($this->input->post('status'))
        {
            $this->db->where('status', $this->input->post('status'));
        }

    $this->db->from($this->table);
    $i = 0;
  
    foreach ($this->column_search as $item) // loop column 
    {
      if(isset($_POST['search']['value'])) // if datatable send POST for search
      {
        
        if($i===0) // first loop
        {
          $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->db->like($item, $_POST['search']['value']);
        }
        else
        {
          $this->db->or_like($item, $_POST['search']['value']);
        }

        if(count($this->column_search) - 1 == $i) //last loop
          $this->db->group_end(); //close bracket
      }
      $i++;
    }
    
    if(isset($_POST['order'])) // here order processing
    {
      $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } 
    else if(isset($this->order))
    {
      $order = $this->order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  function get_datatables()
  {
    $this->_get_datatables_query();
    if($_POST['length'] != -1)
    $this->db->limit($_POST['length'], $_POST['start']);
    $query = $this->db->get();
    return $query->result();
  }

  function count_filtered()
  {
    $this->_get_datatables_query();
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function count_all()
  {
    $this->db->from($this->table);
    return $this->db->count_all_results();
  }

	public function rules(){
		return array(
			array('field'=>'judul','label'=>'Judul','rules'=>'required'),
       		array('field'=>'sub_judul','label'=>'Sub Judul','rules'=>'required'),
       		array('field'=>'id_kategori_berita','label'=>'Kategori','rules'=>'required'),
       		array('field'=>'headline','label'=>'Headline','rules'=>'required'),
       		array('field'=>'pilihan','label'=>'Pilihan','rules'=>'required'),
       		array('field'=>'berita_utama','label'=>'Berita Utama','rules'=>'required'),
          array('field'=>'keterangan_gambar','label'=>'Keterangan Gambar','rules'=>'required')
		);
	}

  //Function is use frontend views main/rss and controller BlogGallery
	public function listBerita($id){
		$this->db->order_by('id_berita', 'DESC');
    if ($id > 0) {
      $this->db->limit($id);
    }
		return $this->db->get($this->table);
  }

  //function is use controller BlogGallery
  public function menu_cek($id){
    $this->db->select('*');
    $this->db->from($this->table);
    $this->db->where('id_berita', $id);
    return $this->db->get();
  }

  public function delete($id){
    $this->db->where('id_berita', $id);
    $result=$this->db->delete($this->table);
    return $result;
  }

  public function save($data)
  {
    $this->db->insert($this->table, $data);
    return $this->db->insert_id();
  }

  public function update($where, $data)
  {
    $this->db->update($this->table, $data, $where);
    return $this->db->affected_rows();
  }

  public function autocomplete($judul){
      $this->db->like('judul', $judul , 'both');
      $this->db->order_by('judul', 'ASC');
      $this->db->limit(10);
      return $this->db->get($this->table)->result();
  }

  public function autocomplete_berita($judul){
      $this->db->like('judul', $judul , 'both');
      $this->db->order_by('judul', 'ASC');
      $this->db->where('utama', 'Y');
      $this->db->where('headline', 'N');
      $this->db->where('status', 'Y');
      $this->db->limit(10);
      return $this->db->get($this->table)->result();
  }

  public function getHeadline(){
    $sql = "SELECT * FROM berita a
                LEFT JOIN users on a.username=users.username
                    LEFT JOIN kategori on a.id_kategori=kategori.id_kategori 
                        WHERE a.status='Y' AND a.aktif = 'Y' AND a.headline='Y'
              ";

              $sql .= " ORDER BY a.id_berita DESC LIMIT 2";
              //echo $sql;
              return $this->db->query($sql);
  }

  public function get_by_id($id)
  {
    $this->db->from($this->table);
    $this->db->where('id_berita',$id);
    $query = $this->db->get();
    return $query->row();
  }

  public function get_by_id_edit($id)
  {
    $this->db->from($this->table);
    $this->db->where('id_berita',$id);
    return $this->db->get();
  }

  // public function count_all_berita($judul)
  // {
  //   return $this->db->query("SELECT * FROM berita a 
  //                 JOIN users b on a.username=b.username 
  //                       WHERE a.status='Y' AND a.judul LIKE '%$judul%' 
  //                         ORDER BY a.id_berita DESC");
  // }

  public function count_all_berita($judul, $headline, $aktif, $utama){
      $sql = "SELECT * FROM berita a
                LEFT JOIN users on a.username=users.username
                    LEFT JOIN kategori on a.id_kategori=kategori.id_kategori 
                        WHERE a.status='Y' AND a.aktif='Y' AND a.headline='N'
              ";

              if ($judul != '') {
                $sql .= " AND a.judul LIKE '%$judul%'";
              }

              if ($headline != '') {
                $sql .= " AND a.headline = '$headline'";
              }

              if ($aktif != '') {
                $sql .= " AND a.aktif = '$aktif'";
              }

              if ($utama != '') {
                $sql .= " AND a.utama = '$utama'";
              }

              $sql .= " ORDER BY a.id_berita DESC";
              //echo $sql;
              return $this->db->query($sql);
  }

  //Function is use Controller blog
  // public function getCountBeritaUtama(){
  //     $this->db->from($this->table);
  //     $this->db->where('utama','Y');
  //     return $this->db->get();
  // }

  //Function is use views blog
  public function getUtama($start,$limit){
      return $this->db->query("SELECT * FROM berita 
                                          LEFT JOIN users on berita.username=users.username
                                              LEFT JOIN kategori on berita.id_kategori=kategori.id_kategori
                                                  WHERE berita.status = 'Y' AND berita.utama='Y' AND headline='N' ORDER BY berita.id_berita DESC LIMIT $start, $limit");
  
      // $this->db->from('berita as A');
      // $this->db->join('users as B','A.username = B.username','LEFT');
      // $this->db->join('kategori as C','A.id_kategori = C.id_kategori','LEFT');
      // $this->db->where('A.utama', 'Y');
      // $this->db->order_by('A.id_berita', 'DESC');
      // $this->db->limit($start, $limit);
      // return $this->db->get();
  }



  //Function is use controller blog function kategori
  public function getUtamaKategori($kategori, $start, $limit){
    return $this->db->query("
        SELECT * FROM berita 
        LEFT JOIN users 
            ON berita.username=users.username
        LEFT JOIN kategori 
            ON berita.id_kategori = kategori.id_kategori
        WHERE berita.status = 'Y' AND kategori.kategori_seo = '$kategori'
        ORDER BY berita.id_berita DESC LIMIT $start, $limit
    ");
  }

  //Function is use views blog
  public function getPopuler(){
      return $this->db->query("SELECT * FROM berita 
                                          LEFT JOIN users on berita.username=users.username
                                            LEFT JOIN kategori on berita.id_kategori=kategori.id_kategori 
                                              ORDER BY berita.dibaca DESC LIMIT 5");
  }

  //function is use views blog
  public function getRecent(){
      return $this->db->query("SELECT * FROM berita 
                                          LEFT JOIN users on berita.username=users.username
                                              LEFT JOIN kategori on berita.id_kategori=kategori.id_kategori
                                                  ORDER BY berita.id_berita DESC LIMIT 5");
  }

  //Function is use views blog
  public function getGroupKategori(){
    return $this->db->query("
        SELECT COUNT(*) AS jumlah,
          A.id_kategori, B.nama_kategori, B.kategori_seo
        FROM berita A
        LEFT JOIN kategori B
          ON A.id_kategori = B.id_kategori
        LEFT JOIN users C 
          ON A.username=C.username
        WHERE A.status = 'Y'
        GROUP BY A.id_kategori
    ");
  }

  //Function is use controller blog function kategori
  public function getGroupKategoriId($kategori){
    return $this->db->query("
        SELECT COUNT(*) AS jumlah,
          A.id_kategori, B.nama_kategori, B.kategori_seo
        FROM berita A
        LEFT JOIN kategori B
          ON A.id_kategori = B.id_kategori
        LEFT JOIN users C 
          ON A.username=C.username
        WHERE B.kategori_seo = '$kategori'
        AND A.status = 'Y'
        GROUP BY A.id_kategori
    ");
  }

  //Function is use controller blog function kategori
  public function getGroupKategoriDetail($kategori){
    return $this->db->query("
        SELECT * FROM berita 
        LEFT JOIN users 
            ON berita.username=users.username
        LEFT JOIN kategori 
            ON berita.id_kategori = kategori.id_kategori
        WHERE berita.status = 'Y' AND kategori.kategori_seo = '$kategori'
        ORDER BY berita.id_berita
    ");
  }

  //Function is use views blog
  public function getDetail($judul_seo){
      $this->db->from($this->table);
      $this->db->where('judul_seo', $judul_seo);
      $this->db->where('status', 'Y');
      return $this->db->get();
  }

  //Function is use controller blog
  public function getBeritaDetail($id){
        return $this->db->query("SELECT * FROM berita a 
                    LEFT JOIN users b ON a.username=b.username 
                        LEFT JOIN kategori c ON a.id_kategori=c.id_kategori 
                          WHERE status='Y' AND (a.id_berita='".$this->db->escape_str($id)."' OR a.judul_seo='".$this->db->escape_str($id)."')");
  }

  //Function is use controller blog
  public function getBeritaDibacaUpdate($id){
      return $this->db->query("UPDATE berita SET dibaca=dibaca+1 
                    WHERE id_berita='".$this->db->escape_str($id)."' OR judul_seo='".$this->db->escape_str($id)."'");
  }

  //Function is use controller blog
  public function getCariData($start, $limit, $keyword){
      return $this->db->query("SELECT * FROM berita a 
                  JOIN users b on a.username=b.username 
                        WHERE a.status='Y' AND a.judul LIKE '%$keyword%' 
                          ORDER BY a.id_berita DESC LIMIT $start,$limit");
  }
  
}