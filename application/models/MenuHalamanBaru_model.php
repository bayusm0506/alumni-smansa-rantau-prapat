<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class MenuHalamanBaru_model extends CI_Model
{
  var $table = 'halamanstatis';
  var $column_order = array(null,'judul','judul_seo','tgl_posting'); //set column field database for datatable orderable
  var $column_search = array('judul','judul_seo','tgl_posting'); //set column field database for datatable searchable just firstname , lastname , address are searchable
  var $order = array('id_halaman' => 'ASC'); // default order 

  private function _get_datatables_query()
  {
    //add custom filter here
        if($this->input->post('judul'))
        {
            $this->db->like('judul', $this->input->post('judul'));
        }
        if($this->input->post('judul_seo'))
        {
            $this->db->where('judul_seo', $this->input->post('judul_seo'));
        }
        if($this->input->post('tanggal_posting'))
        {
            $this->db->where('tgl_posting', $this->input->post('tanggal_posting'));
        }

    $this->db->from($this->table);
    $i = 0;
  
    foreach ($this->column_search as $item) // loop column 
    {
      if(isset($_POST['search']['value'])) // if datatable send POST for search
      {
        
        if($i===0) // first loop
        {
          $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->db->like($item, $_POST['search']['value']);
        }
        else
        {
          $this->db->or_like($item, $_POST['search']['value']);
        }

        if(count($this->column_search) - 1 == $i) //last loop
          $this->db->group_end(); //close bracket
      }
      $i++;
    }
    
    if(isset($_POST['order'])) // here order processing
    {
      $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } 
    else if(isset($this->order))
    {
      $order = $this->order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  function get_datatables()
  {
    $this->_get_datatables_query();
    if($_POST['length'] != -1)
    $this->db->limit($_POST['length'], $_POST['start']);
    $query = $this->db->get();
    return $query->result();
  }

  function count_filtered()
  {
    $this->_get_datatables_query();
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function count_all()
  {
    $this->db->from($this->table);
    return $this->db->count_all_results();
  }

	public function rules(){
		return array(
			array('field'=>'judul','label'=>'Judul','rules'=>'required'),
			array('field'=>'isi_halaman','label'=>'Isi Halaman','rules'=>'required')
		);
	}

	public function menuHalaman(){
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->order_by('id_halaman', 'DESC');
		return $this->db->get();
  }

  public function delete($id){
		$this->db->where('id_halaman', $id);
		$result=$this->db->delete($this->table);
		return $result;
    //return $this->db->delete($this->table, array('id_halaman' => $id));
  }

 //  public function save(){
 //  	$config = array(
	// 		'upload_path' => "./asset/foto_statis/",
	// 		'allowed_types' => "gif|jpg|png|jpeg|pdf|ico",
	// 		'overwrite' => TRUE,
	// 		'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
	// 		'max_height' => "768",
	// 		'max_width' => "1024",
 //      'file_name' => round(microtime(true) * 1000)."_".$_FILES["gambar"]['name']
	// 	);
	// 	$this->load->library('upload', $config);
	// 	$this->upload->do_upload('gambar');
 //    	$result = $this->upload->data();
 //    	if ($result['file_name']==''){
 //    		$datadb = array(
 //                  'judul'=>$this->db->escape_str($this->input->post('judul')),
 //                  'judul_seo'=>seo_title($this->input->post('judul')),
 //                  'isi_halaman'=>$this->input->post('isi_halaman'),
 //                  'tgl_posting'=>date('Y-m-d'),
 //                  'username'=>$this->session->username,
 //                  'dibaca'=>'0',
 //                  'jam'=>date('H:i:s'),
 //                  'hari'=>hari_ini(date('w')));
 //    	}else{
 //    		$datadb = array('judul'=>$this->db->escape_str($this->input->post('judul')),
 //                  'judul_seo'=>seo_title($this->input->post('judul')),
 //                  'isi_halaman'=>$this->input->post('isi_halaman'),
 //                  'tgl_posting'=>date('Y-m-d'),
 //                  'gambar'=>$result['file_name'],
 //                  'username'=>$this->session->username,
 //                  'dibaca'=>'0',
 //                  'jam'=>date('H:i:s'),
 //                  'hari'=>hari_ini(date('w')));
 //    	}

 //      // Group Menu is the name of the db table you are inserting in
 //      return $this->db->insert($this->table, $datadb);
	// }

	// public function update(){
	// 	$config = array(
 //  			'upload_path' => "./asset/foto_statis/",
 //  			'allowed_types' => "gif|jpg|png|jpeg|pdf|ico",
 //  			'overwrite' => TRUE,
 //  			'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
 //  			'max_height' => "768",
 //  			'max_width' => "1024"
 //        //'file_name' => round(microtime(true) * 1000)."_".$_FILES["gambar"]['name']
 //  		);
 //  		$this->load->library('upload', $config);
 //  		$this->upload->do_upload('gambar');
 //      	$result = $this->upload->data();
 //      	if ($result['file_name']==''){
 //      		$datadb = array('judul'=>$this->db->escape_str($this->input->post('judul')),
 //                    'judul_seo'=>seo_title($this->input->post('judul')),
 //                    'isi_halaman'=>$this->input->post('isi_halaman'),
 //                    'tgl_posting'=>date('Y-m-d'),
 //                    'username'=>$this->session->username,
 //                    'dibaca'=>'0',
 //                    'jam'=>date('H:i:s'),
 //                    'hari'=>hari_ini(date('w')));
 //      	}else{
 //      		$datadb = array('judul'=>$this->db->escape_str($this->input->post('judul')),
 //                    'judul_seo'=>seo_title($this->input->post('judul')),
 //                    'isi_halaman'=>$this->input->post('isi_halaman'),
 //                    'tgl_posting'=>date('Y-m-d'),
 //                    'gambar'=>$result['file_name'],
 //                    'username'=>$this->session->username,
 //                    'dibaca'=>'0',
 //                    'jam'=>date('H:i:s'),
 //                    'hari'=>hari_ini(date('w')));
 //      	}
	// 	// $id = $this->input->post('id_edit');

	// 	// $this->db->set($datadb);
	// 	// $this->db->where('id_halaman', $id);
	// 	// return $this->db->update($this->table);

 //    $this->db->where('id_halaman',$this->input->post('id_edit'));
 //    $this->db->update($this->table,$datadb);
	// }

  public function save($data)
  {
    $this->db->insert($this->table, $data);
    return $this->db->insert_id();
  }

  public function update($where, $data)
  {
    $this->db->update($this->table, $data, $where);
    return $this->db->affected_rows();
  }

  public function autocomplete($judul){
        $this->db->like('judul', $judul , 'both');
        $this->db->order_by('judul', 'ASC');
        $this->db->limit(10);
        return $this->db->get($this->table)->result();
    }

    public function autocomplete_link_menu($judul_seo){
        $this->db->like('judul_seo', $judul_seo , 'both');
        $this->db->order_by('judul_seo', 'ASC');
        $this->db->limit(10);
        return $this->db->get($this->table)->result();
    }

  public function get_by_id($id)
  {
    $this->db->from($this->table);
    $this->db->where('id_halaman',$id);
    $query = $this->db->get();

    return $query->row();
  }

  //function is use controller menuWebsite and pageGallery
  public function menu_cek($id){
    $this->db->select('*');
    $this->db->from($this->table);
    $this->db->where('id_halaman', $id);
    return $this->db->get();
  }

  //Function is use controller page
  public function getDetail($judul_seo){
      $this->db->from($this->table);
      $this->db->where('judul_seo', $judul_seo);
      return $this->db->get();
  }

  //Function is use controller page
  public function getPageDetail($id){
        return $this->db->query("SELECT * FROM $this->table a 
                    LEFT JOIN users b ON a.username=b.username 
                          WHERE (a.id_halaman='".$this->db->escape_str($id)."' OR a.judul_seo='".$this->db->escape_str($id)."')");
  }

  //Function is use controller page
  public function getPageDibaca($id){
      return $this->db->query("UPDATE $this->table SET dibaca=dibaca+1 
                    WHERE id_halaman='".$this->db->escape_str($id)."'");
  }

  //Function is use controller pageGallery
  public function getHalaman(){
    $this->db->order_by('id_halaman', 'ASC');
    return $this->db->get($this->table);
  }
}