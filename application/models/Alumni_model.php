<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Alumni_model extends CI_Model
{
  var $table = 'siswa';
  var $column_order = array(null,'nama','tempat_lahir','tgl_lahir','alamat_baru','stambuk','id_jurusan','id_gelar','foto','kd_anggota'); //set column field database for datatable orderable
  var $column_search = array('nama','tempat_lahir','stambuk'); //set column field database for datatable searchable just firstname , lastname , address are searchable
  var $order = array('siswa_id' => 'ASC'); // default order 

  private function _get_datatables_query()
  {
    //add custom filter here
        if($this->input->post('nama'))
        {
            $this->db->like('nama', $this->input->post('nama'));
        }
        if($this->input->post('tgl_lahir'))
        {
            $this->db->where('tgl_lahir', $this->input->post('tgl_lahir'));
        }
        if($this->input->post('stambuk'))
        {
            $this->db->where('stambuk', $this->input->post('stambuk'));
        }
        if($this->input->post('tahun_lulus'))
        {
            $this->db->where('tahun_lulus', $this->input->post('tahun_lulus'));
        }
        if($this->input->post('id_jurusan'))
        {
            $this->db->where('id_jurusan', $this->input->post('id_jurusan'));
        }
        if($this->input->post('id_gelar'))
        {
            $this->db->where('id_gelar', $this->input->post('id_gelar'));
        }
        if($this->input->post('tgl_terdaftar'))
        {
            $this->db->where('tgl_terdaftar', $this->input->post('tgl_terdaftar'));
        }

    $this->db->from($this->table);
    $i = 0;
  
    foreach ($this->column_search as $item) // loop column 
    {
      if(isset($_POST['search']['value'])) // if datatable send POST for search
      {
        
        if($i===0) // first loop
        {
          $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->db->like($item, $_POST['search']['value']);
        }
        else
        {
          $this->db->or_like($item, $_POST['search']['value']);
        }

        if(count($this->column_search) - 1 == $i) //last loop
          $this->db->group_end(); //close bracket
      }
      $i++;
    }
    
    if(isset($_POST['order'])) // here order processing
    {
      $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } 
    else if(isset($this->order))
    {
      $order = $this->order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  function get_datatables()
  {
    $this->_get_datatables_query();
    if($_POST['length'] != -1)
    $this->db->limit($_POST['length'], $_POST['start']);
    $query = $this->db->get();
    return $query->result();
  }

  function count_filtered()
  {
    $this->_get_datatables_query();
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function count_all()
  {
    $this->db->from($this->table);
    return $this->db->count_all_results();
  }

  public function delete($id){
    $this->db->where('siswa_id', $id);
    $result=$this->db->delete($this->table);
    return $result;
  }

  public function save($data)
  {
    $this->db->insert($this->table, $data);
    return $this->db->insert_id();
  }

  public function update($where, $data)
  {
    $this->db->update($this->table, $data, $where);
    return $this->db->affected_rows();
  }

  public function autocomplete($nama){
      $this->db->like('nama', $nama , 'both');
      $this->db->order_by('nama', 'ASC');
      $this->db->limit(10);
      return $this->db->get($this->table)->result();
  }

  public function get_by_id($id)
  {
    $this->db->from($this->table);
    $this->db->where('siswa_id',$id);
    $query = $this->db->get();
    return $query->row();
  }

  public function rules(){
    return array(
      array('field'=>'nama','label'=>'Nama','rules'=>'required'),
          array('field'=>'email','label'=>'Email','rules'=>'required|min_length[5]|max_length[50]'),
          array('field'=>'subjek','label'=>'Subjek','rules'=>'required|min_length[5]|max_length[250]'),
          array('field'=>'isi_pesan','label'=>'Message','rules'=>'required|min_length[5]|max_length[250]')
    );
  }

  public function cekAnggota($kd_anggota)
  {
    $this->db->from($this->table);
    $this->db->where('kd_anggota',$kd_anggota);
    $this->db->limit(1);
    return $this->db->get();
  }

  public function chartJurusan(){
    return $this->db->query("
      SELECT B.nama_jurusan AS nama_jurusan, COUNT(A.id_jurusan) AS jumlah FROM siswa A, jurusan B
      WHERE A.id_jurusan = B.kd_jurusan
        GROUP BY A.id_jurusan
      ORDER BY jumlah
    ")->result();
  }

  public function chartGelar(){
    return $this->db->query("
      SELECT B.nama_gelar AS nama_gelar, COUNT(A.id_jurusan) AS jumlah FROM siswa A, gelar B
      WHERE A.id_gelar = B.id_gelar
        GROUP BY A.id_gelar
      ORDER BY jumlah
    ")->result();
  }

  public function cekData($stambuk, $jurusan){
    $this->db->select('COUNT(*) AS jumlah_data');
    $this->db->from($this->table);
    $this->db->where('stambuk',$stambuk);
    $this->db->where('id_jurusan',$jurusan);
    return $this->db->get();
  }

  public function cekDataTerakhir($stambuk, $jurusan){
    $this->db->select('kd_anggota');
    $this->db->from($this->table);
    $this->db->where('stambuk',$stambuk);
    $this->db->where('id_jurusan',$jurusan);
    $this->db->order_by('kd_anggota', 'DESC');
    $this->db->limit(1);
    return $this->db->get();
  }

  public function max_id_siswa(){
    $this->db->select_max('id');
    $this->db->from($this->table);
    return $this->db->get();
  }

  //Function is use Controller Main
  public function getCountAlumni(){
      $this->db->from($this->table);
      return $this->db->get();
  }

  //Function is use controller main
  public function getAlumni($start,$limit){
      return $this->db->query("SELECT * FROM siswa A
                                LEFT JOIN jurusan B ON A.id_jurusan = B.kd_jurusan
                                  LEFT JOIN gelar C ON A.id_gelar = C.id_gelar
                                  ORDER BY A.siswa_id ASC LIMIT $start, $limit");
  
  }

  //Function is use controller main
  public function terkait($siswa_id, $stambuk,$jurusan){
      return $this->db->query("SELECT * FROM siswa A
                                LEFT JOIN jurusan B ON A.id_jurusan = B.kd_jurusan
                                  LEFT JOIN gelar C ON A.id_gelar = C.id_gelar
                                  WHERE A.stambuk='$stambuk' AND A.id_jurusan='$jurusan' AND A.siswa_id <> $siswa_id ORDER BY RAND() LIMIT 3");
  
  }

  //Function is use controller blog
  public function getCariData($start, $limit, $nama, $stambuk, $jurusan,$profesi){
      $sql = "SELECT A.siswa_id, A.foto, A.id_jurusan, A.id_gelar, A.nama, A.tempat_lahir, A.tgl_lahir, A.stambuk, A.profesi, B.nama_jurusan, C.nama_gelar FROM siswa A
                                LEFT JOIN jurusan B ON A.id_jurusan = B.kd_jurusan
                                  LEFT JOIN gelar C ON A.id_gelar = C.id_gelar
                                  WHERE 1=1
              ";

              if ($nama != '') {
                $sql .= " AND A.nama LIKE '%$nama%'";
              }

              if ($stambuk != '') {
                $sql .= " AND A.stambuk='$stambuk'";
              }

              if ($jurusan != '') {
                $sql .= " AND A.id_jurusan='$jurusan'";
              }

              if ($profesi != '') {
                $sql .= " AND A.profesi='$profesi'";
              }

              $sql .= " ORDER BY A.siswa_id ASC LIMIT $start, $limit";
              //echo $sql;
              return $this->db->query($sql);
  }

  # get count of record
  public function count_all_alumni($nama, $stambuk, $jurusan, $profesi)
  {
          //$sql = "SELECT * FROM db_ticketing.tr_ticket WHERE requested_by LIKE '%$match%'";
          $sql = "
            SELECT A.siswa_id, A.foto, A.id_jurusan, A.id_gelar, A.nama, A.tempat_lahir, A.tgl_lahir, A.stambuk, B.nama_jurusan, C.nama_gelar FROM siswa A
                                LEFT JOIN jurusan B ON A.id_jurusan = B.kd_jurusan
                                  LEFT JOIN gelar C ON A.id_gelar = C.id_gelar
                                  WHERE 1=1
          ";

          if ($nama != '') {
            $sql .= " AND A.nama LIKE '%$nama%'";
          }

          if ($stambuk != '') {
            $sql .= " AND A.stambuk='$stambuk'";
          }

          if ($jurusan != '') {
            $sql .= " AND A.id_jurusan='$jurusan'";
          }

          if ($profesi != '') {
            $sql .= " AND A.profesi='$profesi'";
          }
              
          $query =  $this->db->query($sql);
          $result = $query->result_array();

          $count = count($result);
          return $count;
  }

  # get all results matched
  function get_all_alumni($limit,$page)
  {
      if($this->input->get('jurusan'))
      {
          $match = $this->input->get('jurusan');
          // $sql = "SELECT * FROM db_ticketing.tr_ticket WHERE requested_by LIKE '%$match%'  LIMIT $page, $limit";
          $sql = "
            SELECT A.siswa_id, A.foto, A.id_jurusan, A.id_gelar, A.nama, A.tempat_lahir, A.tgl_lahir, A.stambuk, B.nama_jurusan, C.nama_gelar FROM siswa A
                                LEFT JOIN jurusan B ON A.id_jurusan = B.kd_jurusan
                                  LEFT JOIN gelar C ON A.id_gelar = C.id_gelar
                                  WHERE 1=1 AND A.id_jurusan = '$match' LIMIT $page, $limit
          ";
          return $this->db->query($sql);
          // $result = $query->result_array();
          // return $result;
      }
  }
}