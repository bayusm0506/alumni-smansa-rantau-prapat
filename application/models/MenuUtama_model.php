<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class MenuUtama_model extends CI_Model
{
	public function rules(){
		return array(
			array('field'=>'nama_website','label'=>'Nama Website','rules'=>'required'),
       		array('field'=>'email','label'=>'Email','rules'=>'required'),
       		array('field'=>'social','label'=>'Social Network','rules'=>'required'),
       		array('field'=>'domain','label'=>'Domain','rules'=>'required'),
       		array('field'=>'telpon','label'=>'No. Telpon','rules'=>'required'),
       		array('field'=>'meta_keyword','label'=>'Meta Keyword','rules'=>'required'),
       		array('field'=>'meta_deskripsi','label'=>'Meta Deskripsi','rules'=>'required'),
       		array('field'=>'maps','label'=>'Maps','rules'=>'required'),
       		array('field'=>'info_footer','label'=>'Info Footer','rules'=>'required')
		);
	}

	public function identitas(){
		$this->db->order_by('id_identitas', 'DESC');
		return $this->db->get('identitas', 1);
  }

  public function favicon(){
    $this->db->select('favicon');
    return $this->db->get('identitas', 1);
  }

  public function do_upload(){
    	$config = array(
  			'upload_path' => "./asset/images/",
  			'allowed_types' => "gif|jpg|png|jpeg|pdf|ico",
  			'overwrite' => TRUE,
  			'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
  			'max_height' => "768",
  			'max_width' => "1024"
  		);
  		$this->load->library('upload', $config);
  		$this->upload->do_upload('favicon');
      $result = $this->upload->data();
      if ($result['file_name']==''){
              $datadb = array('nama_website'=>$this->db->escape_str($this->input->post('nama_website')),
                              'address'=>$this->db->escape_str($this->input->post('address')),
                              'email'=>$this->db->escape_str($this->input->post('email')),
                              'url'=>$this->db->escape_str($this->input->post('domain')),
                              'facebook'=>$this->db->escape_str($this->input->post('social')),
                              'twitter'=>$this->db->escape_str($this->input->post('twitter')),
                              'youtube'=>$this->db->escape_str($this->input->post('youtube')),
                              'open'=>$this->db->escape_str($this->input->post('open')),
                              'keterangan'=>$this->db->escape_str($this->input->post('info_footer')),
                              'no_telp'=>$this->db->escape_str($this->input->post('telpon')),
                              'whatsapp'=>$this->db->escape_str($this->input->post('whatsapp')),
                              'meta_deskripsi'=>$this->db->escape_str($this->input->post('meta_deskripsi')),
                              'meta_keyword'=>$this->db->escape_str($this->input->post('meta_keyword')),
                              'moto'=>$this->db->escape_str($this->input->post('moto')),
                              'maps'=>$this->db->escape_str($this->input->post('maps')));
      }else{
              $datadb = array('nama_website'=>$this->db->escape_str($this->input->post('nama_website')),
                              'address'=>$this->db->escape_str($this->input->post('address')),
                              'email'=>$this->db->escape_str($this->input->post('email')),
                              'url'=>$this->db->escape_str($this->input->post('domain')),
                              'facebook'=>$this->db->escape_str($this->input->post('social')),
                              'twitter'=>$this->db->escape_str($this->input->post('twitter')),
                              'youtube'=>$this->db->escape_str($this->input->post('youtube')),
                              'open'=>$this->db->escape_str($this->input->post('open')),
                              'keterangan'=>$this->db->escape_str($this->input->post('info_footer')),
                              'no_telp'=>$this->db->escape_str($this->input->post('telpon')),
                              'whatsapp'=>$this->db->escape_str($this->input->post('whatsapp')),
                              'meta_deskripsi'=>$this->db->escape_str($this->input->post('meta_deskripsi')),
                              'meta_keyword'=>$this->db->escape_str($this->input->post('meta_keyword')),
                              'moto'=>$this->db->escape_str($this->input->post('moto')),
                              'maps'=>$this->db->escape_str($this->input->post('maps')),
                              'favicon'=>$result['file_name']
                         );
      }
      $this->db->where('id_identitas',1);
      $this->db->update('identitas',$datadb);
    }
}