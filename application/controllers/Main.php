<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Main extends CI_Controller {
	public function index(){
		// if (isset($_POST['cari'])) {
		// 	$nama = strip_tags($this->input->post('nama'));
		// 	$stambuk = $this->input->post('stambuk');
		// 	$jurusan = $this->input->post('jurusan');

		// 	$this->session->set_userdata(array('nama'=>$nama));
		// 	$this->session->set_userdata(array('stambuk'=>$stambuk));
		// 	$this->session->set_userdata(array('jurusan'=>$jurusan));

		// 	$hasil = "";
		// 	if ($nama != '') {
		// 		$hasil .= "Nama : ".$nama;
		// 	}

		// 	if ($stambuk != '') {
		// 		$hasil .= " Stambuk : ".$stambuk;
		// 	}

		// 	if ($jurusan != '') {
		// 		$kd_jurusan = $this->Jurusan_model->getName($jurusan)->row_array();
		// 		$hasil .= " Jurusan : ".$kd_jurusan['nama_jurusan'];
		// 	}

		// 	$data['title'] = 'Hasil Pencarian dengan keyword '.$hasil;
		// 	$data['description'] = getDescription();
		// 	$data['keywords'] = getKeywords();
		// 	$data['favicon'] = getFavicon();

		// 	$jumlah= $this->Alumni_model->count_all_alumni($nama, $stambuk, $jurusan);
		// 	$config['base_url'] = base_url().'main/index';
		// 	$config['total_rows'] = $jumlah;
		// 	$config['per_page'] = 16; 
		// 	$config['first_link'] = 'First';
		// 	$config['last_link'] = 'Last';	
		// 	$config['prev_link'] = 'Prev';
		// 	$config['next_link'] = 'Next';

		// 	if ($this->uri->segment('4')!=''){
		// 		$dari = $this->uri->segment('4');
		// 	}else{
		// 		$dari = 0;
		// 	}

		// 	if (is_numeric($dari)) {
		// 		$data['alumni'] = $this->Alumni_model->getCariData($dari, $config['per_page'],$nama,$stambuk,$jurusan);
		// 	}else{
		// 		redirect('main');
		// 	}
		// 	$this->pagination->initialize($config);
		// }
		if (isset($_GET['cari'])) {
			$nama = strip_tags($this->input->get('nama', TRUE));
			$stambuk = $this->input->get('stambuk', TRUE);
			$jurusan = $this->input->get('jurusan', TRUE);
			$profesi = $this->input->get('profesi', TRUE);

			//$dari = $this->input->get('per_page');

			$hasil = "";
			if ($nama != '') {
				$hasil .= "Nama : ".$nama;
			}

			if ($stambuk != '') {
				$hasil .= " Stambuk : ".$stambuk;
			}

			if ($jurusan != '') {
				$kd_jurusan = $this->Jurusan_model->getName($jurusan)->row_array();
				$hasil .= " Jurusan : ".$kd_jurusan['nama_jurusan'];
			}

			if ($profesi != '') {
				$hasil .= " Profesi : ".$profesi;
			}

			$data['title'] = 'Hasil Pencarian dengan keyword '.$hasil;
			$data['description'] = getDescription();
			$data['keywords'] = getKeywords();
			$data['favicon'] = getFavicon();

			$jumlah= $this->Alumni_model->count_all_alumni($nama, $stambuk, $jurusan, $profesi);
			$config['page_query_string'] = TRUE;
			$config['base_url'] = base_url().'main/index/?nama='.$nama.'&stambuk='.$stambuk.'&jurusan='.$jurusan.'&profesi='.$profesi.'&cari=';
			$config['total_rows'] = $jumlah;
			$config['per_page'] = 12; 
			$config['first_link'] = 'First';
			$config['last_link'] = 'Last';	
			$config['prev_link'] = 'Prev';
			$config['next_link'] = 'Next';

			$fetch_data = $this->input->get('per_page', TRUE);
			if ($fetch_data != ''){
				$dari = $fetch_data;
			}else{
				$dari = 0;
			}

			if (is_numeric($dari)) {
				$data['alumni'] = $this->Alumni_model->getCariData($dari, $config['per_page'],$nama,$stambuk,$jurusan,$profesi);
			}else{
				redirect('main');
			}
			$this->pagination->initialize($config);
		}
		else{
			$jumlah= $this->Alumni_model->getCountAlumni()->num_rows();
			$data['title'] = getTitle();
			$data['description'] = getDescription();
			$data['keywords'] = getKeywords();
			$data['favicon'] = getFavicon();

			$config['base_url'] = base_url().'main/index';
			$config['total_rows'] = $jumlah;
			$config['per_page'] = 16; 
			$config['first_link'] = 'First';
			$config['last_link'] = 'Last';	
			$config['prev_link'] = 'Prev';
			$config['next_link'] = 'Next';
			if ($this->uri->segment('3')!=''){
				$dari = $this->uri->segment('3');
			}else{
				$dari = 0;
			}

			if (is_numeric($dari)) {
				$data['alumni'] = $this->Alumni_model->getAlumni($dari, $config['per_page']);
			}else{
				redirect('main');
			}
			$this->pagination->initialize($config);
		}

		$data['jurusan'] = $this->Jurusan_model->jurusan();
		$data['kepsek'] = $this->KepalaSekolah_model->kepalaSekolah();
		$data['ads'] = $this->AdsHome_model->getAds();
		$this->template->load('layouts/Template','main/index',$data);
		$this->load->view('main/rss');
	}

	public function detail(){
    	$ids = $this->uri->segment(3);
    	$dat = $this->Alumni_model->get_by_id($this->db->escape_str($ids));
    	if ($dat == '') {
    		redirect('main');
    	}

        $data['title'] = getCetak($dat->nama);
		$data['description'] = getCetak($dat->nama);
		$data['keywords'] = getKeywords();
		$data['favicon'] = getFavicon();

		$data['detail'] = $this->Alumni_model->get_by_id($this->db->escape_str($ids));
		$data['jurusan'] = $this->Jurusan_model->getName($dat->id_jurusan)->row_array();
		$data['gelar'] = $this->Gelar_model->getName($dat->id_gelar)->row_array();
		$data['terkait'] = $this->Alumni_model->terkait($dat->siswa_id, $dat->stambuk, $dat->id_jurusan); 
		$data['ads'] = $this->AdsDetailAlumni_model->getAds();

		$this->template->load('layouts/Template','main/_alumni_detail',$data);
		$this->load->view('main/rss');
    }

	public function get_autocomplete(){
        if (isset($_GET['term'])) {
            $result = $this->Alumni_model->autocomplete($_GET['term']);
            if (count($result) > 0) {
            foreach ($result as $row)
                $arr_result[] = $row->nama;
                echo json_encode($arr_result);
            }
        }
    }

    private function rules(){
		return $this->Alumni_model->rules();
	}

    public function validateForm()
   	{
       	$rules = self::rules();
       	$this->form_validation->set_rules($rules);
       	$this->form_validation->set_message('required', '{field} Tidak Boleh Kosong');

        if ($this->form_validation->run() == FALSE){
            $errors = validation_errors();
           	//echo json_encode(['error'=>$errors]);
            $response = ["status"=>'error','msg'=>$errors];
            echo json_encode($response);
        }else{
        	$data = array(
	            'nama'=>$this->input->post('nama'),
				'email'=>$this->input->post('email'),
				'subjek'=>$this->input->post('subjek'),
				'pesan'=>$this->input->post('isi_pesan'),
				'tanggal'=>date('Y-m-d'),
				'jam'=>date('H:i:s')
			);

			$insert = $this->PesanMasuk_model->save($data);
            $response = ["status"=>'info','msg'=>'Data Berhasil di Simpan.'];
            echo json_encode($response);
           	//echo json_encode(['success'=>'Form submitted successfully.']);
        }
    }
}
