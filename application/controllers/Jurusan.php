<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Jurusan extends CI_Controller {
	public function index(){
		if (isset($_POST['cari'])) {
			$nama = strip_tags($this->input->post('nama'));
			$stambuk = "";
			$jurusan = "";

			$hasil = "";
			if ($nama != '') {
				$hasil .= "Nama : ".$nama;
			}

			$data['title'] = 'Hasil Pencarian dengan keyword '.$hasil;
			$data['description'] = getDescription();
			$data['keywords'] = getKeywords();
			$data['favicon'] = getFavicon();

			$jumlah= $this->Alumni_model->count_all_alumni($nama, $stambuk, $jurusan);
			$config['base_url'] = base_url().'jurusan/index';
			$config['total_rows'] = $jumlah;
			$config['per_page'] = 16; 
			$config['first_link'] = 'First';
			$config['last_link'] = 'Last';	
			$config['prev_link'] = 'Prev';
			$config['next_link'] = 'Next';

			if ($this->uri->segment('3')!=''){
				$dari = $this->uri->segment('3');
			}else{
				$dari = 0;
			}

			if (is_numeric($dari)) {
				$data['alumni'] = $this->Alumni_model->getCariData($dari, $config['per_page'],$nama,$stambuk,$jurusan);
			}else{
				redirect('main');
			}
			$this->pagination->initialize($config);
		}else{
			$jurusan = $this->uri->segment(3);
	    	$nama = "";
	    	$stambuk = "";
	    	$data['title'] = getTitle();
			$data['description'] = getDescription();
			$data['keywords'] = getKeywords();
			$data['favicon'] = getFavicon();

			$jumlah= $this->Alumni_model->count_all_alumni($nama, $stambuk, $jurusan);
			$config['base_url'] = base_url().'jurusan/index/'.$jurusan;
			$config['total_rows'] = $jumlah;
			$config['per_page'] = 16; 
			$config['first_link'] = 'First';
			$config['last_link'] = 'Last';	
			$config['prev_link'] = 'Prev';
			$config['next_link'] = 'Next';

			if ($this->uri->segment('4')!=''){
				$dari = $this->uri->segment('4');
			}else{
				$dari = 0;
			}

			if (is_numeric($dari)) {
				$data['alumni'] = $this->Alumni_model->getCariData($dari, $config['per_page'],$nama,$stambuk,$jurusan);
			}else{
				redirect('main');
			}
			$this->pagination->initialize($config);
		}

		$data['jurusan'] = $this->Jurusan_model->getName($jurusan)->row_array();
		$this->template->load('layouts/Template','jurusan/index',$data);
		$this->load->view('main/rss');
	}
}
