<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Page extends CI_Controller {
	public function index(){
		$ids = $this->uri->segment(3);
		$dat = $this->MenuHalamanBaru_model->getDetail($this->db->escape_str($ids));
	    $row = $dat->row();
	    $total = $dat->num_rows();
        if ($total == 0){
        	redirect('main');
        }
		$data['title'] = getCetak($row->judul);
		$data['description'] = getDescription();
		$data['keywords'] = getKeywords();
		$data['record'] = $this->MenuHalamanBaru_model->getPageDetail($ids)->row_array();
		$data['favicon'] = getFavicon();
		$this->MenuHalamanBaru_model->getPageDibaca($row->id_halaman);

		$this->template->load('layouts/Template','page/index',$data);
		$this->load->view('main/rss');
	}

	public function detail(){
		$ids = $this->uri->segment(3);
		$dat = $this->MenuHalamanBaru_model->getDetail($this->db->escape_str($ids));
	    $row = $dat->row();
	    $total = $dat->num_rows();
        if ($total == 0){
        	redirect('main');
        }
		$data['title'] = getCetak($row->judul);
		$data['description'] = getDescription();
		$data['keywords'] = getKeywords();
		$data['record'] = $this->MenuHalamanBaru_model->getPageDetail($ids)->row_array();
		$data['gallery'] = $this->PageGallery_model->getPageGallery($row->id_halaman)->result_array();
		$data['jumGallery'] = $this->PageGallery_model->getPageGallery($row->id_halaman)->num_rows();
		$data['favicon'] = getFavicon();
		$this->MenuHalamanBaru_model->getPageDibaca($row->id_halaman);

		$this->template->load('layouts/Template','page/index',$data);
		$this->load->view('main/rss');
	}
}
