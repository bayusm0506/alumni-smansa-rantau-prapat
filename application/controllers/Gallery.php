<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Gallery extends CI_Controller {
	public function index(){
		// $data['kategori'] = $this->Album_model->getKategoriAlbum()->result_array();
		$data['gallery'] = $this->Gallery_model->getGalleryAlbum(0)->result_array();
		$data['title'] = 'Gallery - '.getTitle();
		$data['description'] = getDescription();
		$data['keywords'] = getKeywords();
		$data['favicon'] = getFavicon();

		$this->template->load('layouts/Template','gallery/index',$data);
		$this->load->view('main/rss');
	}

	public function detail(){
		$ids = $this->uri->segment(3);
		$dat = $this->Gallery_model->getDetail($this->db->escape_str($ids));
	    $row = $dat->row();
	    $total = $dat->num_rows();
        if ($total == 0){
        	redirect('main');
        }
		$data['title'] = getCetak($row->jdl_gallery);
		$data['description'] = getDescription();
		$data['keywords'] = getKeywords();
		$data['record'] = $this->Gallery_model->getPortfolioDetail($ids)->row_array();
		$data['related'] = $this->Gallery_model->getGalleryAlbum($row->id_album)->result_array();
		$data['favicon'] = getFavicon();
		$this->Album_model->getPortfolioDibaca($row->id_album);
		$this->template->load('layouts/Template','portfolio/_portfolio_detail',$data);
	}
}
