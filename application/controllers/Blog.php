<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Blog extends CI_Controller {
	public function index(){
		// if (isset($_POST['kata'])) {
		// 	$keyword = strip_tags($this->input->post('kata'));
		// 	$data['title'] = 'Hasil Pencarian dengan keyword : '.$keyword;
		// 	$data['description'] = getDescription();
		// 	$data['keywords'] = getKeywords();
		// 	$data['favicon'] = getFavicon();
		// 	$data['utama'] = $this->ListBerita_model->getCariData(0,5,$keyword);
		// }
		if (isset($_GET['cari'])) {
			$judul = strip_tags($this->input->get('judul', TRUE));
			$hasil = "";
			if ($judul != '') {
				$hasil .= "Judul Berita : ".$judul;
			}

			$data['title'] = 'Hasil Pencarian dengan keyword '.$hasil;
			$data['description'] = getDescription();
			$data['keywords'] = getKeywords();
			$data['favicon'] = getFavicon();

			$jumlah= $this->ListBerita_model->count_all_berita($judul, $headline = "", $aktif = "", $utama = "")->num_rows();
			$config['page_query_string'] = TRUE;
			$config['base_url'] = base_url().'blog/index/?judul='.$judul;
			$config['total_rows'] = $jumlah;
			$config['per_page'] = 6; 
			$config['first_link'] = 'First';
			$config['last_link'] = 'Last';	
			$config['prev_link'] = 'Prev';
			$config['next_link'] = 'Next';

			$fetch_data = $this->input->get('per_page', TRUE);
			if ($fetch_data != ''){
				$dari = $fetch_data;
			}else{
				$dari = 0;
			}

			if (is_numeric($dari)) {
				$data['utama'] = $this->ListBerita_model->getCariData($dari, $config['per_page'],$judul);
			}else{
				redirect('main');
			}
			$data['headline'] = $this->ListBerita_model->getHeadline();
			$this->pagination->initialize($config);
		}
		else{
			$jumlah= $this->ListBerita_model->count_all_berita($judul = "", $headline = "", $aktif = "", $utama = "")->num_rows();
			$data['title'] = 'Blog - '.getTitle();
			$data['description'] = getDescription();
			$data['keywords'] = getKeywords();
			$data['favicon'] = getFavicon();

			$config['base_url'] = base_url().'blog/index';
			$config['total_rows'] = $jumlah;
			$config['per_page'] = 6; 	
			$config['first_link'] = 'First';
			$config['last_link'] = 'Last';	
			$config['prev_link'] = 'Prev';
			$config['next_link'] = 'Next';
			if ($this->uri->segment('3')!=''){
				$dari = $this->uri->segment('3');
			}else{
				$dari = 0;
			}

			if (is_numeric($dari)) {
				$data['utama'] = $this->ListBerita_model->getUtama($dari, $config['per_page']);
			}else{
				redirect('main');
			}
			$data['ads'] = $this->AdsBlog_model->getAds();
			$data['headline'] = $this->ListBerita_model->getHeadline(); 
			$this->pagination->initialize($config);
		}

		$this->template->load('layouts/Template','blog/index',$data);
		$this->load->view('main/rss');
	}

	public function detail(){
		$ids = $this->uri->segment(3);
		$dat = $this->ListBerita_model->getDetail($this->db->escape_str($ids));
	    $row = $dat->row();
	    $total = $dat->num_rows();
        if ($total == 0){
        	redirect('main');
        }
		$data['title'] = getCetak($row->judul);
		$data['description'] = getCetak($row->isi_berita);
		$data['keywords'] = getKeywords();
		$data['record'] = $this->ListBerita_model->getBeritaDetail($ids)->row_array();
		$data['gallery'] = $this->BlogGallery_model->getBlogGallery($row->id_berita)->result_array();
		$data['jumGallery'] = $this->BlogGallery_model->getBlogGallery($row->id_berita)->num_rows();
		$data['favicon'] = getFavicon();
		$this->ListBerita_model->getBeritaDibacaUpdate($ids);
		$this->template->load('layouts/Template','blog/_berita_detail',$data);
		$this->load->view('main/rss');
	}

	public function get_autocomplete(){
        if (isset($_GET['term'])) {
            $result = $this->ListBerita_model->autocomplete_berita($_GET['term']);
            if (count($result) > 0) {
            foreach ($result as $row)
                $arr_result[] = $row->judul;
                echo json_encode($arr_result);
            }
        }
    }
}
