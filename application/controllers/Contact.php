<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Contact extends CI_Controller {
	public function index(){
		$data['identity'] = $this->MenuUtama_model->identitas()->row_array();
		$data['title'] = 'Kontak Kami - '.getTitle();
		$data['description'] = 'Silahkan Mengisi Form Dibawah ini untuk menghubungi kami';
		$data['keywords'] = 'hubungi, kontak, kritik, saran, pesan';
		$data['favicon'] = getFavicon();

		$this->template->load('layouts/Template','contact/index',$data);
		$this->load->view('main/rss');
	}
}
