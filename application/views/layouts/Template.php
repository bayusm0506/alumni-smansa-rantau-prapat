<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title; ?></title>
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no'/>
    <meta name="robots" content="index, follow">
    <meta name="description" content="<?php echo $description; ?>">
    <meta name="keywords" content="<?php echo $keywords; ?>">
    <meta name="author" content="<?php echo $title; ?>">
    <meta http-equiv="imagetoolbar" content="no">
    <meta name="language" content="Indonesia">
    <meta name="revisit-after" content="7">
    <meta name="webcrawlers" content="all">
    <meta name="rating" content="general">
    <meta name="spiders" content="all">

    <link rel="shortcut icon" href="<?php echo base_url()?>asset/images/<?php echo $favicon; ?>" />
    <link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="rss.xml" />
    
    
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,700" rel="stylesheet">

    <link rel="stylesheet" href="<?=base_url()?>asset/frontend/css/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="<?=base_url()?>asset/frontend/css/animate.css">
    <link rel="stylesheet" href="<?=base_url()?>asset/frontend/fonts/ionicons/css/ionicons.min.css">
    
    <link rel="stylesheet" href="<?=base_url()?>asset/frontend/css/owl.carousel.min.css">
    
    <link rel="stylesheet" href="<?=base_url()?>asset/frontend/fonts/flaticon/font/flaticon.css">

    <link rel="stylesheet" href="<?=base_url()?>asset/frontend/fonts/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?=base_url()?>asset/frontend/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="<?=base_url()?>asset/frontend/css/select2.css">
    

    <link rel="stylesheet" href="<?=base_url()?>asset/frontend/css/helpers.css">
    <link rel="stylesheet" href="<?=base_url()?>asset/frontend/css/style.css">

    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/image-gallery/css/blueimp-gallery.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/image-gallery/css/blueimp-gallery-indicator.css">

    <!-- <script src="<?=base_url()?>asset/frontend/js/jquery.min.js"></script> -->
    <script src="<?php echo base_url(); ?>asset/admin/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/admin/dist/js/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/dist/css/jquery-ui.min.css">
    
    <script src="<?php echo base_url(); ?>asset/admin/dist/js/component.js"></script>

    <style type="text/css">
      .pagination {
          display: inline-block;
      }

      .pagination a {
          color: black;
          float: left;
          padding: 8px 16px;
          text-decoration: none;
          border: 1px solid #ddd;
      }

      .pagination a.active {
          background-color: #4CAF50;
          color: white;
          border: 1px solid #4CAF50;
      }

      .pagination span.current {
          background-color: #4CAF50;
          color: white;
          float: left;
          padding: 8px 16px;
          text-decoration: none;
          border: 1px solid #4CAF50;
      }

      .pagination a:hover:not(.active) {background-color: #ddd;}

      .pagination a:first-child {
          border-top-left-radius: 5px;
          border-bottom-left-radius: 5px;
      }

      .pagination a:last-child {
          border-top-right-radius: 5px;
          border-bottom-right-radius: 5px;
      }

      li a:hover, .nav-item:hover .dropbtn {
          background-color: #00CA4C;
          color:#000;
      }

      li.nav-item {
          display: inline-block;
      }

      .dropdown-content {
          display: none;
          position: absolute;
          background-color: #f9f9f9;
          min-width: 160px;
          box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
          /*z-index: 1;*/
      }

      .dropdown-content a {
          color: black;
          padding: 12px 16px;
          text-decoration: none;
          display: block;
          text-align: left;
      }

      .dropdown-content a:hover {background-color: #f1f1f1}

      .nav-item:hover .dropdown-content {
          display: block;
      }

      #invoice{
          padding: 30px;
      }

      .invoice {
          position: relative;
          background-color: #FFF;
          min-height: 680px;
          padding: 15px
      }

      .invoice header {
          padding: 10px 0;
          margin-bottom: 20px;
          border-bottom: 1px solid #3989c6
      }

      .invoice .company-details {
          text-align: right
      }

      .invoice .company-details .name {
          margin-top: 0;
          margin-bottom: 0
      }

      .invoice .contacts {
          margin-bottom: 20px
      }

      .invoice .invoice-to {
          text-align: left
      }

      .invoice .invoice-to .to {
          margin-top: 0;
          margin-bottom: 0
      }

      .invoice .invoice-details {
          text-align: right
      }

      .invoice .invoice-details .invoice-id {
          margin-top: 0;
          color: #3989c6
      }

      .invoice main {
          padding-bottom: 50px
      }

      .invoice main .thanks {
          margin-top: -100px;
          font-size: 2em;
          margin-bottom: 50px
      }

      .invoice main .notices {
          padding-left: 6px;
          border-left: 6px solid #3989c6
      }

      .invoice main .notices .notice {
          font-size: 1.2em
      }

      .invoice table {
          width: 100%;
          border-collapse: collapse;
          border-spacing: 0;
          margin-bottom: 20px
      }

      .invoice table td,.invoice table th {
          padding: 15px;
          background: #eee;
          border-bottom: 1px solid #fff
      }

      .invoice table th {
          white-space: nowrap;
          font-weight: 400;
          font-size: 16px
      }

      .invoice table td h3 {
          margin: 0;
          font-weight: 400;
          color: #3989c6;
          font-size: 1.2em
      }

      .invoice table .qty,.invoice table .total,.invoice table .unit {
          text-align: right;
          font-size: 1.2em
      }

      .invoice table .no {
          color: #fff;
          font-size: 1.6em;
          background: #3989c6
      }

      .invoice table .unit {
          background: #ddd
      }

      .invoice table .total {
          background: #3989c6;
          color: #fff
      }

      .invoice table tbody tr:last-child td {
          border: none
      }

      .invoice table tfoot td {
          background: 0 0;
          border-bottom: none;
          white-space: nowrap;
          text-align: right;
          padding: 10px 20px;
          font-size: 1.2em;
          border-top: 1px solid #aaa
      }

      .invoice table tfoot tr:first-child td {
          border-top: none
      }

      .invoice table tfoot tr:last-child td {
          color: #3989c6;
          font-size: 1.4em;
          border-top: 1px solid #3989c6
      }

      .invoice table tfoot tr td:first-child {
          border: none
      }

      .invoice footer {
          width: 100%;
          text-align: center;
          color: #777;
          border-top: 1px solid #aaa;
          padding: 8px 0
      }

      @media print {
          .invoice {
              font-size: 11px!important;
              overflow: hidden!important
          }

          .invoice footer {
              position: absolute;
              bottom: 10px;
              page-break-after: always
          }

          .invoice>div:last-child {
              page-break-before: always
          }
      }
    </style>
  </head>
  <body>
    <div class="modal modal-danger fade" id="modal-compare">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"></h4>
          </div>
          <div class="modal-body">
            <p id="m-message">&hellip;</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline pull-right" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <nav class="navbar navbar-expand-lg navbar-dark probootstrap_navbar" id="probootstrap-navbar">
      <div class="container">
        <?php
          $logo = $this->LogoWebsite_model->logo()->result_array();
          foreach ($logo as $p) {
            echo "<a class='navbar-brand' href='".base_url()."'><img src='".base_url()."asset/logo/$p[logo]' alt='Alumni SMANSA Rantau Prapat' width='70px' height='70px'></a>";
          }
        ?>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#probootstrap-menu" aria-controls="probootstrap-menu" aria-expanded="false" aria-label="Toggle navigation">
          <span><i class="ion-navicon"></i></span>
        </button>
        <div class="collapse navbar-collapse" id="probootstrap-menu">
          <ul class="navbar-nav ml-auto">
            <!-- <li class="nav-item"><a class="nav-link" href="<?=site_url()?>">Home</a></li>
            <li class="nav-item"><a class="nav-link" href="<?=site_url('blog')?>">Blog</a></li>
            <li class="nav-item"><a class="nav-link" href="<?=site_url('contact')?>">Kontak</a></li> -->
            

            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url(); ?>"><i class="icon-home"></i> Home</i></a>
            </li>
            <?php
              $menuatas = $this->MenuWebsite_model->getByMenuUtama();
              foreach ($menuatas->result_array() as $row){
                  $dropdown = $this->MenuWebsite_model->getDropdownMenu($row['id_menu'])->num_rows();
                  if ($dropdown == 0){
                    echo "<li class='nav-item'><a class='nav-link' href='".site_url()."$row[link]'>$row[nama_menu]</a></li>";
                  }else{
                    echo "<li class='nav-item'>
                          <a class='nav-link dropbtn' href='".site_url()."$row[link]'>$row[nama_menu] <i class='icon-angle-down'></i></a>
                          <div class='dropdown-content'>";
                            $dropmenu = $this->model_menu->dropdown_menu($row['id_menu']);
                            foreach ($dropmenu->result_array() as $row){
                                echo "<a href='".site_url()."$row[link]'>$row[nama_menu]</a>";
                            }
                          echo "</div>
                        </li>";
                  }
              }
            ?>
          </ul>
        </div>
      </div>
    </nav>
    <!-- END nav -->

    <?php 
      echo $contents; 
      $this->Dashboard_model->visitor();
    ?>


    <footer class="probootstrap_section probootstrap-border-top">
      <div class="container">
        <div class="row pt-5">
          <div class="col-md-12 text-center">
            <?php
              $logo = $this->LogoWebsite_model->logo()->result_array();
              foreach ($logo as $p) {
                echo "<a href='".base_url()."'><img src='".base_url()."asset/logo/$p[logo]' alt='Alumni SMANSA Rantau Prapat' width='170px' height='170px'></a>";
              }
            ?>
            <p class="probootstrap_font-14">&copy; <?=date('Y')?>. All Rights Reserved.</p>
            <p class="probootstrap_font-14"><?php echo getTitle(); ?></p>
            <p role="button" class="btn btn-primary p-3 mr-3 pl-5 pr-5 text-uppercase d-lg-inline d-md-inline d-sm-block d-block mb-3">Jumlah Pengunjung : <?php echo getVisitor(); ?></p>
          </div>
        </div>
      </div>
    </footer>
    
    <script src="<?=base_url()?>asset/frontend/js/popper.min.js"></script>
    <script src="<?=base_url()?>asset/frontend/js/bootstrap.min.js"></script>
    <script src="<?=base_url()?>asset/frontend/js/owl.carousel.min.js"></script>

    <script src="<?=base_url()?>asset/frontend/js/bootstrap-datepicker.js"></script>
    <script src="<?=base_url()?>asset/frontend/js/jquery.waypoints.min.js"></script>
    <script src="<?=base_url()?>asset/frontend/js/jquery.easing.1.3.js"></script>

    <script src="<?=base_url()?>asset/frontend/js/select2.min.js"></script>

    <script src="<?=base_url()?>asset/frontend/js/main.js"></script>
    <script src="<?=base_url()?>asset/frontend/js/holder.min.js"></script>

    <!-- Images Gallery -->
    <script src="<?php echo base_url(); ?>asset/image-gallery/js/blueimp-helper.js"></script>
    <script src="<?php echo base_url(); ?>asset/image-gallery/js/blueimp-gallery.js"></script>
    <script src="<?php echo base_url(); ?>asset/image-gallery/js/blueimp-gallery-fullscreen.js"></script>
    <script src="<?php echo base_url(); ?>asset/image-gallery/js/blueimp-gallery-indicator.js"></script>
    <script src="<?php echo base_url(); ?>asset/image-gallery/js/jquery.blueimp-gallery.js"></script>
    <script>
      Holder.addTheme('thumb', {
        bg: '#55595c',
        fg: '#eceeef',
        text: 'Thumbnail'
      });
    </script>
    <script type="text/javascript">
      var url = window.location;
      // for sidebar menu entirely but not cover treeview
      $('ul.navbar-nav a').filter(function() {
        return this.href == url;
      }).parent().addClass('active');

      // for treeview
      // $('ul.dropdown-menu a').filter(function() {
      //   return this.href == url;
      // }).closest('.dropdown').addClass('active');
    </script>
  </body>
</html>