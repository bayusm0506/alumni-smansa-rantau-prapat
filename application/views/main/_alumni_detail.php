<section class="probootstrap-cover overflow-hidden relative"  style="background-image: url('<?=base_url()?>asset/frontend/images/bg_1.jpg');" data-stellar-background-ratio="0.5" id="section-home">
  <div class="overlay"></div>
  <div class="container">
    <div class="row align-items-center text-center">
      <div class="col-md">
        <h2 class="heading mb-2 display-4 font-light probootstrap-animate"><?=$title?></h2>
      </div> 
    </div>
  </div>
</section>

<section class="probootstrap_section bg-light">
  <div class="container">
    <div class="row text-center mb-5 probootstrap-animate">
      <div class="col-md-12">
        <h2 class="display-4 border-bottom probootstrap-section-heading">Details</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-8">
        <div class="row mb-3">
          <div class="col-md-4">
            <?php
              if ($detail->foto == '') {
                echo "<a href='".base_url()."asset/frontend/images/no-photo.png' title='".$detail->nama."' data-gallery><img src='".base_url()."asset/frontend/images/no-photo.png' alt='".$detail->nama."'></a>";
              }else{
                echo "<a href='".base_url()."asset/alumni/$detail->foto' title='".$detail->nama."' data-gallery><img src='".base_url()."asset/alumni/$detail->foto' alt='".$detail->nama."' width='222px' height='230px'></a>";
                //echo '<div class="probootstrap-media-image" style="background-image: url('.base_url().'asset/alumni/'.$detail->foto.'); width="270px" height="262px"></div>';
              }
            ?>
          </div>
          <div id="blueimp-gallery" class="blueimp-gallery">
            <div class="slides"></div>
            <h3 class="title"></h3>
            <a class="prev" style="color:#fff">‹</a>
            <a class="next" style="color:#fff">›</a>
            <a class="close" style="color:#fff">×</a>
            <a class="play-pause"></a>
            <ol class="indicator"></ol>
          </div>
          <div class="col-md-8">
            <div class="row mb-3">
              <div class="col-md-6">
                <h5 class="mb-3"><?=$detail->nama?></h5>
              </div>
            </div>
            <div class="row mb-3">
              <div class="col-md-6">
                <ul>
                  <li>Tempat Lahir : <?=$detail->tempat_lahir?></li>
                  <li>Tanggal Lahir : <?=tgl_indo($detail->tgl_lahir)?></li>
                  <li>Alamat Lama : <?=$detail->alamat_lama?></li>
                  <li>Alamat Baru : <?=$detail->alamat_baru?></li>
                  <li>Stambuk : <?=$detail->stambuk?></li>
                  <li>Jurusan : <?=$jurusan['nama_jurusan']?></li>
                  <li>Tahun Lulus : <?=$detail->tahun_lulus?></li>
                  <li>Tahun Pindah : <?=$detail->tahun_pindah?></li>
                  <li>Sekolah Pindah : <?=$detail->sekolah_pindah?></li>
                </ul>
              </div>
              <div class="col-md-6">
                <ul>
                  <li>Gelar : <?=$gelar['nama_gelar']?></li>
                  <li>Profesi : <?=$detail->profesi?></li>
                  <li>Nama Keluarga : <?=$detail->nama_keluarga?></li>
                  <li>Profesi Keluarga : <?=$detail->profesi_keluarga?></li>
                  <li>Hobi : <?=$detail->hobi?></li>
                  <li>Kode Anggota : <?=$detail->kd_anggota?></li>
                  <?php
                    if ($detail->facebook != '') {
                      echo "<li><i class='fa fa-facebook-official'></i> Facebook : <a href='".$detail->facebook."' target='_blank'>".$detail->facebook."</a></li>";
                    }

                    if ($detail->instagram != '') {
                      echo "<li><i class='fa fa-instagram'></i> Instagram : <a href='".$detail->instagram."' target='_blank'>".$detail->instagram."</a></li>";
                    }

                    if ($detail->twitter != '') {
                      echo "<li><i class='fa fa-twitter'></i> Twitter : <a href='".$detail->twitter."' target='_blank'>".$detail->twitter."</a></li>";
                    }

                    if ($detail->youtube != '') {
                      echo "<li><i class='fa fa-youtube'></i> Youtube : <a href='".$detail->youtube."' target='_blank'>".$detail->youtube."</a></li>";
                    }
                  ?>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-md-12" style="overflow: auto;">
            <?php
              $cekAds = $ads->num_rows();
              if ($cekAds > 0) {
                $_array = $ads->result_array();
                foreach ($_array as $p) {
                  echo "<img src='".base_url('asset/img_ads/'.$p['gbr_ads'])."' alt='".$p['jdl_ads']."'><br /><br />";
                }
              }else{
                echo "<img src='".base_url('asset/img_ads/advertisement_detail.jpg')."' alt='Ads'>";
              }
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- END section -->