<section class="probootstrap_section" id="section-city-guides">
  <div class="container">
    <div class="row text-center mb-5 probootstrap-animate">
      <div class="col-md-12">
        <h2 class="display-4 border-bottom probootstrap-section-heading">Top Places Around The World</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-3 col-md-6 probootstrap-animate mb-3">
        <a href="#" class="probootstrap-thumbnail">
          <img src="<?=base_url()?>asset/frontend/images/img_1.jpg" alt="Free Template by uicookies.com" class="img-fluid">
          <div class="probootstrap-text">
            <h3>Buena</h3>
          </div>
        </a>
      </div>
      <div class="col-lg-3 col-md-6 probootstrap-animate mb-3">
        <a href="#" class="probootstrap-thumbnail">
          <img src="<?=base_url()?>asset/frontend/images/img_2.jpg" alt="Free Template by uicookies.com" class="img-fluid">
          <h3>Road</h3>
        </a>
      </div>
      <div class="col-lg-3 col-md-6 probootstrap-animate mb-3">
        <a href="#" class="probootstrap-thumbnail">
          <img src="<?=base_url()?>asset/frontend/images/img_3.jpg" alt="Free Template by uicookies.com" class="img-fluid">
          <h3>Australia</h3>
        </a>
      </div>
      <div class="col-lg-3 col-md-6 probootstrap-animate mb-3">
        <a href="#" class="probootstrap-thumbnail">
          <img src="<?=base_url()?>asset/frontend/images/img_4.jpg" alt="Free Template by uicookies.com" class="img-fluid">
          <h3>Japan</h3>
        </a>
      </div>
    </div>
  </div>
</section>