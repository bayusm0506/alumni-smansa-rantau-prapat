<section class="probootstrap-cover overflow-hidden relative"  style="background-image: url('<?=base_url()?>asset/frontend/images/bg_1.jpg');" data-stellar-background-ratio="0.5"  id="section-home">
  <div class="overlay"></div>
  <div class="container">
    <div class="row align-items-center">
      <div class="col-md">
        <h2 class="heading mb-2 display-4 font-light probootstrap-animate">Temukan Teman Alumnimu Disini</h2>
        <p class="lead mb-5 probootstrap-animate">Berharap kamu akan terus terhubung satu sama lain</p>
        <p class="probootstrap-animate">
          <a href="<?=site_url('contact')?>" role="button" class="btn btn-primary p-3 mr-3 pl-5 pr-5 text-uppercase d-lg-inline d-md-inline d-sm-block d-block mb-3">Kontak Kami</a> 
        </p>
      </div> 
      <div class="col-md probootstrap-animate">
        <?php //echo form_open('main/index', array('class'=>'probootstrap-form'));?>
        <form method="get" action="" class="probootstrap-form">
          <div class="form-group">
            <div class="row mb-5">
              <div class="col-md">
                <div class="form-group">
                  <label for="probootstrap-date-departure">Nama Alumni</label>
                  <div class="probootstrap-date-wrap"> 
                    <input name="nama" type="text" class="form-control" placeholder="Nama Alumni" id="nama">
                  </div>
                </div>
              </div>
            </div>
            <div class="row mb-5">
              <div class="col-md">
                <div class="form-group">
                  <label for="probootstrap-date-departure">Profesi</label>
                  <div class="probootstrap-date-wrap"> 
                    <input name="profesi" type="text" class="form-control" placeholder="Profesi">
                  </div>
                </div>
              </div>
            </div>
            <!-- END row -->
            <div class="row mb-3">
              <div class="col-md">
                <div class="form-group">
                  <label for="id_label_single">Stambuk</label>

                  <label for="id_label_single" style="width: 100%;">
                    <select name="stambuk" class="js-example-basic-single js-states form-control" style="width: 100%;">
                      <option value="">Pilih Stambuk</option>
                      <?php
                          for ($i=1950; $i <= date('Y'); $i++) { 
                              echo "<option value='$i'>$i</option>";
                          }
                      ?>
                    </select>
                  </label>


                </div>
              </div>
              <div class="col-md">
                <div class="form-group">
                  <label for="id_label_single2">Jurusan</label>
                  <div class="probootstrap_select-wrap">
                    <label for="id_label_single2" style="width: 100%;">
                    <select name="jurusan" class="js-example-basic-single js-states form-control" style="width: 100%;">
                      <option value="">Pilih Jurusan</option>
                      <?php
                        if ($jurusan->num_rows() > 0)
                        {
                          foreach ($jurusan->result() as $row)
                          {
                            echo "<option value='".$row->kd_jurusan."'>$row->nama_jurusan</option>";    
                          }
                        }      
                      ?>
                    </select>
                  </label>
                  </div>
                </div>
              </div>
            </div>
            <!-- END row -->
            
            <div class="row">
              <div class="col-md">
                <!-- <input type="submit" value="Cari Alumni" class="btn btn-primary btn-block"> -->
                <button name="cari" type="submit" id="btn-filter" class="btn btn-primary btn-block">Cari Alumni</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<!-- END section -->
<script type="text/javascript">
  $( "#nama" ).autocomplete({
    source: "<?php echo site_url('main/get_autocomplete/?');?>"
  });
</script>