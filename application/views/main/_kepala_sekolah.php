<section class="probootstrap_section">
  <div class="container">
    <div class="row text-center mb-5 probootstrap-animate">
      <div class="col-md-12">
        <h2 class="display-4 border-bottom probootstrap-section-heading">Kepala Sekolah</h2>
      </div>
    </div>
    
    <div class="row probootstrap-animate">
      <div class="col-md-12">
        <div class="owl-carousel js-owl-carousel-2">
          <?php
            foreach ($kepsek->result_array() as $p) {
          ?>
          <div>
            <div class="media probootstrap-media d-block align-items-stretch mb-4 probootstrap-animate">
              <img src="<?=base_url()?>asset/kepala_sekolah/<?=$p['foto']?>" alt="<?=$p['nama_kepala']?>" class="img-fluid">
              <div class="media-body" style="text-align: center;">
                <h5 class="mb-3"><?=$p['nama_kepala']?></h5>
                <p>NIP : <?=$p['nip']?></p>
                <p>Periode : <?=$p['periode']?></p>
              </div>
            </div>
          </div>
          <?php } ?>
          <!-- END slide item -->          
        </div>
      </div>
    </div>
  </div>
</section>
<!-- END section -->