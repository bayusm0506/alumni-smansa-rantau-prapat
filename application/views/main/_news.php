<section class="probootstrap_section bg-light">
  <div class="container">
    <div class="row text-center mb-5 probootstrap-animate">
      <div class="col-md-12">
        <h2 class="display-4 border-bottom probootstrap-section-heading">News</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">

        <div class="media probootstrap-media d-flex align-items-stretch mb-4 probootstrap-animate">
          <div class="probootstrap-media-image" style="background-image: url(<?=base_url()?>asset/frontend/images/img_1.jpg)">
          </div>
          <div class="media-body">
            <span class="text-uppercase">January 1st 2018</span>
            <h5 class="mb-3">Travel To United States Without Visa</h5>
            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
            <p><a href="#">Read More</a></p>
          </div>
        </div>

        <div class="media probootstrap-media d-flex align-items-stretch mb-4 probootstrap-animate">
          <div class="probootstrap-media-image" style="background-image: url(<?=base_url()?>asset/frontend/images/img_2.jpg)">
          </div>
          <div class="media-body">
            <span class="text-uppercase">January 1st 2018</span>
            <h5 class="mb-3">Travel To United States Without Visa</h5>
            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
            <p><a href="#">Read More</a></p>
          </div>
        </div>

      </div>
      <div class="col-md-6">
        
        <div class="media probootstrap-media d-flex align-items-stretch mb-4 probootstrap-animate">
          <div class="probootstrap-media-image" style="background-image: url(<?=base_url()?>asset/frontend/images/img_4.jpg)">
          </div>
          <div class="media-body">
            <span class="text-uppercase">January 1st 2018</span>
            <h5 class="mb-3">Travel To United States Without Visa</h5>
            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
            <p><a href="#">Read More</a></p>
          </div>
        </div>

        <div class="media probootstrap-media d-flex align-items-stretch mb-4 probootstrap-animate">
          <div class="probootstrap-media-image" style="background-image: url(<?=base_url()?>asset/frontend/images/img_5.jpg)">
          </div>
          <div class="media-body">
            <span class="text-uppercase">January 1st 2018</span>
            <h5 class="mb-3">Travel To United States Without Visa</h5>
            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
            <p><a href="#">Read More</a></p>
          </div>
        </div>

      </div>
    </div>
  </div>
</section>
<!-- END section -->