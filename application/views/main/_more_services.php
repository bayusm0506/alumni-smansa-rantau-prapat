<section class="probootstrap_section bg-light">
  <div class="container">
    <div class="row text-center mb-5 probootstrap-animate">
      <div class="col-md-12">
        <h2 class="display-4 border-bottom probootstrap-section-heading">More Services</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">

        <div class="media probootstrap-media d-flex align-items-stretch mb-4 probootstrap-animate">
          <div class="probootstrap-media-image" style="background-image: url(<?=base_url()?>asset/frontend/images/img_1.jpg)">
          </div>
          <div class="media-body">
            <h5 class="mb-3">01. Service Title Here</h5>
            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
          </div>
        </div>

        <div class="media probootstrap-media d-flex align-items-stretch mb-4 probootstrap-animate">
          <div class="probootstrap-media-image" style="background-image: url(<?=base_url()?>asset/frontend/images/img_2.jpg)">
          </div>
          <div class="media-body">
            <h5 class="mb-3">02. Service Title Here</h5>
            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
          </div>
        </div>

      </div>
      <div class="col-md-6">
        
        <div class="media probootstrap-media d-flex align-items-stretch mb-4 probootstrap-animate">
          <div class="probootstrap-media-image" style="background-image: url(<?=base_url()?>asset/frontend/images/img_4.jpg)">
          </div>
          <div class="media-body">
            <h5 class="mb-3">03. Service Title Here</h5>
            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
          </div>
        </div>

        <div class="media probootstrap-media d-flex align-items-stretch mb-4 probootstrap-animate">
          <div class="probootstrap-media-image" style="background-image: url(<?=base_url()?>asset/frontend/images/img_5.jpg)">
          </div>
          <div class="media-body">
            <h5 class="mb-3">04. Service Title Here</h5>
            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
          </div>
        </div>

      </div>
    </div>
  </div>
</section>
<!-- END section -->