<section class="probootstrap-cover overflow-hidden relative"  style="background-image: url('<?=base_url()?>asset/frontend/images/bg_1.jpg');" data-stellar-background-ratio="0.5" id="section-home">
  <div class="overlay"></div>
  <div class="container">
    <div class="row align-items-center text-center">
      <div class="col-md">
        <h2 class="heading mb-2 display-4 font-light probootstrap-animate">Gallery</h2>
      </div> 
    </div>
  </div>
</section>

<section class="probootstrap_section bg-light">
  <div class="container">
    <div class="row text-center mb-5 probootstrap-animate">
      <div class="col-md-12">
        <h2 class="display-4 border-bottom probootstrap-section-heading"><?=getTitle()?></h2>
      </div>
    </div>
    <div class="row">
      <?php
      	foreach ($gallery as $p) {
      ?>
      <div class="col-md-4">
        <div class="media probootstrap-media d-block align-items-stretch mb-4 probootstrap-animate">
          <a href="<?=base_url()?>asset/img_galeri/<?=$p['gbr_gallery']?>" title="<?=$p['jdl_gallery']?>" data-gallery>
          		<img src="<?=base_url()?>asset/img_galeri/<?=$p['gbr_gallery']?>" alt="<?=$p['jdl_gallery']?>" class="img-fluid">
          </a>
          <div class="media-body">
            <h5 class="mb-3" style="text-align: center;"><?=$p['jdl_gallery']?></h5>
            <?=$p['keterangan']?> <br />
            <p style="color: red;">Tanggal : <?=tgl_indo($p['tgl_gallery'])?></p>
          </div>
        </div>
      </div>
      <?php } ?>
      <div id="blueimp-gallery" class="blueimp-gallery">
			<div class="slides"></div>
			<h3 class="title"></h3>
			<a class="prev" style="color:#fff">‹</a>
			<a class="next" style="color:#fff">›</a>
			<a class="close" style="color:#fff">×</a>
			<a class="play-pause"></a>
			<ol class="indicator"></ol>
		</div>
    </div>
  </div>
</section>
<!-- END section -->