<?php include "_search.php"; ?>
<section class="probootstrap_section bg-light">
  <div class="container">
    <?php
        $cekHeadline = $headline->num_rows();
        if ($cekHeadline != '') {
      ?>
    <div class="row text-center mb-5 probootstrap-animate">
      <div class="col-md-12">
        <h2 class="display-4 border-bottom probootstrap-section-heading">Headline</h2>
      </div>
    </div>
    <div class="row mb-2">
      <?php
        foreach ($headline->result_array() as $p) {
          $isi_berita =(strip_tags($p['isi_berita'])); 
          $isi = substr($isi_berita,0,40); 
          $isi = substr($isi_berita,0,strrpos($isi," ")); 
          if (trim($isi)==''){
            $isi_artikel = 'Maaf, Tidak Ada ditemukan Informasi Dalam bentuk Teks, Silahkan Melihat Detail Informasi..';
          }else{
            $isi_artikel = $isi.' ....';
          }
      ?>
          <div class="col-md-6">
            <div class="card flex-md-row mb-4 box-shadow h-md-250">
              <div class="well">
                  <div class="media">
                    <a class="pull-left" href="<?=base_url()?>blog/detail/<?=$p['judul_seo']?>">
                      <?php
                        if ($p['gambar'] != '') {
                          echo "<img class='media-object' src='".base_url()."asset/foto_berita/".$p['gambar']."' width='150px' height='150px' alt='".$p['judul']."'>";
                        }else{
                          echo "<img class='media-object' src='".base_url()."asset/frontend/images/img_1.jpg' width='150px' height='150px' alt='".$p['judul']."'>";
                        }
                      ?>
                    </a>
                    <div class="media-body d-flex flex-column align-items-start" style="padding: 10px">
                      <h6 class="mb-0">
                        <a style="color: red;" href="<?=base_url()?>blog/detail/<?=$p['judul_seo']?>"><?=$p['judul']?></a>
                      </h6>
                      <div class="mb-1 text-muted" style="font-size: 10px;"><?=$p['hari'].', '.tgl_indo($p['tanggal']).', '.$p['jam'].' WIB'?></div>
                      <p><?=$isi_artikel?></p>
                      <a href="<?=base_url()?>blog/detail/<?=$p['judul_seo']?>" style="font-size: 16px;">Continue reading</a>
                   </div>
                </div>
              </div>
            </div>
          </div>
      <?php } ?>
    </div>
    <?php } ?>
    <div class="col-md-12" style="text-align: center;">
      <h3>Blog Utama</h3>
    </div>
    <div class="row mb-2">
      <div class="col-md-8">
          <?php
            $cekData = $utama->num_rows();
            if ($cekData != '') {
              foreach ($utama->result_array() as $p){
                $isi_berita =(strip_tags($p['isi_berita'])); 
                $isi = substr($isi_berita,0,40); 
                $isi = substr($isi_berita,0,strrpos($isi," ")); 
                if (trim($isi)==''){
                  $isi_artikel = 'Maaf, Tidak Ada ditemukan Informasi Dalam bentuk Teks, Silahkan Melihat Detail Informasi..';
                }else{
                  $isi_artikel = $isi.' ....';
                }
          ?>
          <div class="card flex-md-row mb-4 box-shadow h-md-250">
            <div class="well">
                <div class="media">
                  <a class="pull-left" href="<?=base_url()?>blog/detail/<?=$p['judul_seo']?>">
                    <?php
                      if ($p['gambar'] != '') {
                        echo "<img class='media-object' src='".base_url()."asset/foto_berita/".$p['gambar']."' width='150px' height='150px' alt='".$p['judul']."'>";
                      }else{
                        echo "<img class='media-object' src='".base_url()."asset/frontend/images/img_1.jpg' width='150px' height='150px' alt='".$p['judul']."'>";
                      }
                    ?>
                  </a>
                  <div class="media-body d-flex flex-column align-items-start" style="padding: 10px">
                    <h6 class="mb-0">
                      <a style="color: red;" href="<?=base_url()?>blog/detail/<?=$p['judul_seo']?>"><?=$p['judul']?></a>
                    </h6>
                    <div class="mb-1 text-muted" style="font-size: 10px;"><?=$p['hari'].', '.tgl_indo($p['tanggal']).', '.$p['jam'].' WIB'?></div>
                    <p><?=$isi_artikel?></p>
                    <a href="<?=base_url()?>blog/detail/<?=$p['judul_seo']?>" style="font-size: 16px;">Continue reading</a>
                 </div>
              </div>
            </div>
          </div>
          <?php
              } 
            }else{
            echo "<h3>Data Blog Belum Tersedia</h3>";
          } ?>
          <div class="pagination">
            <?php echo $this->pagination->create_links(); ?>
          </div>
      </div>
      <div class="col-md-4">
        <?php
          $cekAds = $ads->num_rows();
          if ($cekAds > 0) {
            $_array = $ads->result_array();
            foreach ($_array as $p) {
              echo "<img src='".base_url('asset/img_ads/'.$p['gbr_ads'])."' alt='".$p['jdl_ads']."'><br /><br />";
            }
          }else{
            echo "<img src='".base_url('asset/img_ads/kanan.jpg')."' alt='Ads'>";
          }
        ?>
      </div>
    </div>
  </div>
</section>