<section class="probootstrap-cover overflow-hidden relative"  style="background-image: url('<?=base_url()?>asset/frontend/images/bg_1.jpg');" data-stellar-background-ratio="0.5" id="section-home">
  <div class="overlay"></div>
  <div class="container">
    <div class="row align-items-center text-center">
      <div class="col-md">
        <h2 class="heading mb-2 display-4 font-light probootstrap-animate"><?=$record['judul']?></h2>
      </div> 
    </div>
  </div>

</section>

<!-- END section -->
<section class="probootstrap_section bg-light" id="section-contact">
  <div class="container">
    <div class="row">
      <div class="col-md-6 probootstrap-animate">
        <h5 style="color: red;"><?=$record['judul']?></h5>
        <div class="mb-1 text-muted" style="font-size: 12px;font-color: green;"><?=$record['hari'].', '.tgl_indo($record['tanggal']).', '.$record['jam'].' WIB'?></div>
        <div class="row">
          <div class="col-md-12" style="overflow: auto;">
            <?=$record['isi_berita']?>
          </div>
        </div>
        Tag : <br /><br />
        <p class="probootstrap-animate">
          
          <?php
            $str = explode(",",$record['tag']);
            foreach ($str as $key) {
              // echo $key."<br/>";
              echo "<a href='#' class='btn btn-primary p-3 mr-3 pl-5 pr-5 text-uppercase d-lg-inline d-md-inline d-sm-block d-block mb-3'>$key</a>";
            }
            //print($str);
          ?>
          <!-- <a href="<?=site_url('contact')?>" role="button" class="btn btn-primary p-3 mr-3 pl-5 pr-5 text-uppercase d-lg-inline d-md-inline d-sm-block d-block mb-3">Kontak Kami</a>  -->
        </p>
      </div>
      <div class="col-md-6  probootstrap-animate">
        <?php
          if ($record['gambar'] != '') {
            echo "<a href='".base_url()."asset/foto_berita/$record[gambar]' title='".$record['judul']."' data-gallery><img src='".base_url()."asset/foto_berita/$record[gambar]' alt='".$record['judul']."' width='570px' height='500px'></a>";
          }else{
            //echo "<img src='".base_url()."asset/frontend/images/sq_img_4.jpg' width='570px' height='500px'>";
          }
        ?>
      </div>
      <div id="blueimp-gallery" class="blueimp-gallery">
        <div class="slides"></div>
        <h3 class="title"></h3>
        <a class="prev" style="color:#fff">‹</a>
        <a class="next" style="color:#fff">›</a>
        <a class="close" style="color:#fff">×</a>
        <a class="play-pause"></a>
        <ol class="indicator"></ol>
      </div>
    </div>
  </div>
</section>
<!-- END section -->

<script>
    $("form#table-news").submit(function(){ 
      var _form = $(this);
      //_form += $(this).val(CKEDITOR.instances[$(this).attr('isi')].getData());
      Component.run(_form.attr('action'),'POST',_form.serialize(),function(response){
        // Pesan
        if(response.status == 'error'){
          //Message
        }else if(response.status == 'info'){
          $('form#table-news')[0].reset();
          //window.setTimeout(window.location.href = response.redirect,50000);
        }
      });
      return false;
    });
</script>