<div class="span4">
  <?php
    if($this->uri->segment(3) == ''){
      echo "<aside class='left-sidebar'>";
    }else{
      echo "<aside class='right-sidebar'>";
    }
  ?>
    <div class="widget">
      <?php echo form_open('blog/index');?>
        <div class="input-append">
          <input name="kata" class="span2" id="appendedInputButton" type="text" placeholder="Type here">
          <button class="btn btn-theme" type="submit">Search</button>
        </div>
      </form>
    </div>

    <div class="widget">

      <h5 class="widgetheading">Categories</h5>

      <ul class="cat">
        <?php
          $category = $this->ListBerita_model->getGroupKategori()->result_array();
          foreach($category as $cat){
            echo "<li><i class='icon-angle-right'></i> <a href='".base_url()."blog/kategori/$cat[kategori_seo]'>$cat[nama_kategori]<span> ($cat[jumlah])</span></a></li>";
          }
        ?>
      </ul>
    </div>

    <div class="widget">
      <div class="tabs">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#one" data-toggle="tab"><i class="icon-star"></i> Popular</a></li>
          <li><a href="#two" data-toggle="tab">Recent</a></li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="one">
            <ul class="popular">
              <?php
                $populer = $this->ListBerita_model->getPopuler()->result_array();
                foreach ($populer as $p){
                  echo "
                    <li>
                      <img src='".base_url()."asset/foto_berita/$p[gambar]' alt='$p[judul]' class='thumbnail pull-left' width='65px' height='65px'/>
                      <p><a href='".base_url()."blog/detail/$p[judul_seo]'>$p[judul]</a></p>
                      <span>".tgl_indo($p['tanggal'])."</span>
                    </li>
                  ";
                }
              ?>
            </ul>
          </div>
          <div class="tab-pane" id="two">
            <ul class="recent">
              <?php
                $recent = $this->ListBerita_model->getRecent()->result_array();
                foreach ($recent as $p){
                  echo "
                    <li>
                      <p><a id='recent' href='".base_url()."blog/detail/$p[judul_seo]'>$p[judul]</a></p>
                    </li>
                  ";
                }
              ?>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </aside>
</div>