<section id="inner-headline">
  <div class="container">
    <div class="row">
      <div class="span12">
        <div class="inner-heading">
          <ul class="breadcrumb">
            <li><a href="index.html">Home</a> <i class="icon-angle-right"></i></li>
            <li><a href="#">Pages</a> <i class="icon-angle-right"></i></li>
            <li class="active"><?=ucwords($record['judul'])?></li>
          </ul>
          <h2><?=ucwords($record['judul'])?></h2>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="content">
  <div class="container">
    <div class="row">

      <div class="span6">
        <?=$record['isi_halaman']?>
      </div>
      <div class="span6">
        <style>
          div.gallery {
              margin: 5px;
              border: 1px solid #ccc;
              float: right;
              width: 250px;
          }

          div.gallery:hover {
              border: 1px solid #777;
          }

          div.gallery img {
              width: 100%;
              height: auto;
          }

          div.desc {
              padding: 15px;
              text-align: center;
          }
        </style>
        <?php 
          if ($jumGallery > 0) {
            foreach ($gallery as $p) {
        ?>
          <div class="gallery">
            <a href="<?=base_url()?>asset/page_gallery/<?=$p['gbr_pg']?>" title="<?=$p['jdl_pg']?>" data-gallery>
              <img src="<?=base_url()?>asset/page_gallery/<?=$p['gbr_pg']?>" alt="<?=$p['jdl_pg']?>">
            </a>
            <div class="desc"><?=$p['jdl_pg']?></div>
          </div>
        <?php 
            }
          } 
        ?>
        
        
        <div id="blueimp-gallery" class="blueimp-gallery">
          <div class="slides"></div>
          <h3 class="title"></h3>
          <a class="prev" style="color:#fff">‹</a>
          <a class="next" style="color:#fff">›</a>
          <a class="close" style="color:#fff">×</a>
          <a class="play-pause"></a>
          <ol class="indicator"></ol>
        </div>
      </div>

    </div>

    <!-- divider -->
    <div class="row">
      <div class="span12">
        <div class="solidline"></div>
      </div>
    </div>
    <!-- end divider -->

  </div>
</section>