<section class="probootstrap-cover overflow-hidden relative"  style="background-image: url('<?=base_url()?>asset/frontend/images/bg_1.jpg');" data-stellar-background-ratio="0.5" id="section-home">
  <div class="overlay"></div>
  <div class="container">
    <div class="row align-items-center text-center">
      <div class="col-md">
        <h2 class="heading mb-2 display-4 font-light probootstrap-animate">Kontak Kami <br /><?php //echo getTitle(); ?></h2>
      </div> 
    </div>
  </div>

</section>

<!-- END section -->
<section class="probootstrap_section bg-light" id="section-contact">
  <div class="container">
    <div style="margin-bottom: 50px;">
      <iframe width="100%" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="<?=$identity['maps']?>"></iframe>
    </div>
    <div class="row">
      <div class="col-md-6 probootstrap-animate">
        <p class="mb-5"><?=$identity['moto']?> </p>
        <div class="row">
          <div class="col-md-6">
            <ul class="probootstrap-contact-details">
              <li>
                <span class="text-uppercase"><span class="ion-paper-airplane"></span> Email</span>
                <?=$identity['email']?>
              </li>
              <li>
                <span class="text-uppercase"><span class="ion-ios-telephone"></span> Phone</span>
                <?=$identity['whatsapp']?>
              </li>
            </ul>
          </div>
          <div class="col-md-6">
            <ul class="probootstrap-contact-details">
              <li>
                <span class="text-uppercase"><span class="ion-ios-telephone"></span> Telp</span>
                <?=$identity['no_telp']?> 
              </li>
              <li>
                <span class="text-uppercase"><span class="ion-location"></span> Address</span>
                <?=$identity['address']?>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-md-6  probootstrap-animate">
        <?php echo form_open('main/validateForm', array('id'=>'table-news', 'role'=>'form', 'class'=>'probootstrap-form probootstrap-form-box mb60 contactForm'));?>
          <div class="form-group">
            <label for="name" class="sr-only sr-only-focusable">Nama</label>
            <input type="text" class="form-control" id="name" name="nama" placeholder="Nama">
          </div>
          <div class="form-group">
            <label for="email" class="sr-only sr-only-focusable">Email</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="Email">
          </div>
          <div class="form-group">
            <label for="subject" class="sr-only sr-only-focusable">Subjek</label>
            <input type="text" class="form-control" id="subject" name="subjek"  placeholder="Subjek">
          </div>
          <div class="form-group">
            <label for="message" class="sr-only sr-only-focusable">Message</label>
            <textarea cols="30" rows="10" class="form-control" id="message" name="isi_pesan" placeholder="Write your message"></textarea>
          </div>
          <div class="form-group">
            <input type="submit" class="btn btn-primary" id="submit" value="Send Message">
          </div>
        <?php echo form_close();?>
      </div>
    </div>
  </div>
</section>
<!-- END section -->

<script>
    $("form#table-news").submit(function(){ 
      var _form = $(this);
      //_form += $(this).val(CKEDITOR.instances[$(this).attr('isi')].getData());
      Component.run(_form.attr('action'),'POST',_form.serialize(),function(response){
        // Pesan
        if(response.status == 'error'){
          //Message
        }else if(response.status == 'info'){
          $('form#table-news')[0].reset();
          //window.setTimeout(window.location.href = response.redirect,50000);
        }
      });
      return false;
    });
</script>