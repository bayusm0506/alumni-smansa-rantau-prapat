<section class="probootstrap_section" id="section-city-guides">
  <div class="container">
    <div class="row mb-3">
      <div class="col-md-12">
        <h5 class="mb-3"><?=$title?> > JURUSAN : <?=$jurusan['nama_jurusan']?></h5>
      </div>
    </div>
    <?php 
      $cekData = $alumni->num_rows();
      if ($cekData > 0) {
        // print_r($jumlahnya);
        // exit;
    ?>
      <div class="row mb-3">
        <?php
          foreach ($alumni->result_array() as $p){
        ?>
          <div class="col-md-6">
            <div class="media probootstrap-media d-flex align-items-stretch mb-4 probootstrap-animate">
              <?php
                if ($p['foto'] == '') {
                  echo '<div class="probootstrap-media-image" style="background-image: url('.base_url().'asset/frontend/images/no-photo.png); width="270px" height="262px"></div>';
                }else{
                  echo '<div class="probootstrap-media-image" style="background-image: url('.base_url().'asset/alumni/'.$p['foto'].'); width="270px" height="262px"></div>';
                }
              ?>
              <div class="media-body">
                <a href="<?=base_url()?>main/detail/<?=$p['siswa_id']?>"><h5 class="mb-3" style="color: red;"><?=$p['nama']?></h5></a>
                <span>Tempat Lahir : <?=$p['tempat_lahir']?></span><br />
                <span>Tanggal Lahir : <?=tgl_indo($p['tgl_lahir'])?></span><br />
                <span>Stambuk : <?=$p['stambuk']?></span><br />
                <span>Jurusan : <?php if($p['id_jurusan'] == ''){echo "-";}else{echo $p['nama_jurusan'];} ?></span><br />
                <span>Gelar : <?php if($p['id_gelar'] == ''){echo "-";}else{echo $p['nama_gelar'];} ?></span><br />
                <p style="color: red;"><a href="<?=site_url('main/detail/')?><?=$p['siswa_id']?>">Keterangan Details <i class="fa fa-info-circle" aria-hidden="true"></i></a></p>
              </div>
            </div>
          </div>
        <?php } ?>
      </div>
    <?php 
        }else{
    ?>
        <div class="row text-center mb-5 probootstrap-animate">
          <div class="col-md-12">
            <h2 class="display-4 border-bottom probootstrap-section-heading">Tidak Ada Data Untuk Kategori Pencarian Ini</h2>
          </div>
        </div>
    <?php
        }   
    ?>
    <div class="pagination">
        <?php echo $this->pagination->create_links(); ?>
      </div>
  </div>
</section>