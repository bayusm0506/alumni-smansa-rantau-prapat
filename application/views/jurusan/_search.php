<section class="probootstrap-cover overflow-hidden relative"  style="background-image: url('<?=base_url()?>asset/frontend/images/bg_1.jpg');" data-stellar-background-ratio="0.5"  id="section-home">
  <div class="overlay"></div>
  <div class="container">
    <div class="row align-items-center">
      <div class="col-md">
        <h2 class="heading mb-2 display-4 font-light probootstrap-animate">Temukan Teman Alumnimu Disini</h2>
        <p class="lead mb-5 probootstrap-animate">Berharap kamu akan terus terhubung satu sama lain</p>
        <p class="probootstrap-animate">
          <a href="<?=site_url('contact')?>" role="button" class="btn btn-primary p-3 mr-3 pl-5 pr-5 text-uppercase d-lg-inline d-md-inline d-sm-block d-block mb-3">Kontak Kami</a> 
        </p>
      </div> 
      <div class="col-md probootstrap-animate">
        <?php echo form_open('jurusan/index', array('class'=>'probootstrap-form'));?>
        <!-- <form method="get" action=""> -->
          <div class="form-group">
            <div class="row mb-5">
              <div class="col-md">
                <div class="form-group">
                  <label for="probootstrap-date-departure">Nama Alumni</label>
                  <div class="probootstrap-date-wrap"> 
                    <input name="nama" type="text" class="form-control" placeholder="" id="nama">
                  </div>
                </div>
              </div>
            </div>
            <!-- END row -->
            <div class="row">
              <div class="col-md">
                <!-- <input type="submit" value="Cari Alumni" class="btn btn-primary btn-block"> -->
                <button name="cari" type="submit" id="btn-filter" class="btn btn-primary btn-block">Cari Alumni</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<!-- END section -->
<script type="text/javascript">
  $( "#nama" ).autocomplete({
    source: "<?php echo site_url('main/get_autocomplete/?');?>"
  });
</script>