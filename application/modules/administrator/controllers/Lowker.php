<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Lowker extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//$this->Ion_auth_model->Authenticate();
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('administrator/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			//Success
		}
	}

	private function actionDashboard(){
		$cek = $this->Dashboard_model->getUserDescription($this->session->id_groups);
		$row = $cek->row_array();
		$this->data['first_name'] = $this->session->first_name;
		$this->data['description'] = $row['description'];
		$this->data['info_messages'] = $this->Dashboard_model->info_getMessages();
		$this->data['messages'] = $this->Dashboard_model->new_message(10);
		$this->data['record'] = $this->MenuUtama_model->identitas()->row_array();
		$this->data['fav'] = $this->MenuUtama_model->favicon()->row_array();
	}

    /* Function Action Lowongan Kerja */
    public function index(){
		$this->data['title'] = 'Lowongan Kerja';
		self::actionDashboard();
		
		$this->_render_page('layouts/main_header', $this->data);
		$this->_render_page('lowker' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->_render_page('layouts/main_footer', $this->data);
	}

	public function get_autocomplete_judul(){
        if (isset($_GET['term'])) {
            $result = $this->Lowker_model->autocomplete($_GET['term']);
            if (count($result) > 0) {
            foreach ($result as $row)
                $arr_result[] = $row->judul;
                echo json_encode($arr_result);
            }
        }
    }

    public function get_autocomplete_nama_perusahaan(){
        if (isset($_GET['term'])) {
            $result = $this->Lowker_model->autocomplete_np($_GET['term']);
            if (count($result) > 0) {
            foreach ($result as $row)
                $arr_result[] = $row->nama_perusahaan;
                echo json_encode($arr_result);
            }
        }
    }

	public function lowker_ajax_list()
	{
		$list = $this->Lowker_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $lowker) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $lowker->judul;
			$row[] = $lowker->nama_perusahaan;
			$row[] = tgl_indo($lowker->deadline);
			$row[] = '<a title="$row[file_pendukung]" href="'.base_url().'asset/files/'.$lowker->file_pendukung.'" target="_blank">Download</a>';

			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_lowker('."'".$lowker->id_lowongan."'".')"><i class="glyphicon glyphicon-edit"></i> Edit</a>
				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_lowker('."'".$lowker->id_lowongan."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->Lowker_model->count_all(),
						"recordsFiltered" => $this->Lowker_model->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function save(){

        $this->_validate();

		$data = array(
            'judul'=>$this->db->escape_str($this->input->post('judul')),
            'judul_seo'=>seo_title($this->input->post('judul')),
            'nama_perusahaan'=>$this->db->escape_str($this->input->post('nama_perusahaan')),
            'deskripsi_perusahaan'=>$this->input->post('deskripsi'),
            'posisi'=>$this->db->escape_str($this->input->post('posisi')),
            'deadline'=>$this->input->post('deadline'),
            'keterangan'=>$this->input->post('keterangan'),
            'tanggal_posting'=>date('Y-m-d'),
            'username'=>$this->session->username
		);

		if(!empty($_FILES['file_pendukung']['name']))
		{
			$upload = $this->_do_upload();
			$data['file_pendukung'] = $upload;
		}

		$insert = $this->Lowker_model->save($data);

		echo json_encode(array("status" => 'info', 'msg'=>"Data Berhasil Disimpan"));
	}

	private function _do_upload()
	{
		$config['upload_path']          = './asset/files/';
        $config['allowed_types']        = 'gif|jpg|png|zip|rar|pdf|doc|docx|ppt|pptx|xls|xlsx|txt';
        $config['max_size']             = 25000; //set max size allowed in Kilobyte
        //$config['max_width']            = 1000; // set max width image allowed
        //$config['max_height']           = 1000; // set max height allowed
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name

        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('file_pendukung')) //upload and validate
        {
            $data['inputerror'][] = 'file_pendukung';
			$data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
			$data['status'] = FALSE;
			echo json_encode($data);
			exit();
		}
		return $this->upload->data('file_name');
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('judul') == '')
		{
			$data['inputerror'][] = 'judul';
			$data['error_string'][] = 'Judul Harus Diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('nama_perusahaan') == '')
		{
			$data['inputerror'][] = 'nama_perusahaan';
			$data['error_string'][] = 'Perusahaan Harus Diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('posisi') == '')
		{
			$data['inputerror'][] = 'posisi';
			$data['error_string'][] = 'Posisi Harus Diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('deadline') == '')
		{
			$data['inputerror'][] = 'deadline';
			$data['error_string'][] = 'Deadline Harus Diisi';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	public function ajax_edit($id)
	{
		$data = $this->Lowker_model->get_by_id($id);
		$data->deskripsi_perusahaan = ($data->deskripsi_perusahaan == '...') ? '' : $data->deskripsi_perusahaan;
		$data->deadline = ($data->deadline == '0000-00-00') ? '' : $data->deadline; // if 0000-00-00 set tu empty for datepicker compatibility
		echo json_encode($data);
	}

	public function edit(){
        $this->_validate();
		$data = array(
			'judul'=>$this->db->escape_str($this->input->post('judul')),
            'judul_seo'=>seo_title($this->input->post('judul')),
            'nama_perusahaan'=>$this->db->escape_str($this->input->post('nama_perusahaan')),
            'deskripsi_perusahaan'=>$this->input->post('deskripsi'),
            'posisi'=>$this->db->escape_str($this->input->post('posisi')),
            'deadline'=>$this->input->post('deadline'),
            'keterangan'=>$this->input->post('keterangan'),
            'tanggal_posting'=>date('Y-m-d'),
            'username'=>$this->session->username
		);

		if($this->input->post('remove_file')) // if remove File checked
		{
			if(file_exists('./asset/files/'.$this->input->post('remove_file')) && $this->input->post('remove_file'))
				unlink('./asset/files/'.$this->input->post('remove_file'));
			$data['file_pendukung'] = '';
		}

		if(!empty($_FILES['file_pendukung']['name']))
		{
			$upload = $this->_do_upload();

			//delete file
			$lowker = $this->Lowker_model->get_by_id($this->input->post('id'));
			
			if(file_exists('./asset/files'.$lowker->file_pendukung) && $lowker->file_pendukung)
				unlink('./asset/files/'.$lowker->file_pendukung);

			$data['file_pendukung'] = $upload;
		}

		$this->Lowker_model->update(array('id_lowongan' => $this->input->post('id')), $data);
		//echo json_encode(array("status" => TRUE));
		echo json_encode(array("status" => 'info', 'msg'=>"Data Berhasil Diperbarui"));
	}

	public function delete(){
		if($_POST['empid']) {
			$lowker = $this->Lowker_model->get_by_id($_POST['empid']);
				unlink('./asset/files/'.$lowker->file_pendukung);
			$resultset = $this->Lowker_model->delete($_POST['empid']);
			if($resultset) {
				echo "Record Deleted";
			}
		}
	}
	/* End Function Action Lowongan Kerja*/

    /**
	 * @param string     $view
	 * @param array|null $data
	 * @param bool       $returnhtml
	 *
	 * @return mixed
	 */
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{
		//$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);
		//$view_html = $this->template->load('template', $view, $this->viewdata, $returnhtml);
		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}
}
