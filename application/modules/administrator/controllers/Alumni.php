<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Alumni extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//$this->Ion_auth_model->Authenticate();
		$this->load->library('ciqrcode'); //pemanggilan library QR CODE

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('administrator/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			//Success
		}
	}

	private function actionDashboard(){
		$cek = $this->Dashboard_model->getUserDescription($this->session->id_groups);
		$row = $cek->row_array();
		$this->data['first_name'] = $this->session->first_name;
		$this->data['description'] = $row['description'];
		$this->data['info_messages'] = $this->Dashboard_model->info_getMessages();
		$this->data['messages'] = $this->Dashboard_model->new_message(10);
		$this->data['record'] = $this->MenuUtama_model->identitas()->row_array();
		$this->data['fav'] = $this->MenuUtama_model->favicon()->row_array();
	}

    /* Function Action Alumni */
    public function index(){
		$this->data['title'] = 'Data Alumni';
		self::actionDashboard();
		$this->data['jurusan'] = $this->Jurusan_model->jurusan();
		$this->data['gelar'] = $this->Gelar_model->gelar();
		
		$this->_render_page('layouts/main_header', $this->data);
		$this->_render_page('alumni' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->_render_page('layouts/main_footer', $this->data);
	}

	public function get_autocomplete(){
        if (isset($_GET['term'])) {
            $result = $this->Alumni_model->autocomplete($_GET['term']);
            if (count($result) > 0) {
            foreach ($result as $row)
                $arr_result[] = $row->nama;
                echo json_encode($arr_result);
            }
        }
    }

	public function ajax_list()
	{
		$list = $this->Alumni_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $alumni) {
			$cmenu = $this->Gelar_model->menu_cek($alumni->id_gelar)->row_array();
			if ($cmenu['id_gelar']=='') {$gelar = '#';}else{$gelar = $cmenu['nama_gelar'];}

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $alumni->nama;
			$row[] = $alumni->tempat_lahir;
			$row[] = tgl_indo($alumni->tgl_lahir);
			$row[] = $alumni->alamat_baru;
			$row[] = $alumni->stambuk;
			$row[] = $alumni->tahun_lulus;
			$row[] = $this->Jurusan_model->lookup($alumni->id_jurusan);
			$row[] = $gelar;
			if(!empty($alumni->foto))
				$row[] = '<a href="'.base_url('./asset/alumni/').$alumni->foto.'" target="_blank"><img width="40" height="40" class="img-circle" src="'.base_url('./asset/alumni/').$alumni->foto.'"></a>';
			else
				$row[] = '(No photo)';

			$row[] = $alumni->kd_anggota;
			if(!empty($alumni->qr_code))
				$row[] = '<a href="'.base_url('./asset/images/qrcode/').$alumni->qr_code.'" target="_blank"><img width="40" height="40" class="img-circle" src="'.base_url('./asset/images/qrcode/').$alumni->qr_code.'"></a>';
			else
				$row[] = '(No QR Code)';
			$row[] = tgl_indo($alumni->tgl_terdaftar);
			$invID = str_pad($alumni->siswa_id, 4, '0', STR_PAD_LEFT);
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_alumni('."'".$alumni->siswa_id."'".')"><i class="glyphicon glyphicon-edit"></i></a>
				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_alumni('."'".$alumni->siswa_id."'".')"><i class="glyphicon glyphicon-trash"></i></a>
				  <a class="btn btn-sm btn-success" href="javascript:void(0)" title="Upload" onclick="upload('."'".$alumni->siswa_id."'".')"><i class="glyphicon glyphicon-upload"></i></a>';
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->Alumni_model->count_all(),
						"recordsFiltered" => $this->Alumni_model->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function save(){

        $this->_validate();

		$data = array(
            'nama'=>$this->db->escape_str($this->input->post('nama')),
          	'tempat_lahir'=>$this->input->post('tempat_lahir'),
          	'tgl_lahir'=>$this->input->post('tgl_lahir'),
          	'alamat_lama'=>$this->input->post('alamat_lama'),
          	'alamat_baru'=>$this->input->post('alamat_baru'),
          	'stambuk'=>$this->input->post('stambuk'),
          	'id_jurusan'=>$this->input->post('id_jurusan'),
          	'tahun_lulus'=>$this->input->post('tahun_lulus'),
          	'tahun_pindah'=>$this->input->post('tahun_pindah'),
          	'sekolah_pindah'=>$this->input->post('sekolah_pindah'),
          	'id_gelar'=>$this->input->post('id_gelar'),
          	'profesi'=>$this->input->post('profesi'),
          	'nama_keluarga'=>$this->input->post('nama_keluarga'),
          	'profesi_keluarga'=>$this->input->post('profesi_keluarga'),
          	'hobi'=>$this->input->post('hobi'),
          	'facebook'=>$this->input->post('facebook'),
          	'instagram'=>$this->input->post('instagram'),
          	'twitter'=>$this->input->post('twitter'),
          	'youtube'=>$this->input->post('youtube'),
          	'tgl_input'=>date('Y-m-d'),
          	'tgl_terdaftar'=>$this->input->post('tgl_terdaftar'),
          	'username'=>$this->session->username
		);

		

		// $cekData = $this->Alumni_model->cekData($this->input->post('stambuk'), $this->input->post('id_jurusan'))->num_rows();
		// if ($cekData > 0) {
		// 	$hasil = $this->Alumni_model->cekData($this->input->post('stambuk'), $this->input->post('id_jurusan'))->row_array();
		// 	$data['kd_anggota'] = $this->input->post('stambuk').$this->input->post('id_jurusan').$hasil['jumlah_data']+1;
		// }else{
		// 	$data['kd_anggota'] = $this->input->post('stambuk').$this->input->post('id_jurusan').'1';
		// }

		$cekData = $this->Alumni_model->max_id_siswa();
		$row = $cekData->row();
		// print_r($row);
		// exit;
		$ex_date = explode("-", $this->input->post('tgl_terdaftar'));
		$kode_anggota = str_pad($row->id+1, 4, '0', STR_PAD_LEFT)."-".substr($this->input->post('stambuk'), -2).substr($this->input->post('tahun_lulus'), -2)."-".$ex_date[1].substr($ex_date[0], -2);
		$data['kd_anggota'] = $kode_anggota;
		$data['id'] = $row->id+1;

		$qrcode['cacheable']	= true; //boolean, the default is true
		$qrcode['cachedir']		= './asset/'; //string, the default is application/cache/
		$qrcode['errorlog']		= './asset/'; //string, the default is application/logs/
		$qrcode['imagedir']		= './asset/images/qrcode/'; //direktori penyimpanan qr code
		$qrcode['quality']		= true; //boolean, the default is true
		$qrcode['size']			= '1024'; //interger, the default is 1024
		$qrcode['black']		= array(224,255,255); // array, default is array(255,255,255)
		$qrcode['white']		= array(70,130,180); // array, default is array(0,0,0)
		$this->ciqrcode->initialize($qrcode);

		$image_name=$kode_anggota.'.png'; //buat name dari qr code sesuai dengan nim
		$data['qr_code'] = $image_name;
		$params['data'] = $kode_anggota; //data yang akan di jadikan QR CODE
		$params['level'] = 'H'; //H=High
		$params['size'] = 10;
		$params['savename'] = FCPATH.$qrcode['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
		$this->ciqrcode->generate($params); // fungsi untuk generate QR CODE

		if(!empty($_FILES['photo']['name']))
		{
			$upload = $this->_do_upload();
			$data['foto'] = $upload;
		}

		

		$insert = $this->Alumni_model->save($data);

		echo json_encode(array("status" => 'info', 'msg'=>"Data Berhasil Disimpan"));
	}

	private function _do_upload()
	{
		$config['upload_path']          = './asset/alumni/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 3000; //set max size allowed in Kilobyte
        //$config['max_width']            = 1000; // set max width image allowed
        //$config['max_height']           = 1000; // set max height allowed
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name

        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('photo')) //upload and validate
        {
            $data['inputerror'][] = 'photo';
			$data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
			$data['status'] = FALSE;
			echo json_encode($data);
			exit();
		}
		return $this->upload->data('file_name');
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('nama') == '')
		{
			$data['inputerror'][] = 'nama';
			$data['error_string'][] = 'Nama Tidak Boleh Kosong';
			$data['status'] = FALSE;
		}

		// if($this->input->post('tempat_lahir') == '')
		// {
		// 	$data['inputerror'][] = 'tempat_lahir';
		// 	$data['error_string'][] = 'Tempat Lahir Tidak Boleh Kosong';
		// 	$data['status'] = FALSE;
		// }

		// if($this->input->post('tgl_lahir') == '')
		// {
		// 	$data['inputerror'][] = 'tgl_lahir';
		// 	$data['error_string'][] = 'Tanggal Lahir Tidak Boleh Kosong';
		// 	$data['status'] = FALSE;
		// }

		// if($this->input->post('alamat_lama') == '')
		// {
		// 	$data['inputerror'][] = 'alamat_lama';
		// 	$data['error_string'][] = 'Alamat Lama Tidak Boleh Kosong';
		// 	$data['status'] = FALSE;
		// }

		// if($this->input->post('alamat_baru') == '')
		// {
		// 	$data['inputerror'][] = 'alamat_baru';
		// 	$data['error_string'][] = 'Alamat Baru Tidak Boleh Kosong';
		// 	$data['status'] = FALSE;
		// }

		if($this->input->post('stambuk') == '')
		{
			$data['inputerror'][] = 'stambuk';
			$data['error_string'][] = 'Stambuk Tidak Boleh Kosong';
			$data['status'] = FALSE;
		}

		if($this->input->post('id_jurusan') == '')
		{
			$data['inputerror'][] = 'id_jurusan';
			$data['error_string'][] = 'Jurusan Tidak Boleh Kosong';
			$data['status'] = FALSE;
		}

		if($this->input->post('tahun_lulus') == '')
		{
			$data['inputerror'][] = 'tahun_lulus';
			$data['error_string'][] = 'Tahun Lulus Tidak Boleh Kosong';
			$data['status'] = FALSE;
		}

		if($this->input->post('tgl_terdaftar') == '')
		{
			$data['inputerror'][] = 'tgl_terdaftar';
			$data['error_string'][] = 'Tanggal Daftar Tidak Boleh Kosong';
			$data['status'] = FALSE;
		}

		// if($this->input->post('tahun_pindah') == '')
		// {
		// 	$data['inputerror'][] = 'tahun_pindah';
		// 	$data['error_string'][] = 'Tahun Pindah Tidak Boleh Kosong';
		// 	$data['status'] = FALSE;
		// }

		// if($this->input->post('sekolah_pindah') == '')
		// {
		// 	$data['inputerror'][] = 'sekolah_pindah';
		// 	$data['error_string'][] = 'Sekolah Pindah Tidak Boleh Kosong';
		// 	$data['status'] = FALSE;
		// }

		// if($this->input->post('id_gelar') == '')
		// {
		// 	$data['inputerror'][] = 'id_gelar';
		// 	$data['error_string'][] = 'Gelar Tidak Boleh Kosong';
		// 	$data['status'] = FALSE;
		// }

		// if($this->input->post('profesi') == '')
		// {
		// 	$data['inputerror'][] = 'profesi';
		// 	$data['error_string'][] = 'Profesi Tidak Boleh Kosong';
		// 	$data['status'] = FALSE;
		// }

		// if($this->input->post('nama_keluarga') == '')
		// {
		// 	$data['inputerror'][] = 'nama_keluarga';
		// 	$data['error_string'][] = 'Nama Keluarga Tidak Boleh Kosong';
		// 	$data['status'] = FALSE;
		// }

		// if($this->input->post('profesi_keluarga') == '')
		// {
		// 	$data['inputerror'][] = 'profesi_keluarga';
		// 	$data['error_string'][] = 'Profesi Keluarga Tidak Boleh Kosong';
		// 	$data['status'] = FALSE;
		// }

		// if($this->input->post('hobi') == '')
		// {
		// 	$data['inputerror'][] = 'hobi';
		// 	$data['error_string'][] = 'Hobi Tidak Boleh Kosong';
		// 	$data['status'] = FALSE;
		// }	

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	public function ajax_edit($id)
	{
		$data = $this->Alumni_model->get_by_id($id);
		$data->tgl_lahir = ($data->tgl_lahir == '0000-00-00') ? '' : $data->tgl_lahir;
		echo json_encode($data);
	}

	public function edit(){
        $this->_validate();
		$data = array(
			'nama'=>$this->db->escape_str($this->input->post('nama')),
          	'tempat_lahir'=>$this->input->post('tempat_lahir'),
          	'tgl_lahir'=>$this->input->post('tgl_lahir'),
          	'alamat_lama'=>$this->input->post('alamat_lama'),
          	'alamat_baru'=>$this->input->post('alamat_baru'),
          	'stambuk'=>$this->input->post('stambuk'),
          	'id_jurusan'=>$this->input->post('id_jurusan'),
          	'tahun_lulus'=>$this->input->post('tahun_lulus'),
          	'tahun_pindah'=>$this->input->post('tahun_pindah'),
          	'sekolah_pindah'=>$this->input->post('sekolah_pindah'),
          	'id_gelar'=>$this->input->post('id_gelar'),
          	'profesi'=>$this->input->post('profesi'),
          	'nama_keluarga'=>$this->input->post('nama_keluarga'),
          	'profesi_keluarga'=>$this->input->post('profesi_keluarga'),
          	'hobi'=>$this->input->post('hobi'),
          	'facebook'=>$this->input->post('facebook'),
          	'instagram'=>$this->input->post('instagram'),
          	'twitter'=>$this->input->post('twitter'),
          	'youtube'=>$this->input->post('youtube'),
          	'tgl_terdaftar'=>$this->input->post('tgl_terdaftar'),
          	'tgl_update'=>date('Y-m-d h:i:s'),
          	'username'=>$this->session->username
		);

		$ex_date = explode("-", $this->input->post('tgl_terdaftar'));
		$kode_anggota = str_pad($this->input->post('id_siswa'), 4, '0', STR_PAD_LEFT)."-".substr($this->input->post('stambuk'), -2).substr($this->input->post('tahun_lulus'), -2)."-".$ex_date[1].substr($ex_date[0], -2);
		$data['kd_anggota'] = $kode_anggota;

		$qrcode['cacheable']	= true; //boolean, the default is true
		$qrcode['cachedir']		= './asset/'; //string, the default is application/cache/
		$qrcode['errorlog']		= './asset/'; //string, the default is application/logs/
		$qrcode['imagedir']		= './asset/images/qrcode/'; //direktori penyimpanan qr code
		$qrcode['quality']		= true; //boolean, the default is true
		$qrcode['size']			= '1024'; //interger, the default is 1024
		$qrcode['black']		= array(224,255,255); // array, default is array(255,255,255)
		$qrcode['white']		= array(70,130,180); // array, default is array(0,0,0)
		$this->ciqrcode->initialize($qrcode);

		$image_name=$kode_anggota.'.png'; //buat name dari qr code sesuai dengan nim
		$data['qr_code'] = $image_name;
		$params['data'] = $kode_anggota; //data yang akan di jadikan QR CODE
		$params['level'] = 'H'; //H=High
		$params['size'] = 10;
		$params['savename'] = FCPATH.$qrcode['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
		$this->ciqrcode->generate($params); // fungsi untuk generate QR CODE
		

		// if (($this->input->post('stambuk_cek') != $this->input->post('stambuk')) OR ($this->input->post('id_jurusan_cek') != $this->input->post('id_jurusan'))) {
		// 	$cekData = $this->Alumni_model->cekDataTerakhir($this->input->post('stambuk'), $this->input->post('id_jurusan'))->num_rows();
		// 	if ($cekData > 0) {
		// 		$hasil = $this->Alumni_model->cekDataTerakhir($this->input->post('stambuk'), $this->input->post('id_jurusan'))->row_array();
		// 		$data['kd_anggota'] = $hasil['kd_anggota']+1;
		// 		// $hasil = $this->Alumni_model->cekData($this->input->post('stambuk'), $this->input->post('id_jurusan'))->row_array();
		// 		// $data['kd_anggota'] = $this->input->post('stambuk').$this->input->post('id_jurusan').$hasil['jumlah_data']+1;
		// 	}else{
		// 		$data['kd_anggota'] = $this->input->post('stambuk').$this->input->post('id_jurusan').'1';
		// 	}
		// }else{
		// 	$data['kd_anggota'] = $this->input->post('stambuk').$this->input->post('id_jurusan').'1';
		// }
		
		if($this->input->post('remove_photo')) // if remove photo checked
		{
			if(file_exists('./asset/alumni/'.$this->input->post('remove_photo')) && $this->input->post('remove_photo'))
				unlink('./asset/alumni/'.$this->input->post('remove_photo'));
			$data['foto'] = '';
		}

		if(!empty($_FILES['photo']['name']))
		{
			$upload = $this->_do_upload();

			//delete file
			$alumni = $this->Alumni_model->get_by_id($this->input->post('id'));
			
			if(file_exists('./asset/alumni/'.$alumni->foto) && $alumni->foto)
				unlink('./asset/alumni/'.$alumni->foto);

			$data['foto'] = $upload;
		}

		$this->Alumni_model->update(array('siswa_id' => $this->input->post('id')), $data);
		//echo json_encode(array("status" => TRUE));
		echo json_encode(array("status" => 'info', 'msg'=>"Data Berhasil Diperbarui"));
	}

	public function upload()
    {
        $id = $this->input->post('id');
        $data = $this->input->post('image');

    	$alumni = $this->Alumni_model->get_by_id($id);
		if(file_exists('./asset/alumni/'.$alumni->foto) && $alumni->foto)
			unlink('./asset/alumni/'.$alumni->foto);
     
        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);
     
        $data = base64_decode($data);
        $imageName = round(microtime(true) * 1000).'.png';
        file_put_contents('asset/alumni/'.$imageName, $data);

        $data_img['foto'] = $imageName;
        $this->Alumni_model->update(array('siswa_id' => $this->input->post('id')), $data_img);
     
        echo json_encode(array('status' => "success", 'msg'=>"Foto Berhasil Diperbaharui"));
    }

	public function delete(){
		if($_POST['empid']) {
			$alumni = $this->Alumni_model->get_by_id($_POST['empid']);
				unlink('./asset/alumni/'.$alumni->foto);
			$resultset = $this->Alumni_model->delete($_POST['empid']);
			if($resultset) {
				echo "Record Deleted";
			}
		}
	}
	/* End Function Action Alumni*/

    /**
	 * @param string     $view
	 * @param array|null $data
	 * @param bool       $returnhtml
	 *
	 * @return mixed
	 */
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{
		//$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);
		//$view_html = $this->template->load('template', $view, $this->viewdata, $returnhtml);
		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}
}
