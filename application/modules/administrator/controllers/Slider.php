<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Slider extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//$this->Ion_auth_model->Authenticate();
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('administrator/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			//Success
		}
	}

	private function actionDashboard(){
		$cek = $this->Dashboard_model->getUserDescription($this->session->user_id);
		$row = $cek->row_array();
		$this->data['first_name'] = $this->session->first_name;
		$this->data['description'] = $row['description'];
		$this->data['info_messages'] = $this->Dashboard_model->info_getMessages();
		$this->data['messages'] = $this->Dashboard_model->new_message(10);
		$this->data['record'] = $this->MenuUtama_model->identitas()->row_array();
		$this->data['fav'] = $this->MenuUtama_model->favicon()->row_array();
	}

    /* Function Action Album */
    public function index(){
		$this->data['title'] = 'Slider';
		self::actionDashboard();

		$this->_render_page('layouts/main_header', $this->data);
		$this->_render_page('slider' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->_render_page('layouts/main_footer', $this->data);
	}

	public function get_autocomplete_judul(){
        if (isset($_GET['term'])) {
            $result = $this->Slider_model->autocomplete($_GET['term']);
            if (count($result) > 0) {
            foreach ($result as $row)
                $arr_result[] = $row->jdl_slide;
                echo json_encode($arr_result);
            }
        }
    }

	public function ajax_list()
	{
		$cek = $this->Dashboard_model->getUserDescription($this->session->id_groups);
		$row = $cek->row_array();
		$list = $this->Slider_model->get_datatables($row['name']);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $slide) {
			$no++;
			$row = array();
			$row[] = $no;
			if($slide->gbr_slide)
				$row[] = '<a href="'.base_url('./asset/img_slider/').$slide->gbr_slide.'" target="_blank"><img width="40" height="40" class="img-circle" src="'.base_url('./asset/img_slider/').$slide->gbr_slide.'"></a>';
			else
				$row[] = '(No photo)';

			if($slide->bg_slide)
				$row[] = '<a href="'.base_url('./asset/img_slider/').$slide->bg_slide.'" target="_blank"><img width="40" height="40" class="img-circle" src="'.base_url('./asset/img_slider/').$slide->bg_slide.'"></a>';
			else
				$row[] = '(No Background)';

			$row[] = $slide->jdl_slide;

			if ($slide->aktif == 'Y') {
				$status = "Aktif";
			}else{
				$status = "Tidak Aktif";
			}
			$row[] = $status;
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_slide('."'".$slide->id_slide."'".')"><i class="glyphicon glyphicon-edit"></i> Edit</a>
				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_slide('."'".$slide->id_slide."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->Slider_model->count_all(),
						"recordsFiltered" => $this->Slider_model->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function export_pdfMW(){
		$data = array(
			'title' => 'Data Album',
			//'list' => $this->Slider_model->getPerdana()
		);
	    $filename = "Bayu Setra Maulana";
	    $this->load->library('pdf');

	    $this->pdf->setPaper('A4', 'potrait');
	    $this->pdf->filename = $filename.".pdf";
		$this->pdf->load_view('menu_utama' . DIRECTORY_SEPARATOR . '_exportPdfMW', $data);

	}

	public function save()
	{
		$this->_validate();

		$data = array(
            'jdl_slide'=>$this->input->post('jdl_slide'),
            'jdl_color'=>$this->input->post('jdl_color'),
            'slide_seo'=>seo_title($this->input->post('jdl_slide')),
            'keterangan'=>$this->input->post('keterangan'),
            'aktif'=>$this->input->post('status'),
            'hits_slide'=>'0',
            'tgl_posting'=>date('Y-m-d'),
            'jam'=>date('H:i:s'),
            'hari'=>hari_ini(date('w')),
            'username'=>$this->session->username
		);

		if(!empty($_FILES['photo']['name']))
		{
			$upload = $this->_do_upload();
			$data['gbr_slide'] = $upload;
		}

		if(!empty($_FILES['bg_slide']['name']))
		{
			$upload = $this->_do_upload_bg();
			$data['bg_slide'] = $upload;
		}

		$insert = $this->Slider_model->save($data);

		echo json_encode(array("status" => 'info', 'msg'=>"Data Berhasil Disimpan"));
	}

	public function ajax_edit($id)
	{
		$data = $this->Slider_model->get_by_id($id);
		echo json_encode($data);
	}

	private function _do_upload()
	{
		$config['upload_path']          = './asset/img_slider/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 3000; //set max size allowed in Kilobyte
        //$config['max_width']            = 1000; // set max width image allowed
        //$config['max_height']           = 1000; // set max height allowed
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name

        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('photo')) //upload and validate
        {
            $data['inputerror'][] = 'photo';
			$data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
			$data['status'] = FALSE;
			echo json_encode($data);
			exit();
		}
		
		return $this->upload->data('file_name');
	}

	private function _do_upload_bg()
	{
		$config['upload_path']          = './asset/img_slider/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 3000; //set max size allowed in Kilobyte
        //$config['max_width']            = 1000; // set max width image allowed
        //$config['max_height']           = 1000; // set max height allowed
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name

        $this->load->library('upload', $config);

		if(!$this->upload->do_upload('bg_slide')) //upload and validate
        {
            $data['inputerror'][] = 'bg_slide';
			$data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
			$data['status'] = FALSE;
			echo json_encode($data);
			exit();
		}
		return $this->upload->data('file_name');
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('jdl_slide') == '')
		{
			$data['inputerror'][] = 'jdl_slide';
			$data['error_string'][] = 'Judul Album Harus Diisi';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	public function edit(){
        $this->_validate();

		$data = array(
			'jdl_slide'=>$this->input->post('jdl_slide'),
			'jdl_color'=>$this->input->post('jdl_color'),
            'slide_seo'=>seo_title($this->input->post('jdl_slide')),
            'keterangan'=>$this->input->post('keterangan'),
            'aktif'=>$this->input->post('status'),
            'hits_slide'=>'0',
            'tgl_posting'=>date('Y-m-d'),
            'jam'=>date('H:i:s'),
            'hari'=>hari_ini(date('w')),
            'username'=>$this->session->username
		);

		if($this->input->post('remove_photo')) // if remove photo checked
		{
			if(file_exists('./asset/img_slider/'.$this->input->post('remove_photo')) && $this->input->post('remove_photo'))
				unlink('./asset/img_slider/'.$this->input->post('remove_photo'));
			$data['gbr_slide'] = '';
		}

		if($this->input->post('remove_photo_bg')) // if remove photo checked
		{
			if(file_exists('./asset/img_slider/'.$this->input->post('remove_photo_bg')) && $this->input->post('remove_photo_bg'))
				unlink('./asset/img_slider/'.$this->input->post('remove_photo_bg'));
			$data['bg_slide'] = '';
		}

		if(!empty($_FILES['photo']['name']))
		{
			$upload = $this->_do_upload();

			//delete file
			$slide = $this->Slider_model->get_by_id($this->input->post('id'));
			
			if(file_exists('./asset/img_slider/'.$slide->gbr_slide) && $slide->gbr_slide)
				unlink('./asset/img_slider/'.$slide->gbr_slide);

			$data['gbr_slide'] = $upload;
		}

		if(!empty($_FILES['bg_slide']['name']))
		{
			$upload = $this->_do_upload_bg();

			//delete file
			$slide = $this->Slider_model->get_by_id($this->input->post('id'));
			if(file_exists('./asset/img_slider/'.$slide->bg_slide) && $slide->bg_slide)
				unlink('./asset/img_slider/'.$slide->bg_slide);

			$data['bg_slide'] = $upload;
		}


		$this->Slider_model->update(array('id_slide' => $this->input->post('id')), $data);
		echo json_encode(array("status" => 'info', 'msg'=>"Data Berhasil Diperbarui"));
	}

	public function delete(){
		if($_POST['empid']) {
			$slide = $this->Slider_model->get_by_id($_POST['empid']);
				unlink('./asset/img_slider/'.$slide->gbr_slide);
				unlink('./asset/img_slider/'.$slide->bg_slide);
			$resultset = $this->Slider_model->delete($_POST['empid']);
			if($resultset) {
				echo "Record Deleted";
			}
		}
	}
	/* End Function Action Album */

    /**
	 * @param string     $view
	 * @param array|null $data
	 * @param bool       $returnhtml
	 *
	 * @return mixed
	 */
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{
		//$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);
		//$view_html = $this->template->load('template', $view, $this->viewdata, $returnhtml);
		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}
}
