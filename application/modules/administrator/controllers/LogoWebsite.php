<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class LogoWebsite extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//$this->Ion_auth_model->Authenticate();
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('administrator/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			//Success
		}
	}

	private function actionDashboard(){
		$cek = $this->Dashboard_model->getUserDescription($this->session->id_groups);
		$row = $cek->row_array();
		$this->data['first_name'] = $this->session->first_name;
		$this->data['description'] = $row['description'];
		$this->data['info_messages'] = $this->Dashboard_model->info_getMessages();
		$this->data['messages'] = $this->Dashboard_model->new_message(10);
		$this->data['record'] = $this->MenuUtama_model->identitas()->row_array();
		$this->data['fav'] = $this->MenuUtama_model->favicon()->row_array();
	}

    /* Function Action Logo Website */
    public function index(){
		$this->data['title'] = 'Logo Website';
		self::actionDashboard();
		$this->data['logo'] = $this->LogoWebsite_model->logo()->result_array();

		$this->_render_page('layouts/main_header', $this->data);
		$this->_render_page('logoWebsite' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->_render_page('layouts/main_footer', $this->data);
	}

	private function _do_upload()
	{
		$config['upload_path']          = './asset/logo/';
        $config['allowed_types']        = 'gif|jpg|png|JPG';
        $config['max_size']             = 2000; //set max size allowed in Kilobyte
        //$config['max_width']            = 820; // set max width image allowed
        //$config['max_height']           = 180; // set max height allowed
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name

        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('photo')) //upload and validate
        {
			$errors = $this->upload->display_errors('','');
            $response = ["status"=>'error','msg'=>$errors];
            echo json_encode($response);
            exit();
		}
		return $this->upload->data('file_name');
	}

	public function edit(){
		$data = array(
			'id_logo'=>$this->db->escape_str($this->input->post('id'))
		);

		if($this->input->post('remove_photo')) // if remove photo checked
		{
			if(file_exists('./asset/logo/'.$this->input->post('remove_photo')) && $this->input->post('remove_photo'))
				unlink('./asset/logo/'.$this->input->post('remove_photo'));
			$data['gambar'] = '';
		}

		if(!empty($_FILES['photo']['name']))
		{
			$upload = $this->_do_upload();

			//delete file
			$logo = $this->LogoWebsite_model->get_by_id($this->input->post('id'));
			
			if(file_exists('./asset/logo'.$logo->gambar) && $logo->gambar)
				unlink('./asset/logo/'.$logo->gambar);

			$data['gambar'] = $upload;
		}

		$this->LogoWebsite_model->update(array('id_logo' => $this->input->post('id')), $data);
		
		$response = ["status"=>'info','msg'=>"Data Berhasil Diperbarui",'url'=>site_url('administrator/logoWebsite')];
        echo json_encode($response);
	}

	public function edit_logo(){
		$data = array(
			'id_logo'=>$this->db->escape_str($this->input->post('id'))
		);

		if($this->input->post('remove_photo')) // if remove photo checked
		{
			if(file_exists('./asset/logo/'.$this->input->post('remove_photo')) && $this->input->post('remove_photo'))
				unlink('./asset/logo/'.$this->input->post('remove_photo'));
			$data['logo'] = '';
		}

		if(!empty($_FILES['photo']['name']))
		{
			$upload = $this->_do_upload();

			//delete file
			$logo = $this->LogoWebsite_model->get_by_id($this->input->post('id'));
			
			if(file_exists('./asset/logo'.$logo->logo) && $logo->logo)
				unlink('./asset/logo/'.$logo->logo);

			$data['logo'] = $upload;
		}

		$this->LogoWebsite_model->update(array('id_logo' => $this->input->post('id')), $data);
		
		$response = ["status"=>'info','msg'=>"Data Berhasil Diperbarui",'url'=>site_url('administrator/logoWebsite')];
        echo json_encode($response);
	}

	/* End Function Action Logo Website*/

    /**
	 * @param string     $view
	 * @param array|null $data
	 * @param bool       $returnhtml
	 *
	 * @return mixed
	 */
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{
		//$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);
		//$view_html = $this->template->load('template', $view, $this->viewdata, $returnhtml);
		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}
}
