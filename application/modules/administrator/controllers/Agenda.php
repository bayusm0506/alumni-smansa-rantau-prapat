<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Agenda extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//$this->Ion_auth_model->Authenticate();
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('administrator/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			//Success
		}
	}

	private function actionDashboard(){
		$cek = $this->Dashboard_model->getUserDescription($this->session->id_groups);
		$row = $cek->row_array();
		$this->data['first_name'] = $this->session->first_name;
		$this->data['description'] = $row['description'];
		$this->data['info_messages'] = $this->Dashboard_model->info_getMessages();
		$this->data['messages'] = $this->Dashboard_model->new_message(10);
		$this->data['record'] = $this->MenuUtama_model->identitas()->row_array();
		$this->data['fav'] = $this->MenuUtama_model->favicon()->row_array();
	}

    /* Function Action Lowongan Kerja */
    public function index(){
		$this->data['title'] = 'Agenda';
		self::actionDashboard();
		
		$this->_render_page('layouts/main_header', $this->data);
		$this->_render_page('agenda' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->_render_page('layouts/main_footer', $this->data);
	}

	public function get_autocomplete_tema(){
        if (isset($_GET['term'])) {
            $result = $this->Agenda_model->autocomplete($_GET['term']);
            if (count($result) > 0) {
            foreach ($result as $row)
                $arr_result[] = $row->tema;
                echo json_encode($arr_result);
            }
        }
    }

	public function agenda_ajax_list()
	{
		$list = $this->Agenda_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $agenda) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $agenda->tema;
			$row[] = tgl_indo($agenda->tgl_mulai);
			$row[] = tgl_indo($agenda->tgl_selesai);
			$row[] = $agenda->jam;

			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_agenda('."'".$agenda->id_agenda."'".')"><i class="glyphicon glyphicon-edit"></i> Edit</a>
				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_agenda('."'".$agenda->id_agenda."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->Agenda_model->count_all(),
						"recordsFiltered" => $this->Agenda_model->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function save(){

        $this->_validate();
        $tms = explode(" - ",$this->input->post('tanggal'));
		if( count($tms) > 0 ){
			$tanggal_mulai =  explode("-",$tms[0]); 
			$tanggal_selesai = explode("-",$tms[1]);
		}
		$data = array(
            'tema'=>$this->db->escape_str($this->input->post('tema')),
            'tema_seo'=>seo_title($this->input->post('tema')),
            'isi_agenda'=>$this->input->post('isi_agenda'),
            'tempat'=>$this->db->escape_str($this->input->post('tempat')),
            'pengirim'=>$this->db->escape_str($this->input->post('pengirim')),
            'tgl_mulai'=>$tanggal_mulai[0].'-'.$tanggal_mulai[1].'-'.$tanggal_mulai[2],
            'tgl_selesai'=>$tanggal_selesai[0].'-'.$tanggal_selesai[1].'-'.$tanggal_selesai[2],
            'tgl_posting'=>date('Y-m-d'),
            'jam'=>$this->db->escape_str($this->input->post('jam')),
            'dibaca'=>'0',
            'username'=>$this->session->username
		);

		if(!empty($_FILES['photo']['name']))
		{
			$upload = $this->_do_upload();
			$data['gambar'] = $upload;
		}

		$insert = $this->Agenda_model->save($data);

		echo json_encode(array("status" => 'info', 'msg'=>"Data Berhasil Disimpan"));
	}

	private function _do_upload()
	{
		$config['upload_path']          = './asset/foto_agenda/';
        $config['allowed_types']        = 'gif|jpg|png|JPG|JPEG';
        $config['max_size']             = 3000; //set max size allowed in Kilobyte
        //$config['max_width']            = 1000; // set max width image allowed
        //$config['max_height']           = 1000; // set max height allowed
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name

        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('photo')) //upload and validate
        {
            $data['inputerror'][] = 'photo';
			$data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
			$data['status'] = FALSE;
			echo json_encode($data);
			exit();
		}
		return $this->upload->data('file_name');
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('tema') == '')
		{
			$data['inputerror'][] = 'tema';
			$data['error_string'][] = 'Tema Harus Diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('tempat') == '')
		{
			$data['inputerror'][] = 'tempat';
			$data['error_string'][] = 'Tempat Harus Diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('pengirim') == '')
		{
			$data['inputerror'][] = 'pengirim';
			$data['error_string'][] = 'Pengirim Harus Diisi';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	public function ajax_edit($id)
	{
		$data = $this->Agenda_model->get_by_id($id);
		$data->isi_agenda = ($data->isi_agenda == '...') ? '' : $data->isi_agenda;
		echo json_encode($data);
	}

	public function edit(){
        $this->_validate();
        $tms = explode(" - ",$this->input->post('tanggal'));
		if( count($tms) > 0 ){
			$tanggal_mulai =  explode("-",$tms[0]); 
			$tanggal_selesai = explode("-",$tms[1]);
		}
		$data = array(
            'tema'=>$this->db->escape_str($this->input->post('tema')),
            'tema_seo'=>seo_title($this->input->post('tema')),
            'isi_agenda'=>$this->input->post('isi_agenda'),
            'tempat'=>$this->db->escape_str($this->input->post('tempat')),
            'pengirim'=>$this->db->escape_str($this->input->post('pengirim')),
            'tgl_mulai'=>$tanggal_mulai[0].'-'.$tanggal_mulai[1].'-'.$tanggal_mulai[2],
            'tgl_selesai'=>$tanggal_selesai[0].'-'.$tanggal_selesai[1].'-'.$tanggal_selesai[2],
            'tgl_posting'=>date('Y-m-d'),
            'jam'=>$this->db->escape_str($this->input->post('jam')),
            'dibaca'=>'0',
            'username'=>$this->session->username
		);

		if($this->input->post('remove_file')) // if remove File checked
		{
			if(file_exists('./asset/foto_agenda/'.$this->input->post('remove_file')) && $this->input->post('remove_file'))
				unlink('./asset/foto_agenda/'.$this->input->post('remove_file'));
			$data['gambar'] = '';
		}

		if(!empty($_FILES['photo']['name']))
		{
			$upload = $this->_do_upload();

			//delete file
			$agenda = $this->Agenda_model->get_by_id($this->input->post('id'));
			
			if(file_exists('./asset/foto_agenda/'.$agenda->gambar) && $agenda->gambar)
				unlink('./asset/foto_agenda/'.$agenda->gambar);

			$data['gambar'] = $upload;
		}

		$this->Agenda_model->update(array('id_agenda' => $this->input->post('id')), $data);
		//echo json_encode(array("status" => TRUE));
		echo json_encode(array("status" => 'info', 'msg'=>"Data Berhasil Diperbarui"));
	}

	public function delete(){
		if($_POST['empid']) {
			$agenda = $this->Agenda_model->get_by_id($_POST['empid']);
				unlink('./asset/foto_agenda/'.$agenda->gambar);
			$resultset = $this->Agenda_model->delete($_POST['empid']);
			if($resultset) {
				echo "Record Deleted";
			}
		}
	}
	/* End Function Action Lowongan Kerja*/

    /**
	 * @param string     $view
	 * @param array|null $data
	 * @param bool       $returnhtml
	 *
	 * @return mixed
	 */
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{
		//$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);
		//$view_html = $this->template->load('template', $view, $this->viewdata, $returnhtml);
		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}
}
