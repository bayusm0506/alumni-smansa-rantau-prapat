<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Administrator extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
	}

	public function index(){
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('administrator/auth/login', 'refresh');
		}else{
			redirect('administrator/dashboard', 'refresh');
		}
	}
}
