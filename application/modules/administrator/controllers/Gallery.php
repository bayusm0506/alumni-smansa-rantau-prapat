<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Gallery extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//$this->Ion_auth_model->Authenticate();
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('administrator/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			//Success
		}
	}

	private function actionDashboard(){
		$cek = $this->Dashboard_model->getUserDescription($this->session->user_id);
		$row = $cek->row_array();
		$this->data['first_name'] = $this->session->first_name;
		$this->data['description'] = $row['description'];
		$this->data['info_messages'] = $this->Dashboard_model->info_getMessages();
		$this->data['messages'] = $this->Dashboard_model->new_message(10);
		$this->data['record'] = $this->MenuUtama_model->identitas()->row_array();
		$this->data['fav'] = $this->MenuUtama_model->favicon()->row_array();
	}

    /* Function Action Gallery */
    public function index(){
		$this->data['title'] = 'Gallery';
		self::actionDashboard();
		$this->data['album'] = $this->Album_model->menu_cek($id = "");

		$this->_render_page('layouts/main_header', $this->data);
		$this->_render_page('gallery' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->_render_page('layouts/main_footer', $this->data);
	}

	public function get_autocomplete_gallery(){
        if (isset($_GET['term'])) {
            $result = $this->Gallery_model->autocomplete($_GET['term']);
            if (count($result) > 0) {
            foreach ($result as $row)
                $arr_result[] = $row->jdl_gallery;
                echo json_encode($arr_result);
            }
        }
    }

	public function gallery_ajax_list()
	{
		// $cek = $this->Dashboard_model->getUserDescription($this->session->id_groups);
		// $row = $cek->row_array();
		$list = $this->Gallery_model->get_datatables($this->session->username);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $gallery) {
			$cmenu = $this->Album_model->menu_cek($gallery->id_album)->row_array();
			if ($cmenu['id_album']=='') {$menu = 'No Album';}else{$menu = $cmenu['jdl_album'];}
			$no++;
			$row = array();
			$row[] = $no;
			//$row[] = $album->gbr_album;
			if($gallery->gbr_gallery)
				$row[] = '<a href="'.base_url('./asset/img_galeri/').$gallery->gbr_gallery.'" target="_blank"><img width="40" height="40" class="img-circle"  src="'.base_url('./asset/img_galeri/').$gallery->gbr_gallery.'"></a>';
			else
				$row[] = '(No photo)';

			$row[] = $gallery->jdl_gallery;
			$row[] = $menu;
			$row[] = tgl_indo($gallery->tgl_gallery);
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_gallery('."'".$gallery->id_gallery."'".')"><i class="glyphicon glyphicon-edit"></i> Edit</a>
				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_gallery('."'".$gallery->id_gallery."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->Gallery_model->count_all(),
						"recordsFiltered" => $this->Gallery_model->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function export_pdfMW(){
		$data = array(
			'title' => 'Data Gallery',
			//'list' => $this->Gallery_model->getPerdana()
		);
	    $filename = "Bayu Setra Maulana";
	    $this->load->library('pdf');

	    $this->pdf->setPaper('A4', 'potrait');
	    $this->pdf->filename = $filename.".pdf";
		$this->pdf->load_view('menu_utama' . DIRECTORY_SEPARATOR . '_exportPdfMW', $data);

	}

	public function save()
	{
		$this->_validate();

		$data = array(
            'id_album'=>$this->input->post('id_album'),
            'username'=>$this->session->username,
            'jdl_gallery'=>$this->input->post('jdl_gallery'),
            'gallery_seo'=>seo_title($this->input->post('jdl_gallery')),
            'keterangan'=>$this->input->post('keterangan'),
            'tgl_gallery'=>$this->input->post('tgl_gallery')
		);

		if(!empty($_FILES['photo']['name']))
		{
			$upload = $this->_do_upload();
			$data['gbr_gallery'] = $upload;
		}

		if(!empty($_FILES['photo2']['name']))
		{
			$upload = $this->_do_upload2();
			$data['gbr_gallery2'] = $upload;
		}

		if(!empty($_FILES['photo3']['name']))
		{
			$upload = $this->_do_upload3();
			$data['gbr_gallery3'] = $upload;
		}

		$insert = $this->Gallery_model->save($data);

		echo json_encode(array("status" => 'info', 'msg'=>"Data Berhasil Disimpan"));
	}

	public function ajax_edit($id)
	{
		$data = $this->Gallery_model->get_by_id($id);
		$data->tgl_gallery = ($data->tgl_gallery == '0000-00-00') ? '' : $data->tgl_gallery;
		echo json_encode($data);
	}

	private function _do_upload()
	{
		$config['upload_path']          = './asset/img_galeri/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 3000; //set max size allowed in Kilobyte
        //$config['max_width']            = 1000; // set max width image allowed
        //$config['max_height']           = 1000; // set max height allowed
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name

        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('photo')) //upload and validate
        {
            $data['inputerror'][] = 'photo';
			$data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
			$data['status'] = FALSE;
			echo json_encode($data);
			exit();
		}
		return $this->upload->data('file_name');
	}

	private function _do_upload2()
	{
		$config['upload_path']          = './asset/img_galeri/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 3000; //set max size allowed in Kilobyte
        //$config['max_width']            = 1000; // set max width image allowed
        //$config['max_height']           = 1000; // set max height allowed
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name

        $this->load->library('upload', $config);

		if(!$this->upload->do_upload('photo2')) //upload and validate
        {
            $data['inputerror'][] = 'photo2';
			$data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
			$data['status'] = FALSE;
			echo json_encode($data);
			exit();
		}
		return $this->upload->data('file_name');
	}

	private function _do_upload3()
	{
		$config['upload_path']          = './asset/img_galeri/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 3000; //set max size allowed in Kilobyte
        //$config['max_width']            = 1000; // set max width image allowed
        //$config['max_height']           = 1000; // set max height allowed
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name

        $this->load->library('upload', $config);

		if(!$this->upload->do_upload('photo3')) //upload and validate
        {
            $data['inputerror'][] = 'photo3';
			$data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
			$data['status'] = FALSE;
			echo json_encode($data);
			exit();
		}
		return $this->upload->data('file_name');
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('jdl_gallery') == '')
		{
			$data['inputerror'][] = 'jdl_gallery';
			$data['error_string'][] = 'Judul Gallery Harus Diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('id_album') == '')
		{
			$data['inputerror'][] = 'id_album';
			$data['error_string'][] = 'Album Harus Diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('tgl_gallery') == '')
		{
			$data['inputerror'][] = 'tgl_gallery';
			$data['error_string'][] = 'Tanggal Kegiatan Tidak Boleh Kosong';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	public function edit(){
        $this->_validate();

		$data = array(
			'id_album'=>$this->input->post('id_album'),
            'username'=>$this->session->username,
            'jdl_gallery'=>$this->input->post('jdl_gallery'),
            'gallery_seo'=>seo_title($this->input->post('jdl_gallery')),
            'keterangan'=>$this->input->post('keterangan'),
            'tgl_gallery'=>$this->input->post('tgl_gallery')
		);

		if($this->input->post('remove_photo')) // if remove photo checked
		{
			if(file_exists('./asset/img_galeri/'.$this->input->post('remove_photo')) && $this->input->post('remove_photo'))
				unlink('./asset/img_galeri/'.$this->input->post('remove_photo'));
			$data['gbr_gallery'] = '';
		}

		if($this->input->post('remove_photo2')) // if remove photo checked
		{
			if(file_exists('./asset/img_galeri/'.$this->input->post('remove_photo2')) && $this->input->post('remove_photo2'))
				unlink('./asset/img_galeri/'.$this->input->post('remove_photo2'));
			$data['gbr_gallery2'] = '';
		}

		if($this->input->post('remove_photo3')) // if remove photo checked
		{
			if(file_exists('./asset/img_galeri/'.$this->input->post('remove_photo3')) && $this->input->post('remove_photo3'))
				unlink('./asset/img_galeri/'.$this->input->post('remove_photo3'));
			$data['gbr_gallery2'] = '';
		}

		if(!empty($_FILES['photo']['name']))
		{
			$upload = $this->_do_upload();

			//delete file
			$gallery = $this->Gallery_model->get_by_id($this->input->post('id'));
			
			if(file_exists('./asset/img_galeri/'.$gallery->gbr_gallery) && $gallery->gbr_gallery)
				unlink('./asset/img_galeri/'.$gallery->gbr_gallery);

			$data['gbr_gallery'] = $upload;
		}

		if(!empty($_FILES['photo2']['name']))
		{
			$upload = $this->_do_upload2();

			//delete file
			$gallery = $this->Gallery_model->get_by_id($this->input->post('id'));
			
			if(file_exists('./asset/img_galeri/'.$gallery->gbr_gallery2) && $gallery->gbr_gallery2)
				unlink('./asset/img_galeri/'.$gallery->gbr_gallery2);

			$data['gbr_gallery2'] = $upload;
		}

		if(!empty($_FILES['photo3']['name']))
		{
			$upload = $this->_do_upload3();

			//delete file
			$gallery = $this->Gallery_model->get_by_id($this->input->post('id'));
			
			if(file_exists('./asset/img_galeri/'.$gallery->gbr_gallery3) && $gallery->gbr_gallery3)
				unlink('./asset/img_galeri/'.$gallery->gbr_gallery3);

			$data['gbr_gallery3'] = $upload;
		}


		$this->Gallery_model->update(array('id_gallery' => $this->input->post('id')), $data);
		echo json_encode(array("status" => 'info', 'msg'=>"Data Berhasil Diperbarui"));
	}

	public function delete(){
		if($_POST['empid']) {
			$album = $this->Gallery_model->get_by_id($_POST['empid']);
				unlink('./asset/img_galeri/'.$album->gbr_gallery);
			$resultset = $this->Gallery_model->delete($_POST['empid']);
			if($resultset) {
				echo "Record Deleted";
			}
		}
	}
	/* End Function Action Gallery */

    /**
	 * @param string     $view
	 * @param array|null $data
	 * @param bool       $returnhtml
	 *
	 * @return mixed
	 */
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{
		//$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);
		//$view_html = $this->template->load('template', $view, $this->viewdata, $returnhtml);
		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}
}
