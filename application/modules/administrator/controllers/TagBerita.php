<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class TagBerita extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//$this->Ion_auth_model->Authenticate();
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('administrator/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			//Success
		}
	}

	private function actionDashboard(){
		$cek = $this->Dashboard_model->getUserDescription($this->session->id_groups);
		$row = $cek->row_array();
		$this->data['first_name'] = $this->session->first_name;
		$this->data['description'] = $row['description'];
		$this->data['info_messages'] = $this->Dashboard_model->info_getMessages();
		$this->data['messages'] = $this->Dashboard_model->new_message(10);
		$this->data['record'] = $this->MenuUtama_model->identitas()->row_array();
		$this->data['fav'] = $this->MenuUtama_model->favicon()->row_array();
	}

    /* Function Action Tag Berita */
    public function index(){
		$this->data['title'] = 'Tag Berita';
		self::actionDashboard();

		$this->_render_page('layouts/main_header', $this->data);
		$this->_render_page('tagBerita' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->_render_page('layouts/main_footer', $this->data);
	}

	public function get_autocomplete_tagBerita(){
        if (isset($_GET['term'])) {
            $result = $this->TagBerita_model->autocomplete($_GET['term']);
            if (count($result) > 0) {
            foreach ($result as $row)
                $arr_result[] = $row->nama_tag;
                echo json_encode($arr_result);
            }
        }
    }

	public function tagBerita_ajax_list()
	{
		$list = $this->TagBerita_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $tag) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $tag->nama_tag;
			$row[] = '<a href="'.base_url()."berita/tag/".$tag->tag_seo.'" target="_blank">berita/tag/'.$tag->tag_seo.'</a>';

			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_tag('."'".$tag->id_tag."'".')"><i class="glyphicon glyphicon-edit"></i> Edit</a>
				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_tag('."'".$tag->id_tag."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->TagBerita_model->count_all(),
						"recordsFiltered" => $this->TagBerita_model->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function export_pdfMW(){
		$data = array(
			'title' => 'Data Tag Berita',
			//'list' => $this->TagBerita_model->getPerdana()
		);
	    $filename = "Bayu Setra Maulana";
	    $this->load->library('pdf');

	    $this->pdf->setPaper('A4', 'potrait');
	    $this->pdf->filename = $filename.".pdf";
		$this->pdf->load_view('menu_utama' . DIRECTORY_SEPARATOR . '_exportPdfMW', $data);

	}

	public function save()
	{
		$this->_validate();

		$data = array(
            'nama_tag'=>$this->db->escape_str($this->input->post('nama_tag')),
            'username'=>$this->session->username,
            'tag_seo'=>seo_title($this->input->post('nama_tag')),
            'count'=>'0'
		);

		$insert = $this->TagBerita_model->save($data);

		echo json_encode(array("status" => 'info', 'msg'=>"Data Berhasil Disimpan"));
	}

	public function ajax_edit($id)
	{
		$data = $this->TagBerita_model->get_by_id($id);
		echo json_encode($data);
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('nama_tag') == '')
		{
			$data['inputerror'][] = 'nama_tag';
			$data['error_string'][] = 'Nama Tag Harus Diisi';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	public function edit(){
        $this->_validate();

		$data = array(
			'nama_tag'=>$this->db->escape_str($this->input->post('nama_tag')),
            'username'=>$this->session->username,
            'tag_seo'=>seo_title($this->input->post('nama_tag'))
		);

		$this->TagBerita_model->update(array('id_tag' => $this->input->post('id')), $data);
		echo json_encode(array("status" => 'info', 'msg'=>"Data Berhasil Diperbarui"));
	}

	public function delete(){
		if($_POST['empid']) {
			$resultset = $this->TagBerita_model->delete($_POST['empid']);
			if($resultset) {
				echo "Record Deleted";
			}
		}
	}
	/* End Function Action Tag Berita */

    /**
	 * @param string     $view
	 * @param array|null $data
	 * @param bool       $returnhtml
	 *
	 * @return mixed
	 */
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{
		//$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);
		//$view_html = $this->template->load('template', $view, $this->viewdata, $returnhtml);
		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}
}
