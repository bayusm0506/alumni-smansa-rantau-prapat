<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Dashboard extends CI_Controller {
	public function __construct()
	{
		parent::__construct();

		//$this->Ion_auth_model->Authenticate();
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('administrator/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			//Success
		}
	}

	private function rules(){
		return $this->Dashboard_model->rules();
	}

	public function index(){
		$cek = $this->Dashboard_model->getUserDescription($this->session->id_groups);
		$row = $cek->row_array();

		$this->data['title'] = 'Dashboard';
		$this->data['first_name'] = $this->session->first_name;
		$this->data['description'] = $row['description'];
		$this->data['info_messages'] = $this->Dashboard_model->info_getMessages();
		$this->data['messages'] = $this->Dashboard_model->new_message(10);
		$this->data['news'] = $this->Dashboard_model->info_news();
		$this->data['pages'] = $this->Dashboard_model->info_pages();
		$this->data['agenda'] = $this->Dashboard_model->info_agenda();
		$this->data['users_info'] = $this->Dashboard_model->info_users();
		$this->data['grafik'] =	$this->Dashboard_model->grafik_kunjungan();
		$this->data['kategori'] =	getKategoriBerita();
		$this->data['record'] = $this->MenuUtama_model->identitas()->row_array();
		$this->data['fav'] = $this->MenuUtama_model->favicon()->row_array();


		$this->_render_page('layouts/main_header', $this->data);
		$this->_render_page('dashboard' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->_render_page('layouts/main_footer', $this->data);
	}

	public function RealData(){
		$return = [];
		$news = $this->Dashboard_model->info_news();
		$pages = $this->Dashboard_model->info_pages();
		$agenda = $this->Dashboard_model->info_agenda();
		$users_info = $this->Dashboard_model->info_users();
		$grafik = $this->Dashboard_model->grafik_kunjungan();
		$hits_stat = $this->Dashboard_model->hits();

		$return['news'] = $news;
		$return['pages'] = $pages;
		$return['agenda'] = $agenda;
		$return['users_info'] = $users_info;

		$tgl_graf = [];
		$jml_kunjung = [];
		foreach ($grafik as $row) {
			$tgl_graf[] = tgl_indo($row->tanggal);
			$jml_kunjung[] = $row->jumlah;
		}

		$hits = [];
		foreach ($hits_stat as $row) {
			$hits[] = $row->jmlh_hits;
		}

		$realChart = [
			"title" => "Real Time Data",
			"text" => "Jumlah Kunjungan",
			"subtitle" => "",
			"categories"=>array_values($tgl_graf),
			"series"=> array(
						array(
								'name'	=>'Kunjungan',
								'type' 	=>'column',
								'yAxis'=> 0,
								'data'	=>array_values($jml_kunjung),
								'dataLabels'=> array(
									'enabled'=> true
								)
						)
						,
						array(
								'name'=>'Hits',
								'type' 	=>'line',
								'yAxis'=> 1,
								'data'=>array_values($hits),
								'dataLabels'=> array(
									'enabled'=> true
								)
						)

			)
		];

		$return['realChart'] = $realChart;

		echo json_encode($return,JSON_NUMERIC_CHECK);
		
	}

	public function validateForm()
   	{
       	$rules = self::rules();
       	$this->form_validation->set_rules($rules);
       	$this->form_validation->set_message('required', '{field} Tidak Boleh Kosong');

        if ($this->form_validation->run() == FALSE){
            $errors = validation_errors();
           	//echo json_encode(['error'=>$errors]);
            $response = ["status"=>'error','msg'=>$errors];
            echo json_encode($response);
        }else{
        	$this->Dashboard_model->quickInsertNews();
            $response = ["status"=>'info','msg'=>'Data Berhasil di Simpan.'];
            echo json_encode($response);
           	//echo json_encode(['success'=>'Form submitted successfully.']);
        }
    }

    /**
	 * @param string     $view
	 * @param array|null $data
	 * @param bool       $returnhtml
	 *
	 * @return mixed
	 */
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{
		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);
		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}
}
