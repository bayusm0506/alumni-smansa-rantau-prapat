<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class ListBerita extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//$this->Ion_auth_model->Authenticate();
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('administrator/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			//Success
		}
	}

	private function actionDashboard(){
		$cek = $this->Dashboard_model->getUserDescription($this->session->id_groups);
		$row = $cek->row_array();
		$this->data['first_name'] = $this->session->first_name;
		$this->data['description'] = $row['description'];
		$this->data['info_messages'] = $this->Dashboard_model->info_getMessages();
		$this->data['messages'] = $this->Dashboard_model->new_message(10);
		//$this->data['record'] = $this->MenuUtama_model->identitas()->row_array();
		$this->data['fav'] = $this->MenuUtama_model->favicon()->row_array();
	}

	/* Funtion Action List Berita */
	public function index(){
		$this->data['title'] = 'List Berita';
		self::actionDashboard();
		//$this->data['record'] = $this->ListBerita_model->listBerita()->result_array();
		$this->data['kategori_berita'] = $this->KategoriBerita_model->kategoriBerita()->result_array();
		$this->data['tag'] = $this->TagBerita_model->tagBerita()->result_array();

		$this->_render_page('layouts/main_header', $this->data);
		$this->_render_page('listBerita' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->_render_page('layouts/main_footer', $this->data);
	}

	public function update(){
		$this->data['title'] = 'List Berita';
		self::actionDashboard();
		$id = $this->uri->segment(4);
		$this->data['kategori_berita'] = $this->KategoriBerita_model->kategoriBerita()->result_array();
		$this->data['tag'] = $this->TagBerita_model->tagBerita()->result_array();
		$this->data['rows'] = $this->ListBerita_model->get_by_id_edit($id)->row_array();

		$this->_render_page('layouts/main_header', $this->data);
		$this->_render_page('listBerita' . DIRECTORY_SEPARATOR . '_form_update', $this->data);
		$this->_render_page('layouts/main_footer', $this->data);
	}

	public function get_autocomplete_listBerita(){
        if (isset($_GET['term'])) {
            $result = $this->ListBerita_model->autocomplete($_GET['term']);
            if (count($result) > 0) {
            foreach ($result as $row)
                $arr_result[] = $row->judul;
                echo json_encode($arr_result);
            }
        }
    }

	public function listBerita_ajax_list()
	{
		$list = $this->ListBerita_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $listberita) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $listberita->judul;
			$row[] = tgl_indo($listberita->tanggal);
			if ($listberita->status == 'Y') {
				$status = "<span style='color:green'>Published</span>";
			}else{
				$status = "<span style='color:red'>Unpublished</span>";
			}
			$row[] = $status;

			if($listberita->gambar)
				$row[] = '<a href="'.base_url('./asset/foto_berita/').$listberita->gambar.'" target="_blank"><img width="40" height="40" class="img-circle" src="'.base_url('./asset/foto_berita/').$listberita->gambar.'"></a>';
			else
				$row[] = '(No photo)';

			$row[] = '<a class="btn btn-sm btn-primary" href="'.base_url().'administrator/listBerita/update/'.$listberita->id_berita.'" title="Edit"><i class="glyphicon glyphicon-edit"></i> Edit</a>
				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_lb('."'".$listberita->id_berita."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
			
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->ListBerita_model->count_all(),
						"recordsFiltered" => $this->ListBerita_model->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function saveListBerita(){
        $this->_validate();
        $cek = $this->Dashboard_model->getUserDescription($this->session->id_groups);
		$row = $cek->row_array();
		if ($row['name'] == 'kontributor') {
			$status = 'N';
		}else{
			$status = 'Y';
		}

	    if ($this->input->post('tag') != ''){
	        $tag_seo = $this->input->post('tag');
	        $tag=implode(',',$tag_seo);
	    }else{
	        $tag = '';
	    }

		$data = array(
          	'id_kategori'=>$this->db->escape_str($this->input->post('id_kategori_berita')),
	        'username'=>$this->session->username,
	        'judul'=>$this->db->escape_str($this->input->post('judul')),
	        'sub_judul'=>$this->db->escape_str($this->input->post('sub_judul')),
	        'youtube'=>$this->db->escape_str($this->input->post('video')),
	        'judul_seo'=>seo_title($this->input->post('judul')),
	        'headline'=>$this->db->escape_str($this->input->post('headline')),
	        'aktif'=>$this->db->escape_str($this->input->post('pilihan')),
	        'utama'=>$this->db->escape_str($this->input->post('berita_utama')),
	        'isi_berita'=>$this->input->post('isi_berita'),
	        'keterangan_gambar'=>$this->db->escape_str($this->input->post('keterangan_gambar')),
	       	'hari'=>hari_ini(date('w')),
	        'tanggal'=>date('Y-m-d'),
	        'jam'=>date('H:i:s'),
	        'dibaca'=>'0',
	        'tag'=>$tag,
	        'status'=>$status
		);

		if(!empty($_FILES['photo']['name']))
		{
			$upload = $this->_do_upload();
			$data['gambar'] = $upload;
		}

		$insert = $this->ListBerita_model->save($data);

		echo json_encode(array("status" => 'info', 'msg'=>"Data Berhasil Disimpan"));
	}

	private function _do_upload()
	{
		$config['upload_path']          = './asset/foto_berita/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 3000; //set max size allowed in Kilobyte
        //$config['max_width']            = 1000; // set max width image allowed
        //$config['max_height']           = 1000; // set max height allowed
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name

        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('photo')) //upload and validate
        {
            $data['inputerror'][] = 'photo';
			$data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
			$data['status'] = FALSE;
			echo json_encode($data);
			exit();
		}
		return $this->upload->data('file_name');
	}

	private function _do_upload_update()
	{
		$config['upload_path']          = './asset/foto_berita/';
        $config['allowed_types']        = 'gif|jpg|png|JPG|JPEG';
        $config['max_size']             = 3000; //set max size allowed in Kilobyte
        //$config['max_width']            = 1000; // set max width image allowed
        //$config['max_height']           = 1000; // set max height allowed
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name

        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('photo')) //upload and validate
        {
			$errors = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
			$response = ["status"=>'info','msg'=>$errors];
	        echo json_encode($response);
		}
		return $this->upload->data('file_name');
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('judul') == '')
		{
			$data['inputerror'][] = 'judul';
			$data['error_string'][] = 'Judul Harus Diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('sub_judul') == '')
		{
			$data['inputerror'][] = 'sub_judul';
			$data['error_string'][] = 'Sub Judul Harus Diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('id_kategori_berita') == '')
		{
			$data['inputerror'][] = 'id_kategori_berita';
			$data['error_string'][] = 'Kategori Harus Diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('keterangan_gambar') == '')
		{
			$data['inputerror'][] = 'keterangan_gambar';
			$data['error_string'][] = 'Keterangan Gambar Harus Diisi';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	public function editListBerita(){
       	$this->form_validation->set_rules($this->ListBerita_model->rules());
       	$this->form_validation->set_message('required', '{field} Tidak Boleh Kosong');

        if ($this->form_validation->run() == FALSE){
            $errors = validation_errors();
            $response = ["status"=>'error','msg'=>$errors];
            echo json_encode($response);
        }else{
        	$cek = $this->Dashboard_model->getUserDescription($this->session->id_groups);
			$row = $cek->row_array();
			if ($row['name'] == 'kontributor') {
				$status = 'N';
			}else{
				$status = 'Y';
			}

		    if ($this->input->post('tag') != ''){
		        $tag_seo = $this->input->post('tag');
		        $tag=implode(',',$tag_seo);
		    }else{
		        $tag = '';
		    }

			$data = array(
	          	'id_kategori'=>$this->db->escape_str($this->input->post('id_kategori_berita')),
		        'username'=>$this->session->username,
		        'judul'=>$this->db->escape_str($this->input->post('judul')),
		        'sub_judul'=>$this->db->escape_str($this->input->post('sub_judul')),
		        'youtube'=>$this->db->escape_str($this->input->post('video')),
		        'judul_seo'=>seo_title($this->input->post('judul')),
		        'headline'=>$this->db->escape_str($this->input->post('headline')),
		        'aktif'=>$this->db->escape_str($this->input->post('pilihan')),
		        'utama'=>$this->db->escape_str($this->input->post('berita_utama')),
		        'isi_berita'=>$this->input->post('isi_berita'),
		        'keterangan_gambar'=>$this->db->escape_str($this->input->post('keterangan_gambar')),
		       	'hari'=>hari_ini(date('w')),
		        'tanggal'=>date('Y-m-d'),
		        'jam'=>date('H:i:s'),
		        //'dibaca'=>'0',
		        'tag'=>$tag,
		        'status'=>$status
			);

			if($this->input->post('remove_photo')) // if remove photo checked
			{
				if(file_exists('./asset/foto_berita/'.$this->input->post('remove_photo')) && $this->input->post('remove_photo'))
					unlink('./asset/foto_berita/'.$this->input->post('remove_photo'));
				$data['gambar'] = '';
			}

			if(!empty($_FILES['photo']['name']))
			{
				$upload = $this->_do_upload_update();

				//delete file
				$list = $this->ListBerita_model->get_by_id($this->input->post('id'));
				
				if(file_exists('./asset/foto_berita/'.$list->gambar) && $list->gambar)
					unlink('./asset/foto_berita/'.$list->gambar);

				$data['gambar'] = $upload;
			}


			$this->ListBerita_model->update(array('id_berita' => $this->input->post('id')), $data);

			$response = ["status"=>'info','msg'=>'Data Berhasil di Perbarui.','url'=>site_url('administrator/listBerita/update/'.$this->input->post('id'))];
	        echo json_encode($response);
        }
	}

	public function deleteListBerita(){
		if($_POST['empid']) {
			$berita = $this->ListBerita_model->get_by_id($_POST['empid']);
			if(file_exists('./asset/foto_berita/'.$berita->gambar) && $berita->gambar)
				unlink('./asset/foto_berita/'.$berita->gambar);
			$resultset = $this->ListBerita_model->delete($_POST['empid']);
			if($resultset) {
				echo "Record Deleted";
			}
		}
	}
	/* End Funtion Action List Berita */

	/**
	 * @param string     $view
	 * @param array|null $data
	 * @param bool       $returnhtml
	 *
	 * @return mixed
	 */
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{
		//$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);
		//$view_html = $this->template->load('template', $view, $this->viewdata, $returnhtml);
		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}
}