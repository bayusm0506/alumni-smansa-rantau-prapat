<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class GrafikAlumni extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//$this->Ion_auth_model->Authenticate();
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('administrator/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			//Success
		}
	}

	private function actionDashboard(){
		$cek = $this->Dashboard_model->getUserDescription($this->session->id_groups);
		$row = $cek->row_array();
		$this->data['first_name'] = $this->session->first_name;
		$this->data['description'] = $row['description'];
		$this->data['info_messages'] = $this->Dashboard_model->info_getMessages();
		$this->data['messages'] = $this->Dashboard_model->new_message(10);
		$this->data['record'] = $this->MenuUtama_model->identitas()->row_array();
		$this->data['fav'] = $this->MenuUtama_model->favicon()->row_array();
	}

    /* Function Action Grafik Alumni */
    public function index(){
		$this->data['title'] = 'Grafik Alumni';
		self::actionDashboard();
		
		$this->_render_page('layouts/main_header', $this->data);
		$this->_render_page('alumni' . DIRECTORY_SEPARATOR . '_chart', $this->data);
		$this->_render_page('layouts/main_footer', $this->data);
	}

	public function RealData(){
		$return = [];
		$data_jur = $this->Alumni_model->chartJurusan();

		$jurusan = [];
		$jml_siswa = [];
		foreach ($data_jur as $row) {
			$jurusan[] = $row->nama_jurusan;
			$jml_siswa[] = $row->jumlah;
		}

		$realChart = [
			"title" => "Real Time Data Alumni",
			"text" => "Siswa SMANSA",
			"subtitle" => "",
			"categories"=>array_values($jurusan),
			"series"=> array(
						array(
								'name'=>'Alumni',
								'type' 	=>'line',
								'yAxis'=> 1,
								'data'=>array_values($jml_siswa),
								'dataLabels'=> array(
									'enabled'=> true
								)
						)

			)
		];

		$data_gel = $this->Alumni_model->chartGelar();
		$gelar = [];
		$nama_gelar = [];
		foreach ($data_gel as $row) {
			$nama_gelar[] = $row->nama_gelar;
			$gelar[] = $row->jumlah;
		}

		$realChartGelar = [
			"title" => "Gelar Alumni",
			"text" => "Siswa SMANSA",
			"subtitle" => "",
			"categories"=>array_values($nama_gelar),
			"series"=> array(
						array(
								'name'=>'Alumni',
								'type' 	=>'column',
								'yAxis'=> 1,
								'data'=>array_values($gelar),
								'dataLabels'=> array(
									'enabled'=> true
								)
						)

			)
		];

		$return['realChart'] = $realChart;
		$return['realChartGelar'] = $realChartGelar;

		echo json_encode($return,JSON_NUMERIC_CHECK);
		
	}
	/* End Function Action Grafik Alumni*/

    /**
	 * @param string     $view
	 * @param array|null $data
	 * @param bool       $returnhtml
	 *
	 * @return mixed
	 */
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{
		//$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);
		//$view_html = $this->template->load('template', $view, $this->viewdata, $returnhtml);
		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}
}
