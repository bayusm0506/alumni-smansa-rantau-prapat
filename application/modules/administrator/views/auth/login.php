<div class="login-box">
  <div class="login-logo">
    <a href="#"><b>Admin</b><?=lang('login_heading')?></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg"><!-- Sign in to start your session -->
      <?php echo lang('login_subheading');?>
    </p>
    <?php
      if (isset($message)) {
    ?>
      <div class="alert alert-danger alert-dismissible fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="fa fa-window-close" aria-hidden="true"></i></a>
        <strong><?=lang('login_error')?></strong> <?=$message?>
      </div>
    <?php
      }
    ?>
    
    <?php echo form_open("administrator/auth/login");?>
      <div class="form-group has-feedback">
        <?php echo form_input($identity);?>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <?php echo form_input($password);?>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
              <?php echo lang('login_remember_label');?>
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <?php echo form_submit('submit', lang('login_submit_btn'), array('class'=>'btn btn-primary btn-block btn-flat'));?>
        </div>
        <!-- /.col -->
      </div>
    <?php echo form_close();?>

    <div class="social-auth-links text-center">
      <!-- <p>- OR -</p>
      <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
        Facebook</a>
      <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
        Google+</a> -->
    </div>
    <!-- /.social-auth-links -->

    <a href="forgot_password"><?php // echo lang('login_forgot_password');?></a><br>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->