<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Identitas
    <small>Website</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url()?>administrator/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?=base_url()?>administrator/identitasWebsite">Menu Utama</a></li>
    <li class="active">Identitas Website</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
<!-- SELECT2 EXAMPLE -->
<div class="box box-default">
<div class="box-header with-border">
  <h3 class="box-title">Form Identitas</h3>

  <div class="box-tools pull-right">
    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
  </div>
</div>
<!-- /.box-header -->
<div class="box-body">
  <div class="row">
    <div class="col-md-6">
    <?php echo form_open_multipart('', array('id'=>'form-menu-utama', 'role'=>'form', 'name'=>'form-menu-utama'));?>
      <div class="form-group">
        <label>Nama Website</label>
        <input type="text" name="nama_website" class="form-control" value="<?=$record['nama_website']?>" placeholder="Nama Website">
      </div>
      <!-- /.form-group -->
      <div class="form-group">
        <label>Email</label>
        <input type="email" name="email" class="form-control" value="<?=$record['email']?>" placeholder="Email">
      </div>
      <div class="form-group">
        <label>Facebook</label>
        <input type="text" name="social" class="form-control" value="<?=$record['facebook']?>" placeholder="Social Network">
      </div>
      <div class="form-group">
        <label>Twitter</label>
        <input type="text" name="twitter" class="form-control" value="<?=$record['twitter']?>" placeholder="Twitter">
      </div>
      <div class="form-group">
        <label>Opening Times</label>
        <input type="text" name="open" class="form-control" value="<?=$record['open']?>" placeholder="Opening Times">
      </div>
      <!-- /.form-group -->
    </div>
    <!-- /.col -->
    <div class="col-md-6">
      <div class="form-group">
        <label>Domain</label>
        <input type="text" name="domain" class="form-control" value="<?=$record['url']?>" placeholder="Domain">
      </div>
      <!-- /.form-group -->
      <div class="form-group">
        <label>No. HP / Whatsapp</label>
        <input type="text" name="whatsapp" class="form-control" value="<?=$record['whatsapp']?>" placeholder="No. Hp / Whatsapp ">
      </div>
      <div class="form-group">
        <label>No. Telpon</label>
        <input type="text" name="telpon" class="form-control" data-inputmask='"mask": "(999) 999-9999"' data-mask value="<?=$record['no_telp']?>" placeholder="No. Telpon">
      </div>
      <div class="form-group">
        <label>Meta Keyword</label>
        <input type="text" name="meta_keyword" class="form-control" value="<?=$record['meta_keyword']?>" placeholder="Meta Keyword">
      </div>
      <div class="form-group">
        <label>Youtube</label>
        <input type="text" name="youtube" class="form-control" value="<?=$record['youtube']?>" placeholder="Youtube">
      </div>
      <!-- /.form-group -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
  <div class="row">
    <div class="col-md-12">
      <div class="form-group">
        <label>Alamat</label>
        <input type="text" name="address" class="form-control" value="<?=$record['address']?>" placeholder="Alamat">
      </div>
      <div class="form-group">
        <label>Meta Deskripsi</label>
        <input type="text" name="meta_deskripsi" class="form-control" value="<?=$record['meta_deskripsi']?>" placeholder="Meta Deskripsi">
      </div>
      <!-- /.form-group -->
      <div class="form-group">
        <label>Google Maps</label>
        <textarea name="maps" class="form-control" value="<?=$record['maps']?>" placeholder="Google Maps" style="height:80px"><?=$record['maps']?></textarea>
      </div>
      <div class="form-group">
        <label>Info Footer</label>
        <textarea name="info_footer" class="form-control" value="<?=$record['keterangan']?>" placeholder="Info Footer" style="height:80px"><?=$record['keterangan']?></textarea>
      </div>
      <!-- /.form-group -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
  <div class="row">
    <div class="col-md-4">
      <div class="form-group">
        <label>File Upload</label>
        <input type="file" name="favicon" id="imgInp">
      </div>
      <div id="element-progress" style="width:100px;"></div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <img id="bsm" src="#" alt="Preview in Here"/>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>Favicon</label><br />
        <?php
          if (isset($record['favicon'])) {
            echo "<img src='".base_url()."asset/images/$record[favicon]'>";
          }else{

          }
        ?>
      </div>
      <div class="form-group">
        <button type="submit" class="btn btn-info pull-right">Update</button>
        </div>
      <?php echo form_close();?>
    </div>
  </div>
</div>
<!-- /.box-body -->
</div>
<!-- /.box -->
</section>
<!-- /.content -->
<script type="text/javascript">
  function readURL(input) {

    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $('#bsm').attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]);
    }
  }

  $("#imgInp").change(function() {
    readURL(this);
  });
</script>
<script>
 	$('form#form-menu-utama').submit(function(){
		Component.upload( "<?=site_url('administrator/identitasWebsite/save')?>",'form-menu-utama',function(response){
			if( response.status == 'info'){
				Component.show_alert('info',response.msg);
        setInterval(function(){ window.location.href = response.url; }, 3000);
			}else{
				Component.show_alert('error',response.msg);
			}
			Component.progressFull();
		});
		return false;
	});
</script>