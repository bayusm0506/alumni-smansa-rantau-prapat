<div class="box-body" style="overflow: auto">
  <table id="table" class="table table-bordered table-striped">
    <thead>
    <tr>
      <th>No</th>
      <th>Username</th>
      <th>Email</th>
      <th>First Name</th>
      <th>Last Name</th>
      <th>Company</th>
      <th>Phone</th>
      <th>Status</th>
      <th>Action</th>
    </tr>
    </thead>
    <tbody>
      
    </tbody>
    <tfoot>
    <tr>
      <th>No</th>
      <th>Username</th>
      <th>Email</th>
      <th>First Name</th>
      <th>Last Name</th>
      <th>Company</th>
      <th>Phone</th>
      <th>Status</th>
      <th>Action</th>
    </tr>
    </tfoot>
  </table>
</div>
<!-- /.box-body -->