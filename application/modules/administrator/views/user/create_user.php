<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Users
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url()?>administrator/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?=base_url()?>administrator/create_user">Modul Users</a></li>
    <li class="active">Create User</li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-md-8">
      <!-- Horizontal Form -->
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo lang('create_user_heading');?></h3>
          <p><?php echo lang('create_user_subheading');?></p>
          <div class="callout callout-info">
            <h4>Status!</h4>
            <p><?php echo $message;?></p>
          </div>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php echo form_open("administrator/user/create_user", array('class'=>'form-horizontal'));?>
          <div class="box-body">
            <div class="form-group">
              <label for="" class="col-sm-2 control-label"><?php echo lang('create_user_fname_label', 'first_name');?></label>

              <div class="col-sm-10">
                <?php echo form_input($first_name_2);?>
              </div>
            </div>
            <div class="form-group">
              <label for="" class="col-sm-2 control-label"><?php echo lang('create_user_lname_label', 'last_name');?></label>

              <div class="col-sm-10">
                <?php echo form_input($last_name);?>
              </div>
            </div>
            <?php
              if($identity_column!=='email') {
                  echo '<p>';
                  echo lang('create_user_identity_label', 'identity');
                  echo '<br />';
                  echo form_error('identity');
                  echo form_input($identity);
                  echo '</p>';
              }
            ?>
            <div class="form-group">
              <label for="" class="col-sm-2 control-label"><?php echo lang('create_user_company_label', 'company');?></label>

              <div class="col-sm-10">
                <?php echo form_input($company);?>
              </div>
            </div>
            <div class="form-group">
              <label for="" class="col-sm-2 control-label"><?php echo lang('create_user_email_label', 'email');?></label>

              <div class="col-sm-10">
                <?php echo form_input($email);?>
              </div>
            </div>
            <div class="form-group">
              <label for="" class="col-sm-2 control-label"><?php echo lang('create_user_phone_label', 'phone');?></label>

              <div class="col-sm-10">
                <?php echo form_input($phone);?>
              </div>
            </div>
            <div class="form-group">
              <label for="" class="col-sm-2 control-label"><?php echo lang('create_user_password_label', 'password');?></label>

              <div class="col-sm-10">
                <?php echo form_input($password);?>
              </div>
            </div>
            <div class="form-group">
              <label for="" class="col-sm-2 control-label"> <?php echo lang('create_user_password_confirm_label', 'password_confirm');?></label>

              <div class="col-sm-10">
                <?php echo form_input($password_confirm);?>
              </div>
            </div>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <?php echo form_submit('submit', lang('create_user_submit_btn'),array('class'=>'btn btn-info pull-right'));?>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>
<!-- /.content -->