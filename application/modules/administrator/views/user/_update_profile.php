<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Update
    <small>Profile</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url()?>administrator/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?=base_url()?>administrator/updateProfile">Menu Utama</a></li>
    <li class="active">Update Profile</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
<!-- SELECT2 EXAMPLE -->
<div class="box box-default">
<div class="box-header with-border">
  <h3 class="box-title">Form Update</h3>

  <div class="box-tools pull-right">
    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
  </div>
</div>
<!-- /.box-header -->
<div class="box-body">
  <div class="row">
    <div class="col-md-3">

      <!-- Profile Image -->
      <div class="box box-primary">
        <div class="box-body box-profile">
          <img class="profile-user-img img-responsive img-circle" src="<?=base_url()?>asset/image/logo.png" alt="User profile picture">

          <h3 class="profile-username text-center"><?=$profile['first_name']?> <?=$profile['last_name']?></h3>

          <p class="text-muted text-center"><?=$profile['email']?></p>

          <ul class="list-group list-group-unbordered">
            <li class="list-group-item">
              <b>Followers</b> <a class="pull-right">1,322</a>
            </li>
            <li class="list-group-item">
              <b>Following</b> <a class="pull-right">543</a>
            </li>
            <li class="list-group-item">
              <b>Friends</b> <a class="pull-right">13,287</a>
            </li>
          </ul>

          <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
    <div class="col-md-9">
      <div class="row">
        <div class="col-md-6">
        <?php echo form_open_multipart('', array('id'=>'form-profile', 'role'=>'form', 'name'=>'form-profile'));?>
          <div class="form-group">
            <label>Fist Name</label>
            <input type="text" name="first_name" class="form-control" value="<?=$profile['first_name']?>" placeholder="First Name">
          </div>
          <div class="form-group">
            <label>Last Name</label>
            <input type="text" name="last_name" class="form-control" value="<?=$profile['last_name']?>" placeholder="Last Nama">
          </div>
          <div class="form-group">
            <label>Company</label>
            <input type="text" name="company" class="form-control" value="<?=$profile['company']?>" placeholder="Company">
          </div>
          <div class="form-group">
            <label>Phone</label>
            <input type="text" name="phone" class="form-control" value="<?=$profile['phone']?>" placeholder="Phone">
          </div>
          <div class="form-group">
            <label>Email</label>
            <input type="email" name="email" class="form-control" value="<?=$profile['email']?>" placeholder="Email" disabled>
          </div>
          <!-- /.form-group -->
        </div>
        <!-- /.col -->
        <div class="col-md-6">
          <div class="form-group">
            <label>Password</label>
            <input type="password" name="password" class="form-control" value="" placeholder="Password">
          </div>
          <!-- /.form-group -->
          <div class="form-group">
            <label>Password Confirm</label>
            <input type="password" name="confirm_password" class="form-control" value="" placeholder="Confirm Password ">
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <button type="submit" class="btn btn-info pull-right">Update</button>
            </div>
          <?php echo form_close();?>
        </div>
      </div>
      <!-- /.nav-tabs-custom -->
    </div>
    <!-- /.col -->
  </div>
</div>
<!-- /.box-body -->
</div>
<!-- /.box -->
</section>
<!-- /.content -->
<script type="text/javascript">
  function readURL(input) {

    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $('#bsm').attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]);
    }
  }

  $("#imgInp").change(function() {
    readURL(this);
  });
</script>
<script>
 	$('form#form-profile').submit(function(){
		Component.upload( "<?=site_url('administrator/updateProfile/save')?>",'form-profile',function(response){
			if( response.status == 'info'){
				Component.show_alert('info',response.msg);
        setInterval(function(){ window.location.href = response.url; }, 3000);
			}else{
				Component.show_alert('error',response.msg);
			}
			Component.progressFull();
		});
		return false;
	});
</script>