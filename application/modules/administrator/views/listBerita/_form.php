<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">List Berita Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal" name="myForm">
                    <div class="callout callout-info">
                    <h4><i class="fa fa-info"></i> Note:</h4>
                        1. Max File Upload : 3 Mb <br>
                        2. Format Extensi File : gif,jpg,png <br>
                        3. File Upload = Width : 870px, Height : 530px <br>
                    </div>
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Judul</label>
                            <div class="col-md-9">
                                <input name="judul" placeholder="Judul" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Sub Judul</label>
                            <div class="col-md-9">
                                <input name="sub_judul" placeholder="Sub Judul" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Video Youtube</label>
                            <div class="col-md-9">
                                <input name="video" placeholder="Contoh link: http://www.youtube.com/embed/xbuEmoRWQHU" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Kategori</label>
                            <div class="col-md-9">
                                <select class="form-control" name="id_kategori_berita" style="width:100%;" id="id_kategori_berita">
                                  <option value="">Pilih Kategori Berita</option>
                                  <?php
                                    foreach ($kategori_berita as $row){
                                        echo "<option value='$row[id_kategori]'>$row[nama_kategori]</option>";
                                    }
                                  ?>
                               </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group" id="headline-preview">
                            <label class="control-label col-md-3">Headline</label>
                            <div class="col-md-9">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group" id="pilihan-preview">
                            <label class="control-label col-md-3">Pilihan</label>
                            <div class="col-md-9">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group" id="berita-preview">
                            <label class="control-label col-md-3">Berita Utama</label>
                            <div class="col-md-9">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group" id="isi-preview">
                            <label class="control-label col-md-3">Isi Berita</label>
                            <div class="col-md-9">
                                
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group" id="photo-preview">
                            <label class="control-label col-md-3">Photo</label>
                            <div class="col-md-9">
                                (No photo)
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3" id="label-photo">Upload Photo </label>
                            <div class="col-md-9">
                                <input type="file" name="photo">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Keterangan Gambar</label>
                            <div class="col-md-9">
                                <input name="keterangan_gambar" placeholder="Keterangan Gambar" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Tag</label>
                            <div class="col-md-9">
                                <?php
                                    foreach ($tag as $tag){
                                        echo "<span style='display:inline-block;'><input type=checkbox class='minimal-red' value='$tag[tag_seo]' name=tag[]>$tag[nama_tag] &nbsp; &nbsp; &nbsp; </span>";
                                    }
                                 ?>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
