<!-- Content Header (Page header) -->

<section class="content-header">

  <h1>

    List

    <small>Berita</small>

  </h1>

  <ol class="breadcrumb">

    <li><a href="<?=base_url()?>administrator/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>

    <li><a href="<?=base_url()?>administrator/listBerita">Menu Utama</a></li>

    <li class="active">List Berita</li>

  </ol>

</section>

<!-- Main content -->

<section class="content">

	<div class="box box-default">

	  <div class="box-header with-border">

	    <h3 class="box-title">Form Update List Berita</h3>

	    <div class="box-tools pull-right">



	    </div>

	  </div>

		<!-- /.box-header -->

		<?php echo form_open_multipart('', array('id'=>'form-list-berita', 'role'=>'form', 'name'=>'form-list-berita', 'class'=>'form-horizontal'));?>

	      	<div class="callout callout-info">

	        <h4><i class="fa fa-info"></i> Note:</h4>

	            1. Max File Upload : 3 Mb <br>

	            2. Format Extensi File : gif,jpg,png <br>

	            3. File Upload = Width : 870px, Height : 530px <br>

	        </div>

	      <input type="hidden" name="id" value="<?=$rows['id_berita']?>">

	      <div class="box-body">

	        <div class="form-group">

	          <label for="" class="col-sm-2 control-label">Judul</label>



	          <div class="col-sm-10">

	            <input type="text" name="judul" class="form-control" placeholder="Judul" value="<?=$rows['judul']?>">

	          </div>

	        </div>

	        <div class="form-group">

	          <label for="" class="col-sm-2 control-label">Sub Judul</label>



	          <div class="col-sm-10">

	            <input type="text" name="sub_judul" class="form-control" placeholder="Sub Judul" value="<?=$rows['sub_judul']?>">

	          </div>

	        </div>

	        <div class="form-group">

	          <label for="" class="col-sm-2 control-label">Video Youtube</label>



	          <div class="col-sm-10">

	            <input name="video" placeholder="Contoh link: http://www.youtube.com/embed/xbuEmoRWQHU" class="form-control" type="text" value="<?=$rows['youtube']?>">

	          </div>

	        </div>

	        <div class="form-group">

	          <label for="" class="col-sm-2 control-label">Kategori</label>



	          <div class="col-sm-10">

	            <select class="select2 form-control" name="id_kategori_berita" style="width:100%;" id="id_kategori_berita">

                  <option value="">Pilih Kategori Berita</option>

                  <?php

                    foreach ($kategori_berita as $row){

                    	if ($rows['id_kategori'] == $row['id_kategori']){

                          echo "<option value='$row[id_kategori]' selected>$row[nama_kategori]</option>";

                        }else{

                          echo "<option value='$row[id_kategori]'>$row[nama_kategori]</option>";

                        }

                    }

                  ?>

               </select>

	          </div>

	        </div>

	        



	          <!-- radio -->

	          <div class="form-group">

	          	<label for="" class="col-sm-2 control-label">Headline</label>

	          	<div class="col-sm-10">

	          		<?php

	          			if ($rows['headline']=='Y') {

	          		?>

	          			<label>

		                  <input type="radio" name="headline" value="Y" class="flat-red" checked>Ya

		                </label>

		                <label>

		                  <input type="radio" name="headline" value="N" class="flat-red">Tidak

		                </label>

	          		<?php

	          			}else{

	          		?>

	          			<label>

		                  <input type="radio" name="headline" value="Y" class="flat-red">Ya

		                </label>

		                <label>

		                  <input type="radio" name="headline" value="N" class="flat-red" checked>Tidak

		                </label>

	          		<?php

	          			}

	          		?>

	        	</div>

	          </div>

	          <!-- radio -->

	          <div class="form-group">

	          	<label for="" class="col-sm-2 control-label">Pilihan</label>

	          	<div class="col-sm-10">

	          		<?php

	          			if ($rows['aktif']=='Y') {

	          		?>

	          			<label>

		                  <input type="radio" name="pilihan" value="Y" class="flat-red" checked>Ya

		                </label>

		                <label>

		                  <input type="radio" name="pilihan" value="N" class="flat-red">Tidak

		                </label>

	          		<?php

	          			}else{

	          		?>

	          			<label>

		                  <input type="radio" name="pilihan" value="Y" class="flat-red">Ya

		                </label>

		                <label>

		                  <input type="radio" name="pilihan" value="N" class="flat-red" checked>Tidak

		                </label>

	          		<?php

	          			}

	          		?>

	        	</div>

	          </div>

	          <!-- radio -->

	          <div class="form-group">

	          	<label for="" class="col-sm-2 control-label">Berita Utama</label>

	          	<div class="col-sm-10">

	          		<?php

	          			if ($rows['utama']=='Y') {

	          		?>

	          			<label>

		                  <input type="radio" name="berita_utama" value="Y" class="flat-red" checked>Ya

		                </label>

		                <label>

		                  <input type="radio" name="berita_utama" value="N" class="flat-red">Tidak

		                </label>

	          		<?php

	          			}else{

	          		?>

	          			<label>

		                  <input type="radio" name="berita_utama" value="Y" class="flat-red">Ya

		                </label>

		                <label>

		                  <input type="radio" name="berita_utama" value="N" class="flat-red" checked>Tidak

		                </label>

	          		<?php

	          			}

	          		?>

	        	</div>

	          </div>

	        <div class="form-group">

	          <label for="" class="col-sm-2 control-label">Isi Berita</label>



	          <div class="col-sm-10">

	            <textarea name="isi_berita" id="isi_berita"><?=$rows['isi_berita']?></textarea>

	            <script type="text/javascript">

	            	CKEDITOR.replace( 'isi_berita' );

	            </script>

	          </div>

	        </div>

	        <div class="form-group">

	          <label for="" class="col-sm-2 control-label">Upload Photo</label>



	          <div class="col-sm-10">

	          	<?php

	          		if ($rows['gambar']!='') {

	          			echo '<i style="color:red;">Lihat Gambar Saat Ini : </i><a href="'.base_url().'asset/foto_berita/'.$rows['gambar'].'" target="_blank">'.$rows['gambar'].'</a><br />';

	          			echo '<input type="checkbox" name="remove_photo" value="'.$rows['gambar'].'"/> Check if you want to Remove photo when saving';

	          		}else{

	          			echo '(No Photo)';

	          		}

	          	?><br />

	            <input type="file" name="photo">

	          </div>

	        </div>

	        <div class="form-group">

	          <label for="" class="col-sm-2 control-label">Keterangan Gambar</label>



	          <div class="col-sm-10">

	            <input type="text" name="keterangan_gambar" class="form-control" placeholder="Keterangan Gambar" value="<?=$rows['keterangan_gambar']?>">

	          </div>

	        </div>

	        <div class="form-group">

	          <label for="" class="col-sm-2 control-label">Tag</label>



	          <div class="col-sm-10">

	            <?php

	            	$_arrNilai = explode(',', $rows['tag']);

                    foreach ($tag as $tag){

                        $_ck = (array_search($tag['tag_seo'], $_arrNilai) === false)? '' : 'checked';

                        echo "<span style='display:inline-block;'><input class='minimal-red' type=checkbox value='$tag[tag_seo]' name=tag[] $_ck>$tag[nama_tag] &nbsp; &nbsp; &nbsp; </span>";

                    }

	            ?>

	          </div>

	        </div>

	      </div>

	      <!-- /.box-body -->

	      <div class="box-footer">

	        <button type="submit" class="btn btn-info pull-right">Update</button>

	        <div id="element-progress" style="width:100px;"></div>

	      </div>

	      <!-- /.box-footer -->

	    <?php echo form_close();?>

	</div>

	<!-- /.box -->

</section>

<!-- /.content -->



<script>

 	$('form#form-list-berita').submit(function(){

 		CKEDITOR.instances.isi_berita.updateElement();

		Component.upload( "<?=site_url('administrator/listBerita/editListBerita')?>",'form-list-berita',function(response){

			if( response.status == 'info'){

				Component.show_alert('info',response.msg);

        setInterval(function(){ window.location.href = response.url; }, 3000);

			}else{

				Component.show_alert('error',response.msg);

			}

			Component.progressFull();

		});

		return false;

	});

</script>