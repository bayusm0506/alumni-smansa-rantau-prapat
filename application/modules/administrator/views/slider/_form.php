<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog" style="width:950px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Slide Form</h3>
            </div>
            <div class="modal-body form">
                <div class="callout callout-info">
                    <h4><i class="fa fa-info"></i> Note:</h4>
                    1. Max File Upload : 3 Mb <br>
                    2. Format Extensi File : gif,jpg,png <br>
                    3. For Photo Slide = Width : 800px, Height : 500px <br>
                    3. For Background Slide = Width : 1990px, Height : 500px 
                </div>
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Judul Slide</label>
                            <div class="col-md-9">
                                <input name="jdl_slide" placeholder="Judul Slide" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Dropcaps</label>
                            <div class="col-md-9">
                                <input name="jdl_color" placeholder="Dropcaps" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group" id="keterangan-preview">
                            <label class="control-label col-md-3">Keterangan</label>
                            <div class="col-md-9">
                                
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group" id="status-preview">
                            <label class="control-label col-md-3">Status</label>
                            <div class="col-md-9">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group" id="photo-preview">
                            <label class="control-label col-md-3">Photo</label>
                            <div class="col-md-9">
                                (No photo)
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3" id="label-photo">Upload Photo </label>
                            <div class="col-md-9">
                                <input type="file" name="photo">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group" id="bg-photo-preview">
                            <label class="control-label col-md-3">Background</label>
                            <div class="col-md-9">
                                (No Background)
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3" id="bg-label-photo">Upload Background </label>
                            <div class="col-md-9">
                                <input type="file" name="bg_slide">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->


