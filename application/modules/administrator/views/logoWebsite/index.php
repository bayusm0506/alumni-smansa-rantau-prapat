<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Logo
    <small>Website</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url()?>administrator/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?=base_url()?>administrator/logoWebsite">Modul Web</a></li>
    <li class="active">Logo Website</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title">Update Header</h3>
    <div class="box-tools pull-right">

    </div>
  </div>
<!-- /.box-header -->
  <?php
    foreach ($logo as $row) {
  ?>
    <?php echo form_open_multipart('administrator/logoWebsite/edit', array('id'=>'form-logo-website', 'role'=>'form', 'name'=>'form-logo-website'));?>
    <div class="box-body">
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <?php
              if (empty($row['gambar'])) {
                echo "Belum Ada Header Website.";
              }else{
                echo "<img width='100%' src='".base_url()."asset/logo/$row[gambar]'>";
                echo "<input type='checkbox' name='remove_photo' value='$row[gambar]'/> Remove photo when saving";
              }
            ?>
            
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>File Upload</label>
            <input type="hidden" name="id" value="<?=$row['id_logo']?>"">
            <input type="file" name="photo">
          </div>
          <div id="element-progress" style="width:100px;"></div>
        </div>
      </div>
    </div>
    <div class="box-footer">
      <button type="submit" class="btn btn-info">Update Header</button>
    </div>
    <?php echo form_close();?>


    <div class="box-header with-border">
      <h3 class="box-title">Update Logo</h3>
      <div class="box-tools pull-right">

      </div>
    </div>
    <?php echo form_open_multipart('administrator/logoWebsite/edit_logo', array('id'=>'form-logo-website-update', 'role'=>'form', 'name'=>'form-logo-website-update'));?>
    <div class="box-body">
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <?php
              if (empty($row['logo'])) {
                echo "Belum Ada Logo Website.";
              }else{
                echo "<img src='".base_url()."asset/logo/$row[logo]'><br />";
                echo "<input type='checkbox' name='remove_photo' value='$row[logo]'/> Remove photo when saving";
              }
            ?>
            
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>File Upload</label>
            <input type="hidden" name="id" value="<?=$row['id_logo']?>"">
            <input type="file" name="photo">
          </div>
          <div id="element-progress" style="width:100px;"></div>
        </div>
      </div>
    </div>
    <div class="box-footer">
      <button type="submit" class="btn btn-info">Update Logo</button>
    </div>
    <?php echo form_close();?>
  <?php
    }
  ?>
</div>
<!-- /.box -->
</section>
<!-- /.content -->
<script>
  $('form#form-logo-website').submit(function(){
    Component.upload( "<?=site_url('administrator/logoWebsite/edit')?>",'form-logo-website',function(response){
      if( response.status == 'info'){
        Component.show_alert('info',response.msg);
        setInterval(function(){ window.location.href = response.url; }, 3000);
      }else{
        Component.show_alert('error',response.msg);
      }
      Component.progressFull();
    });
    return false;
  });
</script>

<script>
  $('form#form-logo-website-update').submit(function(){
    Component.upload( "<?=site_url('administrator/logoWebsite/edit_logo')?>",'form-logo-website-update',function(response){
      if( response.status == 'info'){
        Component.show_alert('info',response.msg);
        setInterval(function(){ window.location.href = response.url; }, 3000);
      }else{
        Component.show_alert('error',response.msg);
      }
      Component.progressFull();
    });
    return false;
  });
</script>