<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?=$description;?> - <?=$title?></title>
  <meta name="author" content="bayusm.com">
  <?php
    if (isset($fav['favicon'])) {
      echo "<link href='".base_url()."asset/images/$fav[favicon]' type='image/x-icon' rel='shortcut icon'>";
    }else{
      echo "";
    }
  ?>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/dist/css/AdminLTE.min.css">

  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  
  
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/plugins/timepicker/bootstrap-timepicker.min.css">
  
  <!-- Google Font -->
  <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic"> -->
    <!-- jQuery 3 -->
  <script src="<?php echo base_url(); ?>asset/admin/bower_components/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="<?php echo base_url(); ?>asset/admin/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url(); ?>asset/admin/bower_components/ckeditor/ckeditor.js"></script>
  <!-- <script src="<?php echo base_url(''); ?>asset/ckeditor/ckeditor.js"></script> -->
  <!-- <script src="<?php echo base_url(); ?>asset/ckeditor/ckeditor.js"></script> -->
  <script src="<?php echo base_url(); ?>asset/admin/dist/js/ajax.js"></script>
  <script src="<?php echo base_url(); ?>asset/admin/dist/js/component.js"></script>
  <script src="<?php echo base_url(); ?>asset/admin/dist/js/bootbox.min.js"></script>
  <script src="<?php echo base_url(); ?>asset/admin/dist/sweetalert/sweetalert.min.js"></script>
  <script src="<?php echo base_url(); ?>asset/admin/bower_components/select2/dist/js/select2.min.js"></script>

  <script src="<?php echo base_url(); ?>asset/admin/dist/js/notify.min.js"></script>
  <script src="<?php echo base_url(); ?>asset/admin/dist/js/croppie.js"></script>

  <!-- DataTables -->
  <script src="<?php echo base_url(); ?>asset/admin/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url(); ?>asset/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
  <script src="<?php echo base_url(); ?>asset/admin/bower_components/datatables.net-bs/js/dataTables.colVis.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
  
  <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/dist/css/message.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/dist/css/croppie.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/dist/sweetalert/sweetalert.css">
  
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/bower_components/datatables.net-bs/css/dataTables.colVis.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
  <!-- <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script> -->
  <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/dist/js/html5shiv.min.js">
  <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/dist/js/respond.min.js"> -->
  
  <script src="<?php echo base_url(); ?>asset/admin/dist/js/jquery-ui.min.js"></script>
  <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/dist/css/jquery-ui.min.css">
  
  <style type="text/css">
    div.dataTables_wrapper div.dataTables_length label{
      float: right;
    }
    .animationload {
        background-color: #fff;
        height: 100%;
        left: 0;
        position: fixed;
        top: 0;
        width: 100%;
        z-index: 10000;
    }
    .osahanloading {
        animation: 1.5s linear 0s normal none infinite running osahanloading;
        background: #fed37f none repeat scroll 0 0;
        border-radius: 50px;
        height: 50px;
        left: 50%;
        margin-left: -25px;
        margin-top: -25px;
        position: absolute;
        top: 50%;
        width: 50px;
    }
    .osahanloading::after {
        animation: 1.5s linear 0s normal none infinite running osahanloading_after;
        border-color: #85d6de transparent;
        border-radius: 80px;
        border-style: solid;
        border-width: 10px;
        content: "";
        height: 80px;
        left: -15px;
        position: absolute;
        top: -15px;
        width: 80px;
    }
    @keyframes osahanloading {
    0% {
        transform: rotate(0deg);
    }
    50% {
        background: #85d6de none repeat scroll 0 0;
        transform: rotate(180deg);
    }
    100% {
        transform: rotate(360deg);
    }
    }

  </style>
  </head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="animationload">
    <div class="osahanloading"></div>
</div>
<script type="text/javascript">
  $(window).on('load', function() { // makes sure the whole site is loaded 
      $('.animationload').fadeOut(); // will first fade out the loading animation 
      $('.osahanloading').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website. 
      //$('body').delay(350).css({'overflow':'visible'});
    })
</script>
<div class="modal modal-danger fade" id="modal-compare">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <p id="m-message">&hellip;</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline pull-right" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="messsage-container" style="display:none">
  <div class="loading-temp loading-show"></div>
  <div class="center-loading"><a href="#" style="font-size:24px" class="btn btn-default btn-sm">Proses <i class="fa fa-spinner fa-spin" style="font-size:24px"></i></a></div>
</div>
<div class="message-container" style="display:none">
  <div class="center-message">
   <div class="alert alert-danger" id="container-m" role="alert"> <button id="remove-message" type="button" class="close"><i class="fa fa-window-close" style="font-size:32px" aria-hidden="true"></i></button> <h4 id="t-message"></h4> <p id="m-message"></p> </div> 
  </div>
</div>
<div class="wrapper">

  <?php $this->load->view('layouts/_top_menu',array('messages'=>$messages,'description'=>$description,'info_messages'=>$info_messages,'first_name'=>$first_name)); ?>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <?php $this->load->view('layouts/_sidebar_menu',array('first_name'=>$first_name)); ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    