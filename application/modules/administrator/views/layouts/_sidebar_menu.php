<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">
  <!-- Sidebar user panel -->
  <div class="user-panel">
    <div class="pull-left image">
      <img src="<?php echo base_url(); ?>asset/image/logo.png" class="img-circle" alt="User Image">
    </div>
    <div class="pull-left info">
      <p><?php echo $first_name?></p>
      
      <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
    </div>
  </div>
  <!-- search form -->
  <form action="#" method="get" class="sidebar-form">
    <div class="input-group">
      <input type="text" name="q" class="form-control" placeholder="Search...">
      <span class="input-group-btn">
            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
            </button>
          </span>
    </div>
  </form>
  <!-- /.search form -->
  <!-- sidebar menu: : style can be found in sidebar.less -->
  <ul class="sidebar-menu" data-widget="tree">
    <li class="header">MAIN NAVIGATION</li>
    <li><a href="<?php echo base_url(); ?>administrator/dashboard"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
    <li class="treeview">
      <a href="#"><i class="glyphicon glyphicon-qrcode"></i> <span>Alumni SMANSA</span><i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">
        <li><a href="<?php echo base_url(); ?>administrator/alumni"><i class="fa fa-circle-o"></i> Alumni</a></li>
        <li><a href="<?php echo base_url(); ?>administrator/grafikAlumni"><i class="fa fa-circle-o"></i>Grafik Alumni</a></li>
        <li><a href="<?php echo base_url(); ?>administrator/jurusan"><i class="fa fa-circle-o"></i>Jurusan</a></li>
        <li><a href="<?php echo base_url(); ?>administrator/gelar"><i class="fa fa-circle-o"></i>Gelar</a></li>
        <li><a href="<?php echo base_url(); ?>administrator/kepalaSekolah"><i class="fa fa-circle-o"></i>Kepala Sekolah</a></li>
      </ul>
    </li>
    <li class="treeview">
      <a href="#"><i class="glyphicon glyphicon-blackboard"></i> <span>Ads</span><i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">
        <li><a href="<?php echo base_url(); ?>administrator/adsHome"><i class="fa fa-circle-o"></i> Ads Home</a></li>
        <li><a href="<?php echo base_url(); ?>administrator/adsBlog"><i class="fa fa-circle-o"></i>Ads Blog</a></li>
        <li><a href="<?php echo base_url(); ?>administrator/adsDetailAlumni"><i class="fa fa-circle-o"></i>Ads Detail Alumni</a></li>
      </ul>
    </li>
    <li class="treeview">
      <a href="#"><i class="glyphicon glyphicon-th-list"></i> <span>Menu Utama</span><i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">
        <li><a href="<?php echo base_url(); ?>administrator/identitasWebsite"><i class="fa fa-circle-o"></i> Identitas Website</a></li>
        <li><a href="<?php echo base_url(); ?>administrator/menuWebsite"><i class="fa fa-circle-o"></i> Menu Website</a></li>
        <!-- <li><a href="<?php echo base_url(); ?>administrator/menuGroup"><i class="fa fa-circle-o"></i> Menu Group</a></li>
        <li><a href="<?php echo base_url(); ?>administrator/menuGroupList"><i class="fa fa-circle-o"></i> Menu Group List</a></li> -->
        <li><a href="<?php echo base_url(); ?>administrator/halamanBaru"><i class="fa fa-circle-o"></i> Halaman Baru</a></li>
      </ul>
    </li>
    <li class="treeview">
      <a href="#"><i class="glyphicon glyphicon-pencil"></i> <span>Modul Berita</span><i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">
        <li><a href="<?php echo base_url(); ?>administrator/listBerita"><i class="fa fa-circle-o"></i> List Berita</a></li>
        <li><a href="<?php echo base_url(); ?>administrator/kategoriBerita"><i class="fa fa-circle-o"></i> Kategori Berita</a></li>
        <li><a href="<?php echo base_url(); ?>administrator/tagBerita"><i class="fa fa-circle-o"></i> Tag Berita</a></li>
      </ul>
    </li>

    <li class="treeview">
      <a href="#"><i class="glyphicon glyphicon-picture"></i> <span>Modul Gallery</span><i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">
        <li><a href='<?php echo base_url(); ?>administrator/album'><i class='fa fa-circle-o'></i> Album</a></li>
        <li><a href='<?php echo base_url(); ?>administrator/gallery'><i class='fa fa-circle-o'></i> Gallery</a></li>
        <li><a href='<?php echo base_url(); ?>administrator/linkImages'><i class='fa fa-circle-o'></i> Link Images</a></li>
        <!-- <li><a href='<?php echo base_url(); ?>administrator/slider'><i class='fa fa-circle-o'></i> Slider</a></li>
        <li><a href="<?php echo base_url(); ?>administrator/pageGallery"><i class="fa fa-circle-o"></i> Gallery Halaman</a></li>
        <li><a href="<?php echo base_url(); ?>administrator/blogGallery"><i class="fa fa-circle-o"></i> Gallery Blog</a></li> -->
      </ul>
    </li>

    <!-- <li class="treeview">
      <a href="#"><i class="glyphicon glyphicon-play"></i> <span>Modul Video</span><i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">
        <li><a href='<?php echo base_url(); ?>administrator/playlist'><i class='fa fa-circle-o'></i> Playlist Video</a></li>
        <li><a href='<?php echo base_url(); ?>administrator/video'><i class='fa fa-circle-o'></i> Video</a></li>
        <li><a href='<?php echo base_url(); ?>administrator/tagVideo'><i class='fa fa-circle-o'></i> Tag Video</a></li>
      </ul>
    </li> -->
    
    <li class="treeview">
      <a href="#"><i class="glyphicon glyphicon-object-align-left"></i> <span>Modul Web</span><i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">
        <!-- <li><a href="<?php echo base_url(); ?>administrator/logoWebsite"><i class="fa fa-circle-o"></i> Logo Website</a></li>
        <li><a href="<?php echo base_url(); ?>administrator/lowker"><i class="fa fa-circle-o"></i> Info Lowker</a></li>
        <li><a href="<?php echo base_url(); ?>administrator/download"><i class="fa fa-circle-o"></i> File Download</a></li>
        <li><a href="<?php echo base_url(); ?>administrator/agenda"><i class="fa fa-circle-o"></i> List Agenda</a></li>
        <li><a href="<?php echo base_url(); ?>administrator/linkTerkait"><i class="fa fa-circle-o"></i> Link Terkait</a></li> -->
        <li><a href="<?php echo base_url(); ?>administrator/pesanMasuk"><i class="fa fa-circle-o"></i> Pesan Masuk</a></li>
      </ul>
    </li>
    <li class="treeview">
      <a href="#"><i class="fa fa-cog"></i> <span>Modul Users</span><i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">
        <li><a href="<?php echo base_url(); ?>administrator/user"><i class="fa fa-circle-o"></i> Manajemen User</a></li>
        <li><a href="<?php echo base_url(); ?>administrator/groupUser"><i class="fa fa-circle-o"></i> Groups User</a></li>
      </ul>
    </li>
    <li><a href="<?php echo base_url(); ?>administrator/updateProfile"><i class="fa fa-user"></i> <span>Edit Profile</span></a></li>
    <li><a href="<?php echo base_url(); ?>administrator/auth/logout"><i class="fa fa-power-off"></i> <span>Logout</span></a></li>
    <li class="header">LABELS</li>
    <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
    <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
    <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
  </ul>
</section>
<!-- /.sidebar -->