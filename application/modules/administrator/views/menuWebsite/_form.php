<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Menu Website Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Link Menu</label>
                            <div class="col-md-9">
                                <input name="link_menu" placeholder="Link Menu" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Level Menu</label>
                            <div class="col-md-9">
                                <select class="form-control" name="level_menu" style="width:100%;" id="level_menu_f">
                                  <option value="">Pilih Level Menu</option>
                                  <option value="0">Menu Utama</option>
                                  <?php
                                    if ($menu_utama->num_rows() > 0)
                                    {
                                       foreach ($menu_utama->result() as $row)
                                       {
                                  ?>
                                    <option value="<?=$row->id_menu?>"><?=$row->nama_menu?></option>    
                                  <?php
                                       }
                                    }
                                  ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Nama Menu</label>
                            <div class="col-md-9">
                                <input name="nama_menu" placeholder="Nama Menu" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group" id="posisi-preview">
                            <label class="control-label col-md-3">Position</label>
                            <div class="col-md-9">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group" id="status-preview">
                            <label class="control-label col-md-3">Status</label>
                            <div class="col-md-9">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Urutan</label>
                            <div class="col-md-9">
                                <input name="urutan" placeholder="Urutan Contoh : 1" class="form-control" type="number">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->


