<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Link
    <small>Images</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url()?>administrator/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?=base_url()?>administrator/linkImages">Modul Gallery</a></li>
    <li class="active">Link Images</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title">Data Link Images</h3>
    <div class="box-tools pull-right">
      <button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Reload</button>
      <button type="button" class="btn bg-maroon btn-flat" onclick="add_link()">
          <i class="fa fa-plus"></i> Tambah Data
      </button>
      <!-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button> -->
    </div>
  </div>
<!-- /.box-header -->
  <?php $this->load->view('linkImages/_dataTable'); ?>
</div>
<!-- /.box -->
</section>
<!-- /.content -->

<script type="text/javascript">
  var save_method; //for save method string
  var table;
  var base_url = '<?php echo base_url();?>';

  $(document).ready(function() {
      //datatables
      table = $('#table').DataTable({ 
          language: {
              //search: "_INPUT_",
              searchPlaceholder: "Cari Disini..."
          },
          dom: 'Blfrtip',
          buttons: [
              'copyHtml5',
              'excelHtml5',
              'csvHtml5',
              'pdfHtml5'
          ],
          "pagingType": "full_numbers",
          "lengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]],
          "processing": true, //Feature control the processing indicator.
          "serverSide": true, //Feature control DataTables' server-side processing mode.
          "order": [], //Initial no order.

          // Load data for the table's content from an Ajax source
          "ajax": {
              "url": "<?php echo site_url('administrator/linkImages/ajax_list')?>",
              "type": "POST",
              "data": function ( data ) {
                  // data.jdl_link = $('#jdl_link').val();
                  // data.status_link = $('#status_s').val();
              }
          },

          //Set column definition initialisation properties.
          "columnDefs": [
            { 
                "targets": [ 0,-1 ], //last column
                "orderable": false, //set not orderable
            },
          ],

      });

      $("input").change(function(){
          $(this).parent().parent().removeClass('has-error');
          $(this).next().empty();
      });
  });

  $('#btn-filter').click(function(){ //button filter event click
      table.ajax.reload();  //just reload table
  });
  
  $('#btn-reset').click(function(){ //button reset event click
      $('#form-filter')[0].reset();
      table.ajax.reload();  //just reload table
  });

  function add_link()
  {
      save_method = 'add';
      $('#form')[0].reset(); // reset form on modals
      $('.form-group').removeClass('has-error'); // clear error class
      $('.help-block').empty(); // clear error string
      $('#modal_form').modal('show'); // show bootstrap modal
      $('.modal-title').text('Tambah Link Images'); // Set Title to Bootstrap modal title
      $('#photo-preview').hide(); // hide photo preview modal
      $('#label-photo').text('Upload Photo'); // label photo upload
  }

  function reload_table(){
      table.ajax.reload(null,false); //reload datatable ajax 
  }

  function delete_link(id){
    swal({
      title: "Are you sure?",
      text: "You will delete this Record!",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes, delete it!",
      cancelButtonText: "No, cancel plx!",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm) {
      if (isConfirm) {
        $.ajax({
          type: 'POST',
          url: '<?php echo site_url('administrator/linkImages/delete')?>',
          data: 'empid='+id
        });
        reload_table();
        swal("Deleted!", "Record has been deleted.", "success");

      } else {
        swal("Cancelled", "Your Record is safe :)", "error");
      }
    });
  }
</script>

<?php $this->load->view('linkImages/_form'); ?>

<script type="text/javascript">

  function edit_link(id)
  {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
      $('.form-group').removeClass('has-error'); // clear error class
      $('.help-block').empty(); // clear error string


      //Ajax Load data from ajax
      $.ajax({
          url : "<?php echo site_url('administrator/linkImages/ajax_edit')?>/" + id,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {
              
              $('[name="id"]').val(data.id_link);
              $('[name="jdl_link"]').val(data.jdl_link);
              $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
              $('.modal-title').text('Edit Link Images'); // Set title to Bootstrap modal title
              $('#photo-preview').show(); // show photo preview modal

              if(data.gbr_link)
              {
                  $('#label-photo').text('Change Photo'); // label photo upload
                  $('#photo-preview div').html('<img src="'+base_url+'asset/img_link/'+data.gbr_link+'" class="img-responsive">'); // show photo
                  $('#photo-preview div').append('<input type="checkbox" name="remove_photo" value="'+data.gbr_link+'"/> Remove photo when saving'); // remove photo
              }
              else
              {
                  $('#label-photo').text('Upload Photo'); // label photo upload
                  $('#photo-preview div').text('(No photo)');
              }
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error get data from ajax');
          }
      });
  }

  function save()
  {
      $('#btnSave').text('saving...'); //change button text
      $('#btnSave').attr('disabled',true); //set button disable 
      var url;

      if(save_method == 'add') {
          url = "<?php echo site_url('administrator/linkImages/save')?>";
      } else {
          url = "<?php echo site_url('administrator/linkImages/edit')?>";
      }

      // ajax adding data to database

      var formData = new FormData($('#form')[0]);
      $.ajax({
          url : url,
          type: "POST",
          data: formData,
          contentType: false,
          processData: false,
          dataType: "JSON",
          success: function(data)
          {

              if(data.status) //if success close modal and reload ajax table
              {
                  $('#modal_form').modal('hide');
                  reload_table();
                  //Ajax.show_alert('info',data.msg);
                  $.notify(data.msg,"success");
              }
              else
              {
                  for (var i = 0; i < data.inputerror.length; i++) 
                  {
                      $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                      $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                  }
              }
              $('#btnSave').text('save'); //change button text
              $('#btnSave').attr('disabled',false); //set button enable 


          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error adding / update data');
              $('#btnSave').text('save'); //change button text
              $('#btnSave').attr('disabled',false); //set button enable 
          }
      });
  }
</script>