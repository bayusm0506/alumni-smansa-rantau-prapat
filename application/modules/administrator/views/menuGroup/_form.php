<!-- MODAL ADD -->
<div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog" style="width:750px;">
    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="modal-title"></h3>

        <div class="box-tools pull-right">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-window-close" style="font-size:32px" aria-hidden="true"></i></button>
        </div>
      </div>
      <!-- /.box-header -->
      <!-- form start -->
      <form action="#" id="form" class="form-horizontal"> 
        <input type="hidden" value="" name="id"/> 
        <div class="box-body">
          <div class="form-group">
            <label for="nama_group_menu" class="col-sm-2 control-label">Nama Group Menu</label>

            <div class="col-sm-10">
              <input name="nama_group_menu" placeholder="Nama Group Menu" class="form-control" type="text">
              <span class="help-block"></span>
            </div>
          </div>
          <div class="form-group">
            <label for="status" class="col-sm-2 control-label">Status</label>
            <div class="col-sm-10">
                <select name="status" class="form-control" style="width:100%;" id="status_f">
                    <option value="">Pilih Status</option>
                    <option value="Y">Aktif</option>
                    <option value="N">Tidak Aktif</option>
                </select>
                <span class="help-block"></span>
            </div>
          </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <button type="button"  class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
          <button type="button" id="btnSave" onclick="save()" class="btn btn-info pull-right">Simpan</button>
        </div>
        <!-- /.box-footer -->
      </form>
      <!-- /.box-body -->
      <!-- <div class="box-footer">
        Visit <a href="https://select2.github.io/">Select2 documentation</a> for more examples and information about
        the plugin.
      </div> -->
    </div>
  </div>
</div>
<!-- END MODAL ADD -->