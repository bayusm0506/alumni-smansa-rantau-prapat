Highcharts.chart('container', {
				title: {
					text: 'Combination chart'
				},
				xAxis: {
					categories: [
						<?php 
							// data yang diambil dari database
							if(count($grafik)>0)
							{
							   foreach ($grafik as $data) {
							   echo "['" .tgl_grafik($data->tanggal)."'],\n";
							   }
							}
						?>
					]
				},
				
				yAxis: {
					title: {
						text: 'Total Pengunjung',
						style: {
							color: Highcharts.getOptions().colors[0]
						}
					}
				},
				
				labels: {
					items: [{
						//html: 'Total Pengunjung',
						style: {
							left: '50px',
							top: '18px',
							color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
						}
					}]
				},
				series: [
				/*
				{
					type: 'column',
					colorByPoint: true,
					name: 'Pengunjung',
					data: [
						<?php 
							// data yang diambil dari database
							if(count($grafik)>0)
							{
							   foreach ($grafik as $data) {
							   echo $data->jumlah.",";
							   }
							}
						?>
					],
				},
				*/
				{
					type: 'spline',
					name: 'Pengunjung',
					data: [
						<?php 
							// data yang diambil dari database
							if(count($grafik)>0)
							{
							   foreach ($grafik as $data) {
							   echo $data->jumlah.",";
							   }
							}
						?>
					],
					marker: {
						lineWidth: 2,
						lineColor: Highcharts.getOptions().colors[3],
						fillColor: 'white'
					}
				},
					
				{
					type: 'pie',
					//name: 'Total Pengunjung',
					data: [
						<?php 
							// data yang diambil dari database
							if(count($grafik)>0)
							{
							   foreach ($grafik as $data) {
							   echo "['" .tgl_grafik($data->tanggal) . "'," . $data->jumlah ."],\n";
							   }
							}
						?>
					],
					center: [30, 0],
					size: 100,
					showInLegend: false,
					dataLabels: {
						enabled: false
					}
				}]
			});