<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Video
    <small>Playlist</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url()?>administrator/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?=base_url()?>administrator/video">Modul Video</a></li>
    <li class="active">Video</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="box box-default">
	  <div class="box-header with-border">
	    <h3 class="box-title">Form Update Video</h3>
	    <div class="box-tools pull-right">

	    </div>
	  </div>
		<!-- /.box-header -->
		<?php echo form_open_multipart('', array('id'=>'form-video', 'role'=>'form', 'name'=>'form-video', 'class'=>'form-horizontal'));?>
	      <input type="hidden" name="id" value="<?=$rows['id_video']?>">
	      <div class="box-body">
	        <div class="form-group">
	          <label for="" class="col-sm-2 control-label">Judul Video</label>

	          <div class="col-sm-10">
	            <input type="text" name="jdl_video" class="form-control" placeholder="Judul Video" value="<?=$rows['jdl_video']?>">
	          </div>
	        </div>
	        <div class="form-group">
	          <label for="" class="col-sm-2 control-label">Playlist</label>

	          <div class="col-sm-10">
	            <select class="select2 form-control" name="id_playlist" style="width:100%;">
                  <option value="">Pilih Playlist</option>
                  <?php
                    foreach ($playlist as $row){
                    	if ($rows['id_playlist'] == $row['id_playlist']){
                          echo "<option value='$row[id_playlist]' selected>$row[jdl_playlist]</option>";
                        }else{
                          echo "<option value='$row[id_playlist]'>$row[jdl_playlist]</option>";
                        }
                    }
                  ?>
               </select>
	          </div>
	        </div>
	        <div class="form-group">
	          <label for="" class="col-sm-2 control-label">Keterangan</label>

	          <div class="col-sm-10">
	            <textarea name="keterangan" id="keterangan" value="<?=$rows['keterangan']?>"><?=$rows['keterangan']?></textarea>
	            <script type="text/javascript">
	            	CKEDITOR.replace( 'keterangan' );
	            </script>
	          </div>
	        </div>
	        <div class="form-group">
	          <label for="" class="col-sm-2 control-label">Upload Photo</label>

	          <div class="col-sm-10">
	          	<?php
	          		if ($rows['gbr_video']!='') {
	          			echo '<i style="color:red;">Lihat Gambar Saat Ini : </i><a href="'.base_url().'asset/img_video/'.$rows['gbr_video'].'" target="_blank">'.$rows['gbr_video'].'</a><br />';
	          			echo '<input type="checkbox" name="remove_photo" value="'.$rows['gbr_video'].'"/> Check if you want to Remove photo when saving';
	          		}else{
	          			echo '(No Photo)';
	          		}
	          	?><br />
	          	
	            <input type="file" name="photo">
	          </div>
	        </div>
	        <div class="form-group">
	          <label for="" class="col-sm-2 control-label">Link Youtube</label>

	          <div class="col-sm-10">
	            <input name="youtube" placeholder="Contoh link: http://www.youtube.com/embed/xbuEmoRWQHU" class="form-control" type="text" value="<?=$rows['youtube']?>">
	          </div>
	        </div>
	        <div class="form-group">
	          <label for="" class="col-sm-2 control-label">Tag</label>

	          <div class="col-sm-10">
	            <?php
	            	$_arrNilai = explode(',', $rows['tagvid']);
                    foreach ($tagvid as $tag){
                        $_ck = (array_search($tag['tag_seo'], $_arrNilai) === false)? '' : 'checked';
                        echo "<span style='display:inline-block;'><input class='minimal-red' type=checkbox value='$tag[tag_seo]' name=tag[] $_ck>$tag[nama_tag] &nbsp; &nbsp; &nbsp; </span>";
                    }
	            ?>
	          </div>
	        </div>
	      </div>
	      <!-- /.box-body -->
	      <div class="box-footer">
	        <button type="submit" class="btn btn-info pull-right">Update</button>
	        <div id="element-progress" style="width:100px;"></div>
	      </div>
	      <!-- /.box-footer -->
	    <?php echo form_close();?>
	</div>
	<!-- /.box -->
</section>
<!-- /.content -->

<script>
 	$('form#form-video').submit(function(){
 		CKEDITOR.instances.keterangan.updateElement();
		Component.upload( "<?=site_url('administrator/video/edit')?>",'form-video',function(response){
			if( response.status == 'info'){
				Component.show_alert('info',response.msg);
        		setInterval(function(){ window.location.href = response.url; }, 3000);
			}else{
				Component.show_alert('error',response.msg);
			}
			Component.progressFull();
		});
		return false;
	});
</script>