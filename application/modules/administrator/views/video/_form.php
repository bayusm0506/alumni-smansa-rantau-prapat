<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog" style="width:950px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Video Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal" name="myForm">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Judul Video</label>
                            <div class="col-md-9">
                                <input name="jdl_video" placeholder="Judul Video" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Playlist</label>
                            <div class="col-md-9">
                                <select class="form-control" name="id_playlist" style="width:100%;">
                                  <option value="">Pilih Playlist</option>
                                  <?php
                                    foreach ($playlist as $row){
                                        echo "<option value='$row[id_playlist]'>$row[id_playlist]-$row[jdl_playlist]</option>";
                                    }
                                  ?>
                               </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group" id="isi-preview">
                            <label class="control-label col-md-3">Keterangan</label>
                            <div class="col-md-9">
                                
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group" id="photo-preview">
                            <label class="control-label col-md-3">Photo</label>
                            <div class="col-md-9">
                                (No photo)
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3" id="label-photo">Upload Photo </label>
                            <div class="col-md-9">
                                <input type="file" name="photo">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Link Youtube</label>
                            <div class="col-md-9">
                                <input name="youtube" placeholder="Contoh link: http://www.youtube.com/embed/xbuEmoRWQHU" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Tag</label>
                            <div class="col-md-9">
                                <?php
                                    foreach ($tagvid as $tag){
                                        echo "<span style='display:inline-block;'><input type=checkbox class='minimal-red' value='$tag[tag_seo]' name=tag[]>$tag[nama_tag] &nbsp; &nbsp; &nbsp; </span>";
                                    }
                                 ?>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
