<div class="box box-default" id="close-search" style="display: none;">
  <div class="box-header with-border">
    <h3 class="box-title">Custom Filter : </h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <form id="form-filter" class="form-horizontal">
      <div class="col-lg-4">
        <label for="judul">Judul Video</label>        
        <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa fa-object-group fa-lg" aria-hidden="true"></i>
            </div>
            <input class="form-control" placeholder="Judul Video" type="text" id="jdl_video" />          
        </div>
      </div>
      <div class="col-lg-4">
        <label for="judul">Tanggal Posting</label>        
        <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa-calendar fa-lg" aria-hidden="true"></i>
            </div>
            <input class="form-control" placeholder="Tanggal Posting" type="text" id="datepicker" />
        </div>
      </div>
      <div class="col-lg-4">
          <label for="status">Playlist</label>        
          <div class="input-group">
              <div class="input-group-addon">
                <i class="fa fa fa-check fa-lg" aria-hidden="true"></i>
              </div>
              <select class="select2 form-control" name="id_playlist" style="width:100%;" id="id_playlist_s">
                <option value="">Pilih Playlist</option>
                <?php
                  foreach ($playlist as $row){
                      echo "<option value='$row[id_playlist]'>$row[jdl_playlist]</option>";
                  }
                ?>
             </select>
          </div>
        </div>
      <div class="col-lg-6">
        <label for="filter"></label>        
        <div class="input-group">
            <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
            <button type="button" id="btn-reset" class="btn btn-default">Reset</button>
        </div>
      </div>
    </form>
  </div>
  <!-- /.box-body -->
  <div class="box-footer">
    <!-- box-footer -->
  </div>
</div>