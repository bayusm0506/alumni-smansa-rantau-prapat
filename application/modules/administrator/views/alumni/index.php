<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Alumni
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url()?>administrator/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?=base_url()?>administrator/alumni">Alumni SMANSA</a></li>
    <li class="active">Alumni</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
<!-- SELECT2 EXAMPLE -->
<?php $this->load->view('alumni/_search'); ?>
<?php $this->load->view('alumni/_form'); ?>
<!-- SELECT2 EXAMPLE -->
<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title">Data Alumni</h3>
    <div class="box-tools pull-right">
      <button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Reload</button>
      <button type="button" class="btn bg-maroon btn-flat" onclick="add_alumni()">
          <i class="fa fa-plus"></i> Tambah Data
      </button>
      <button class="btn bg-olive btn-flat" onclick="custom_filter()">
          <i class="fa fa-gear"></i> <span id="custom"></span>
      </button>
      <!-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button> -->
    </div>
  </div>
<!-- /.box-header -->
  <?php $this->load->view('alumni/_dataTable'); ?>
</div>
<!-- /.box -->
</section>
<!-- /.content -->

<script type="text/javascript">
  var save_method; //for save method string
  var table;
  var base_url = '<?php echo base_url();?>';
  $("#custom").text('Filter');
  $( "#nama" ).autocomplete({
    source: "<?php echo site_url('administrator/alumni/get_autocomplete/?');?>"
  });

  function custom_filter() {
      var x = document.getElementById("close-search");
      if (x.style.display === "block") {
          x.style.display = "none";
          $("#custom").text('Filter');
      } else {
          x.style.display = "block";
          $("#custom").text('Close Filter');
      }
  }

  $(document).ready(function() {
      //datatables
      table = $('#table').DataTable({ 
          language: {
              //search: "_INPUT_",
              searchPlaceholder: "Cari Disini..."
          },
          dom: 'Blfrtip',
          buttons: [
              'copyHtml5',
              'excelHtml5',
              'csvHtml5',
              'pdfHtml5'
          ],
          "pagingType": "full_numbers",
          "lengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]],
          "processing": true, //Feature control the processing indicator.
          "serverSide": true, //Feature control DataTables' server-side processing mode.
          "order": [], //Initial no order.

          // Load data for the table's content from an Ajax source
          "ajax": {
              "url": "<?php echo site_url('administrator/alumni/ajax_list')?>",
              "type": "POST",
              "data": function ( data ) {
                  data.nama = $('#nama').val();
                  data.tgl_lahir = $('#datepicker').val();
                  data.tgl_terdaftar = $('#datepicker_terdaftar').val();
                  data.stambuk = $('#stambuk_s').val();
                  data.tahun_lulus = $('#thn_lulus_s').val();
                  data.id_jurusan = $('#jurusan_s').val();
                  data.id_gelar = $('#gelar_s').val();
              }
          },

          //Set column definition initialisation properties.
          "columnDefs": [
            { 
                "targets": [ 0,-1,-2 ], //last column
                "orderable": false, //set not orderable
            },
          ],

      });

      $("input").change(function(){
          $(this).parent().parent().removeClass('has-error');
          $(this).next().empty();
      });

      $("select#id_jurusan_f").change(function(){
          $(this).parent().parent().removeClass('has-error');
          $(this).next().empty();
      });

      $("select#id_gelar_f").change(function(){
          $(this).parent().parent().removeClass('has-error');
          $(this).next().empty();
      });
  });

  $('#btn-filter').click(function(){ //button filter event click
      table.ajax.reload();  //just reload table
  });
  
  $('#btn-reset').click(function(){ //button reset event click
      $('#form-filter')[0].reset();
      $("select#jurusan_s").val('').trigger('change');
      $("select#stambuk_s").val('').trigger('change');
      $("select#thn_lulus_s").val('').trigger('change');
      $("select#gelar_s").val('').trigger('change');
      table.ajax.reload();  //just reload table
  });

  function add_alumni()
  {
      save_method = 'add';
      $('#form')[0].reset(); // reset form on modals
      $('.form-group').removeClass('has-error'); // clear error class
      $('.help-block').empty(); // clear error string
      $('#modal_form').modal('show'); // show bootstrap modal
      $('.modal-title').text('Tambah Alumni'); // Set Title to Bootstrap modal title

      $('#photo-preview').hide(); // hide photo preview modal

      $('#label-photo').text('Upload Photo'); // label photo upload

      $('#tertulis div').html('<input name="tgl_lahir" class="form-control" placeholder="Tanggal Lahir" type="text" id="tesla" />'); // show photo
      $('#tesla').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true,
      });

      $('#tertulis_daftar div').html('<input name="tgl_terdaftar" class="form-control" placeholder="Tanggal Terdaftar" type="text" id="tgl_terdaftar" />'); // show photo
      $('#tgl_terdaftar').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true,
      });
  }

  function reload_table()
  {
      table.ajax.reload(null,false); //reload datatable ajax 
  }

  function delete_alumni(id)
  {
    swal({
      title: "Are you sure?",
      text: "You will delete this Record!",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes, delete it!",
      cancelButtonText: "No, cancel plx!",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm) {
      if (isConfirm) {
        $.ajax({
          type: 'POST',
          url: '<?php echo site_url('administrator/alumni/delete')?>',
          data: 'empid='+id
        });
        reload_table();
        swal("Deleted!", "Record has been deleted.", "success");

      } else {
        swal("Cancelled", "Your Record is safe :)", "error");
      }
    });
  }
</script>

<script type="text/javascript">

  function edit_alumni(id)
  {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
      $('.form-group').removeClass('has-error'); // clear error class
      $('.help-block').empty(); // clear error string


      //Ajax Load data from ajax
      $.ajax({
          url : "<?php echo site_url('administrator/alumni/ajax_edit')?>/" + id,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {
              
              $('[name="id"]').val(data.siswa_id);
              $('[name="id_siswa"]').val(data.id);
              $('[name="nama"]').val(data.nama);
              $('[name="tempat_lahir"]').val(data.tempat_lahir);
              $('[name="alamat_lama"]').val(data.alamat_lama);
              $('[name="alamat_baru"]').val(data.alamat_baru);
              $('[name="stambuk"]').val(data.stambuk);
              $('[name="id_jurusan"]').val(data.id_jurusan);

              $('[name="id_jurusan_cek"]').val(data.id_jurusan);
              $('[name="stambuk_cek"]').val(data.stambuk);
              
              $('[name="tahun_lulus"]').val(data.tahun_lulus);
              $('[name="tahun_pindah"]').val(data.tahun_pindah);
              $('[name="sekolah_pindah"]').val(data.sekolah_pindah);
              $('[name="id_gelar"]').val(data.id_gelar);
              $('[name="profesi"]').val(data.profesi);
              $('[name="nama_keluarga"]').val(data.nama_keluarga);
              $('[name="profesi_keluarga"]').val(data.profesi_keluarga);
              $('[name="hobi"]').val(data.hobi);
              $('[name="kd_anggota"]').val(data.kd_anggota);
              $('[name="facebook"]').val(data.facebook);
              $('[name="instagram"]').val(data.instagram);
              $('[name="twitter"]').val(data.twitter);
              $('[name="youtube"]').val(data.youtube);
              $('[name="tanggal_cek"]').val(data.tgl_input);
              $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
              $('.modal-title').text('Edit Alumni'); // Set title to Bootstrap modal title
              $('#photo-preview').show(); // show photo preview modal

              $('#tertulis div').html('<input name="tgl_lahir" class="form-control" placeholder="Tanggal Lahir" type="text" id="tesla" value="'+data.tgl_lahir+'"/>'); // show photo
              $('#tesla').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                todayHighlight: true,
              })

              $('#tertulis_daftar div').html('<input name="tgl_terdaftar" class="form-control" placeholder="Tanggal Terdaftar" type="text" id="tgl_terdaftar" value="'+data.tgl_terdaftar+'"/>'); // show photo
              $('#tgl_terdaftar').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                todayHighlight: true,
              })

              if(data.foto)
              {
                  $('#label-photo').text('Change Photo'); // label photo upload
                  $('#photo-preview div').html('<img src="'+base_url+'asset/alumni/'+data.foto+'" class="img-responsive">'); // show photo
                  $('#photo-preview div').append('<input type="checkbox" name="remove_photo" value="'+data.foto+'"/> Remove photo when saving'); // remove photo

              }
              else
              {
                  $('#label-photo').text('Upload Photo'); // label photo upload
                  $('#photo-preview div').text('(No photo)');
              }


          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error get data from ajax');
          }
      });
  }

  function upload(id)
  {
      save_method = 'upload';
      // $('#form')[0].reset(); // reset form on modals

      $('.form-group').removeClass('has-error'); // clear error class
      $('.help-block').empty(); // clear error string


      //Ajax Load data from ajax
      $.ajax({
          url : "<?php echo site_url('administrator/alumni/ajax_edit')?>/" + id,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {
              $('[name="id"]').val(data.siswa_id);

              $('#modal_form_upload').modal('show'); // show bootstrap modal when complete loaded
              $('.modal-title').text('Upload'); // Set title to Bootstrap modal title
              $('#photo-preview').show(); // show photo preview modal

              if(data.foto)
              {
                  $('#upload-demo-i').html('<img src="'+base_url+'asset/alumni/'+data.foto+'" class="img-responsive">'); // show photo

              }
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error get data from ajax');
          }
      });
  }

  function save()
  {
      $('#btnSave').text('saving...'); //change button text
      $('#btnSave').attr('disabled',true); //set button disable 
      var url;

      if(save_method == 'add') {
          url = "<?php echo site_url('administrator/alumni/save')?>";
      } else {
          url = "<?php echo site_url('administrator/alumni/edit')?>";
      }

      // ajax adding data to database

      var formData = new FormData($('#form')[0]);
      $.ajax({
          url : url,
          type: "POST",
          data: formData,
          contentType: false,
          processData: false,
          dataType: "JSON",
          success: function(data)
          {

              if(data.status) //if success close modal and reload ajax table
              {
                  $('#modal_form').modal('hide');
                  reload_table();
                  //Ajax.show_alert('info',data.msg);
                  $.notify(data.msg,"success");
              }
              else
              {
                  for (var i = 0; i < data.inputerror.length; i++) 
                  {
                      $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                      $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                  }
              }
              $('#btnSave').text('save'); //change button text
              $('#btnSave').attr('disabled',false); //set button enable 


          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error adding / update data');
              $('#btnSave').text('save'); //change button text
              $('#btnSave').attr('disabled',false); //set button enable 
          }
      });
  }
</script>

<script type="text/javascript">
  $uploadCrop = $('#upload-demo').croppie({
      enableExif: true,
      viewport: {
          width: 250,
          height: 250,
          type: 'square'
      },
      boundary: {
          width: 300,
          height: 300
      }
  });

  $('#upload').on('change', function () { 
      var reader = new FileReader();
      reader.onload = function (e) {
          $uploadCrop.croppie('bind', {
              url: e.target.result
          }).then(function(){
              console.log('jQuery bind complete');
          });
      }
      reader.readAsDataURL(this.files[0]);
  });
  $('.upload-result').on('click', function (ev) {
      $uploadCrop.croppie('result', {
          type: 'canvas',
          size: 'viewport'
      }).then(function (resp) {
          var send = {
              id: $('[name="id"]').val(),
              image:resp,
          }
          $.ajax({
              url: "<?php echo site_url('administrator/alumni/upload')?>/",
              type: "POST",
              data: send,
              success: function (data) {
                  html = '<img src="' + resp + '" />';
                  $("#upload-demo-i").html(html);
                  reload_table();
                  $.notify("Sukses Ubah Foto","success");
              }
          });
      });
  });
</script>