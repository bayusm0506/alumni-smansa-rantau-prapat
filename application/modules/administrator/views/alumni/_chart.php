<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Grafik
    <small>Alumni</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url()?>administrator/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?=base_url()?>administrator/grafikAlumni">Alumni SMANSA</a></li>
    <li class="active">Grafik Alumni</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title">Data Grafik Alumni</h3>
  </div>
<!-- /.box-header -->
  <div class="box-body">
  	<script src="<?php echo base_url(); ?>asset/admin/highcharts/code/highcharts.js"></script>
	<script src="<?php echo base_url(); ?>asset/admin/highcharts/code/modules/series-label.js"></script>
	<script src="<?php echo base_url(); ?>asset/admin/highcharts/code/modules/exporting.js"></script>
	<script src="<?php echo base_url(); ?>asset/admin/highcharts/code/modules/export-data.js"></script>
	<script src="<?php echo base_url(); ?>asset/admin/dist/js/maps.js"></script>
	<div class="col-md-6">
		<div id="element_chart_data"></div>
	</div>
	<div class="col-md-6">
		<div id="element_chart_data_gelar"></div>
	</div>
  </div>
</div>
<!-- /.box -->
</section>
<!-- /.content -->
<script>
	var auto_get_info = function(){
		Ajax.run("<?=site_url('administrator/grafikAlumni/RealData')?>", 'GET', {},function(response){
			$("#element_chart_data").html("");
			$("#element_chart_data_gelar").html("");

			var yAxRealChart = [
				{ // Primary yAxis
                    labels: {
                        format: '{value}',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    title: {
                        text: '',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    }
                }
                , 
                { 
                    title: {
                        text: 'Jumlah Alumni',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                }
                ];
			var _response1 = response.realChart;
          //,$category,$yAxis,$series,$title,$subtitle,$text
          Maps.multyAxis("element_chart_data",_response1.categories,yAxRealChart,_response1.series,_response1.title,_response1.subtitle,_response1.text);

          var yAxRealChartGelar = [
				{ // Primary yAxis
                    labels: {
                        format: '{value}',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    title: {
                        text: '',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    }
                }
                , 
                { 
                    title: {
                        text: 'Jumlah Alumni',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    }
                }
                ];
			var _response = response.realChartGelar;
          //,$category,$yAxis,$series,$title,$subtitle,$text
          Maps.multyAxis("element_chart_data_gelar",_response.categories,yAxRealChartGelar,_response.series,_response.title,_response.subtitle,_response.text);
        });
	}
	var runInt = setInterval(function(){ auto_get_info(); }, 9000);

	$(document).ready(function(){
		auto_get_info();
	})
</script>