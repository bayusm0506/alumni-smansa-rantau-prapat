<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Alumni Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal" name="myForm">
                    <div class="callout callout-info">
                    <h4><i class="fa fa-info"></i> Note:</h4>
                        1. Max File Upload : 3 Mb <br>
                        2. Format Extensi File : gif,jpg,png <br>
                        3. File Upload = Width : 426px, Height : 508px <br>
                    </div>
                    <input type="hidden" value="" name="id"/> 
                    <input type="hidden" value="" name="id_siswa"/> 
                    <input type="hidden" value="" name="kd_anggota"/> 
                    <input type="hidden" value="" name="id_jurusan_cek"/> 
                    <input type="hidden" value="" name="stambuk_cek"/>
                    <input type="hidden" value="" name="tanggal_cek"/> 
                    <div class="form-body">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Nama</label>
                                <div class="col-md-9">
                                    <input name="nama" placeholder="Nama" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Tempat Lahir</label>
                                <div class="col-md-9">
                                    <input name="tempat_lahir" placeholder="Tempat Lahir" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group" id="tertulis">
                                <label class="control-label col-md-3">Tanggal Lahir</label>
                                <div class="col-md-9">
                                    
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Alamat Lama</label>
                                <div class="col-md-9">
                                    <input name="alamat_lama" placeholder="Alamat Lama" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Alamat Baru</label>
                                <div class="col-md-9">
                                    <input name="alamat_baru" placeholder="Alamat Baru" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Stambuk</label>
                                <div class="col-md-9">
                                    <select name="stambuk" class="form-control" style="width: 100%" id="stambuk_f">
                                        <option value="">Pilih Stambuk</option>
                                        <?php
                                            for ($i=1950; $i <= date('Y'); $i++) { 
                                                echo "<option value='$i'>$i</option>";
                                            }
                                        ?>
                                    </select>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Jurusan</label>
                                <div class="col-md-9">
                                    <select class="form-control" name="id_jurusan" style="width:100%;" id="id_jurusan_f">
                                      <option value="">Pilih Jurusan</option>
                                      <?php
                                        if ($jurusan->num_rows() > 0)
                                        {
                                           foreach ($jurusan->result() as $row)
                                           {
                                      ?>
                                        <option value="<?=$row->kd_jurusan?>"><?=$row->nama_jurusan?></option>    
                                      <?php
                                           }
                                        }
                                      ?>
                                    </select>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Tahun Lulus</label>
                                <div class="col-md-9">
                                    <select name="tahun_lulus" class="form-control" style="width: 100%">
                                        <option value="">Pilih Tahun</option>
                                        <?php
                                            for ($i=1950; $i <= date('Y'); $i++) { 
                                                echo "<option value='$i'>$i</option>";
                                            }
                                        ?>
                                    </select>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Facebook</label>
                                <div class="col-md-9">
                                    <input name="facebook" placeholder="Facebook" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Instagram</label>
                                <div class="col-md-9">
                                    <input name="instagram" placeholder="Instagram" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Twitter</label>
                                <div class="col-md-9">
                                    <input name="twitter" placeholder="Twitter" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Youtube</label>
                                <div class="col-md-9">
                                    <input name="youtube" placeholder="Youtube" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Tahun Pindah</label>
                                <div class="col-md-9">
                                    <select name="tahun_pindah" class="form-control" style="width: 100%">
                                        <option value="">Pilih Tahun</option>
                                        <?php
                                            for ($i=1950; $i <= date('Y'); $i++) { 
                                                echo "<option value='$i'>$i</option>";
                                            }
                                        ?>
                                    </select>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Sekolah Pindah</label>
                                <div class="col-md-9">
                                    <input name="sekolah_pindah" placeholder="Sekolah Pindah" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Gelar</label>
                                <div class="col-md-9">
                                    <select class="form-control" name="id_gelar" style="width:100%;" id="id_gelar_f">
                                      <option value="">Pilih Gelar</option>
                                      <?php
                                        if ($gelar->num_rows() > 0)
                                        {
                                           foreach ($gelar->result() as $row)
                                           {
                                      ?>
                                        <option value="<?=$row->id_gelar?>"><?=$row->nama_gelar?></option>    
                                      <?php
                                           }
                                        }
                                      ?>
                                    </select>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Profesi</label>
                                <div class="col-md-9">
                                    <input name="profesi" placeholder="Profesi" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Nama Keluarga</label>
                                <div class="col-md-9">
                                    <input name="nama_keluarga" placeholder="Nama Keluarga" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Profesi Keluarga</label>
                                <div class="col-md-9">
                                    <input name="profesi_keluarga" placeholder="Profesi Keluarga" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Hobi</label>
                                <div class="col-md-9">
                                    <input name="hobi" placeholder="Hobi" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group" id="tertulis_daftar">
                                <label class="control-label col-md-3">Tanggal Terdaftar</label>
                                <div class="col-md-9">
                                    
                                </div>
                            </div>
                            <div class="form-group" id="photo-preview">
                                <label class="control-label col-md-3">Photo</label>
                                <div class="col-md-9">
                                    (No photo)
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" id="label-photo">Upload Photo </label>
                                <div class="col-md-9">
                                    <input type="file" name="photo">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->


<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_upload" role="dialog" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Alumni Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal" name="myForm">
                    <div class="callout callout-info">
                    <h4><i class="fa fa-info"></i> Note:</h4>
                        1. Max File Upload : 3 Mb <br>
                    </div>
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">
                        <div class="col-md-6">
                            <div id="upload-demo" style="width:350px"></div>
                        </div>
                        <div class="col-md-6">
                            <div id="upload-demo-i" style="background:#e1e1e1;width:300px;padding:30px;height:300px;margin-top:30px"></div>
                        </div>
                        <div class="col-md-6" style="text-align: center;">
                            <div class="form-group">
                                <label class="control-label col-md-3" id="label-photo">Upload Photo </label>
                                <div class="col-md-9">
                                    <input type="file" id="upload">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary upload-result">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
