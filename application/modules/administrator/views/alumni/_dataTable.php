<div class="box-body" style="overflow: auto;">
  <table id="table" class="table table-bordered table-striped">
    <thead>
    <tr>
      <th>No</th>
      <th>Nama</th>
      <th>Tempat Lahir</th>
      <th>Tanggal Lahir</th>
      <th>Alamat Baru</th>
      <th>Stambuk</th>
      <th>Tahun Lulus</th>
      <th>Jurusan</th>
      <th>Gelar</th>
      <th>Foto</th>
      <th>Kode Anggota</th>
      <th>QR Code</th>
      <th>Tanggal Terdaftar</th>
      <th>Action</th>
    </tr>
    </thead>
    <tbody>
      
    </tbody>
    <tfoot>
    <tr>
      <th>No</th>
      <th>Nama</th>
      <th>Tempat Lahir</th>
      <th>Tanggal Lahir</th>
      <th>Alamat Baru</th>
      <th>Stambuk</th>
      <th>Tahun Lulus</th>
      <th>Jurusan</th>
      <th>Gelar</th>
      <th>Foto</th>
      <th>Kode Anggota</th>
      <th>QR Code</th>
      <th>Tanggal Terdaftar</th>
      <th>Action</th>
    </tr>
    </tfoot>
  </table>
</div>
<!-- /.box-body -->