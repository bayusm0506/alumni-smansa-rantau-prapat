<div class="box box-default" id="close-search" style="display: none;">
  <div class="box-header with-border">
    <h3 class="box-title">Custom Filter : </h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <form id="form-filter" class="form-horizontal">
      <div class="col-lg-4">
        <label for="">Nama</label>        
        <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa fa-object-group fa-lg" aria-hidden="true"></i>
            </div>
            <input class="form-control" placeholder="Nama" type="text" id="nama" />          
        </div>
      </div>
      <div class="col-lg-4">
        <label for="judul">Tanggal Lahir</label>        
        <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa-calendar fa-lg" aria-hidden="true"></i>
            </div>
            <input class="form-control" placeholder="Tanggal Lahir" type="text" id="datepicker" /> 
        </div>
      </div>
      <div class="col-lg-4">
        <label for="">Stambuk</label>        
        <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa-calendar fa-lg" aria-hidden="true"></i>
            </div>
            <select class="select2 form-control" style="width: 100%" id="stambuk_s">
                <option value="">Pilih Stambuk</option>
                <?php
                    for ($i=1950; $i <= date('Y'); $i++) { 
                        echo "<option value='$i'>$i</option>";
                    }
                ?>
            </select>
        </div>
      </div>
      <div class="col-lg-4">
        <label for="">Tahun Lulus</label>        
        <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa-calendar fa-lg" aria-hidden="true"></i>
            </div>
            <select class="select2 form-control" style="width: 100%" id="thn_lulus_s">
                <option value="">Pilih Tahun</option>
                <?php
                    for ($i=1950; $i <= date('Y'); $i++) { 
                        echo "<option value='$i'>$i</option>";
                    }
                ?>
            </select>
        </div>
      </div>
      <div class="col-lg-4">
        <label for="">Jurusan</label>        
        <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa-calendar fa-lg" aria-hidden="true"></i>
            </div>
            <select class="select2 form-control" style="width:100%;" id="jurusan_s">
              <option value="">Pilih Jurusan</option>
              <?php
                if ($jurusan->num_rows() > 0)
                {
                   foreach ($jurusan->result() as $row)
                   {
              ?>
                <option value="<?=$row->kd_jurusan?>"><?=$row->nama_jurusan?></option>    
              <?php
                   }
                }
              ?>
            </select>
        </div>
      </div>
      <div class="col-lg-4">
        <label for="">Gelar</label>        
        <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa-calendar fa-lg" aria-hidden="true"></i>
            </div>
            <select class="select2 form-control" style="width:100%;" id="gelar_s">
              <option value="">Pilih Gelar</option>
              <?php
                if ($gelar->num_rows() > 0)
                {
                   foreach ($gelar->result() as $row)
                   {
              ?>
                <option value="<?=$row->id_gelar?>"><?=$row->nama_gelar?></option>    
              <?php
                   }
                }
              ?>
            </select>
        </div>
      </div>
      <div class="col-lg-4">
        <label for="judul">Tanggal Terdaftar</label>        
        <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa-calendar fa-lg" aria-hidden="true"></i>
            </div>
            <input class="form-control" placeholder="Tanggal Terdaftar" type="text" id="datepicker_terdaftar" /> 
        </div>
      </div>
      <div class="col-lg-6">
        <label for="filter"></label>        
        <div class="input-group">
            <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
            <button type="button" id="btn-reset" class="btn btn-default">Reset</button>
        </div>
      </div>
    </form>
  </div>
  <!-- /.box-body -->
  <div class="box-footer">
    <!-- box-footer -->
  </div>
</div>